using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using System.Linq;
using Crestron;
using Crestron.Logos.SplusLibrary;
using Crestron.Logos.SplusObjects;
using Crestron.SimplSharp;

namespace UserModule_ARTNET
{
    public class UserModuleClass_ARTNET : SplusObject
    {
        static CCriticalSection g_criticalSection = new CCriticalSection();
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        Crestron.Logos.SplusObjects.DigitalInput ENABLE;
        Crestron.Logos.SplusObjects.DigitalInput FORMAT;
        Crestron.Logos.SplusObjects.DigitalInput AUTOREFRESH;
        InOutArray<Crestron.Logos.SplusObjects.AnalogInput> CHANNEL;
        Crestron.Logos.SplusObjects.DigitalOutput SOCKET_ERROR;
        InOutArray<Crestron.Logos.SplusObjects.AnalogOutput> CHANNEL_FB;
        StringParameter BROADCASTIP__DOLLAR__;
        UShortParameter UNIVERSE;
        SplusUdpSocket UDPCLIENT;
        ushort ART_ENABLED = 0;
        ushort ART_PACKET_SEQUENCE = 0;
        ushort [] DMX_BUFFER;
        CrestronString PACK_HEAD__DOLLAR__;
        CrestronString PACK_BODY__DOLLAR__;
        CrestronString PACK_HEADER__DOLLAR__;
        CrestronString OUT__DOLLAR__;
        ushort CHG = 0;
        private void PREPAREPACKETHEADER (  SplusExecutionContext __context__ ) 
            { 
            
            __context__.SourceCodeLine = 201;
            MakeString ( PACK_HEAD__DOLLAR__ , "{0}{1}{2}{3}{4}", "Art-Net\u0000" , Functions.Chr (  (int) ( Functions.Low( (ushort) 20480 ) ) ) , Functions.Chr (  (int) ( Functions.High( (ushort) 20480 ) ) ) , Functions.Chr (  (int) ( 0 ) ) , Functions.Chr (  (int) ( 14 ) ) ) ; 
            
            }
            
        private void PREPAREPACKETBODY (  SplusExecutionContext __context__ ) 
            { 
            
            __context__.SourceCodeLine = 215;
            MakeString ( PACK_BODY__DOLLAR__ , "{0}{1}{2}{3}{4}", Functions.Chr (  (int) ( 0 ) ) , Functions.Chr (  (int) ( UNIVERSE  .Value ) ) , Functions.Chr (  (int) ( 0 ) ) , Functions.Chr (  (int) ( Functions.High( (ushort) 64 ) ) ) , Functions.Chr (  (int) ( Functions.Low( (ushort) 64 ) ) ) ) ; 
            
            }
            
        object CHANNEL_OnChange_0 ( Object __EventInfo__ )
        
            { 
            Crestron.Logos.SplusObjects.SignalEventArgs __SignalEventArg__ = (Crestron.Logos.SplusObjects.SignalEventArgs)__EventInfo__;
            try
            {
                SplusExecutionContext __context__ = SplusThreadStartCode(__SignalEventArg__);
                ushort CHAN = 0;
                
                ushort LEVEL = 0;
                
                
                __context__.SourceCodeLine = 233;
                CHAN = (ushort) ( (Functions.GetLastModifiedArrayIndex( __SignalEventArg__ ) - 1) ) ; 
                __context__.SourceCodeLine = 234;
                if ( Functions.TestForTrue  ( ( FORMAT  .Value)  ) ) 
                    { 
                    __context__.SourceCodeLine = 235;
                    LEVEL = (ushort) ( CHANNEL[ (CHAN + 1) ] .UshortValue ) ; 
                    } 
                
                else 
                    { 
                    __context__.SourceCodeLine = 237;
                    LEVEL = (ushort) ( Functions.MulDiv( (ushort)( CHANNEL[ (CHAN + 1) ] .UshortValue ) , (ushort)( 255 ) , (ushort)( 65535 ) ) ) ; 
                    } 
                
                __context__.SourceCodeLine = 239;
                if ( Functions.TestForTrue  ( ( Functions.BoolToInt (DMX_BUFFER[ CHAN ] != LEVEL))  ) ) 
                    { 
                    __context__.SourceCodeLine = 240;
                    DMX_BUFFER [ CHAN] = (ushort) ( LEVEL ) ; 
                    __context__.SourceCodeLine = 241;
                    CHG = (ushort) ( 1 ) ; 
                    } 
                
                
                
            }
            catch(Exception e) { ObjectCatchHandler(e); }
            finally { ObjectFinallyHandler( __SignalEventArg__ ); }
            return this;
            
        }
        
    object ENABLE_OnPush_1 ( Object __EventInfo__ )
    
        { 
        Crestron.Logos.SplusObjects.SignalEventArgs __SignalEventArg__ = (Crestron.Logos.SplusObjects.SignalEventArgs)__EventInfo__;
        try
        {
            SplusExecutionContext __context__ = SplusThreadStartCode(__SignalEventArg__);
            short STATUS = 0;
            
            ushort CNT = 0;
            
            
            __context__.SourceCodeLine = 253;
            CHG = (ushort) ( 1 ) ; 
            __context__.SourceCodeLine = 256;
            STATUS = (short) ( Functions.SocketUDP_Enable( UDPCLIENT , BROADCASTIP__DOLLAR__  , (ushort)( 6454 ) ) ) ; 
            __context__.SourceCodeLine = 258;
            if ( Functions.TestForTrue  ( ( Functions.BoolToInt ( STATUS < 0 ))  ) ) 
                { 
                __context__.SourceCodeLine = 259;
                SOCKET_ERROR  .Value = (ushort) ( 1 ) ; 
                __context__.SourceCodeLine = 260;
                ART_ENABLED = (ushort) ( 0 ) ; 
                __context__.SourceCodeLine = 261;
                __context__.SourceCodeLine = 262;
                Trace( "{0}: Error listening on UDP Socket - Status: {1:d}\r\n", "Art-Net: " , (short)STATUS) ; 
                
                __context__.SourceCodeLine = 264;
                Functions.TerminateEvent (); 
                } 
            
            __context__.SourceCodeLine = 267;
            SOCKET_ERROR  .Value = (ushort) ( 0 ) ; 
            __context__.SourceCodeLine = 268;
            ART_ENABLED = (ushort) ( 1 ) ; 
            __context__.SourceCodeLine = 270;
            while ( Functions.TestForTrue  ( ( ENABLE  .Value)  ) ) 
                { 
                __context__.SourceCodeLine = 271;
                if ( Functions.TestForTrue  ( ( CHG)  ) ) 
                    { 
                    __context__.SourceCodeLine = 272;
                    CHG = (ushort) ( 0 ) ; 
                    __context__.SourceCodeLine = 273;
                    OUT__DOLLAR__  .UpdateValue ( "" + PACK_HEADER__DOLLAR__  ) ; 
                    __context__.SourceCodeLine = 275;
                    ushort __FN_FORSTART_VAL__1 = (ushort) ( 0 ) ;
                    ushort __FN_FOREND_VAL__1 = (ushort)(64 - 1); 
                    int __FN_FORSTEP_VAL__1 = (int)1; 
                    for ( CNT  = __FN_FORSTART_VAL__1; (__FN_FORSTEP_VAL__1 > 0)  ? ( (CNT  >= __FN_FORSTART_VAL__1) && (CNT  <= __FN_FOREND_VAL__1) ) : ( (CNT  <= __FN_FORSTART_VAL__1) && (CNT  >= __FN_FOREND_VAL__1) ) ; CNT  += (ushort)__FN_FORSTEP_VAL__1) 
                        { 
                        __context__.SourceCodeLine = 276;
                        OUT__DOLLAR__  .UpdateValue ( OUT__DOLLAR__ + Functions.Chr (  (int) ( DMX_BUFFER[ CNT ] ) )  ) ; 
                        __context__.SourceCodeLine = 275;
                        } 
                    
                    __context__.SourceCodeLine = 279;
                    Functions.SocketSend ( UDPCLIENT , OUT__DOLLAR__ ) ; 
                    } 
                
                else 
                    { 
                    __context__.SourceCodeLine = 281;
                    Functions.Delay (  (int) ( 1 ) ) ; 
                    } 
                
                __context__.SourceCodeLine = 270;
                } 
            
            
            
        }
        catch(Exception e) { ObjectCatchHandler(e); }
        finally { ObjectFinallyHandler( __SignalEventArg__ ); }
        return this;
        
    }
    
object ENABLE_OnRelease_2 ( Object __EventInfo__ )

    { 
    Crestron.Logos.SplusObjects.SignalEventArgs __SignalEventArg__ = (Crestron.Logos.SplusObjects.SignalEventArgs)__EventInfo__;
    try
    {
        SplusExecutionContext __context__ = SplusThreadStartCode(__SignalEventArg__);
        short STATUS = 0;
        
        
        __context__.SourceCodeLine = 292;
        ART_ENABLED = (ushort) ( 0 ) ; 
        __context__.SourceCodeLine = 294;
        STATUS = (short) ( Functions.SocketUDP_Disable( UDPCLIENT ) ) ; 
        __context__.SourceCodeLine = 296;
        if ( Functions.TestForTrue  ( ( Functions.BoolToInt ( STATUS < 0 ))  ) ) 
            { 
            __context__.SourceCodeLine = 297;
            SOCKET_ERROR  .Value = (ushort) ( 1 ) ; 
            __context__.SourceCodeLine = 298;
            __context__.SourceCodeLine = 299;
            Trace( "{0}: Error closing on UDP Socket - Status: {1:d}\r\n", "Art-Net: " , (short)STATUS) ; 
            
            __context__.SourceCodeLine = 301;
            Functions.TerminateEvent (); 
            } 
        
        __context__.SourceCodeLine = 303;
        SOCKET_ERROR  .Value = (ushort) ( 0 ) ; 
        
        
    }
    catch(Exception e) { ObjectCatchHandler(e); }
    finally { ObjectFinallyHandler( __SignalEventArg__ ); }
    return this;
    
}

public override object FunctionMain (  object __obj__ ) 
    { 
    try
    {
        SplusExecutionContext __context__ = SplusFunctionMainStartCode();
        
        __context__.SourceCodeLine = 384;
        PREPAREPACKETHEADER (  __context__  ) ; 
        __context__.SourceCodeLine = 385;
        PREPAREPACKETBODY (  __context__  ) ; 
        __context__.SourceCodeLine = 388;
        PACK_HEADER__DOLLAR__  .UpdateValue ( PACK_HEAD__DOLLAR__ + Functions.Chr (  (int) ( 0 ) ) + PACK_BODY__DOLLAR__  ) ; 
        __context__.SourceCodeLine = 392;
        WaitForInitializationComplete ( ) ; 
        
        
    }
    catch(Exception e) { ObjectCatchHandler(e); }
    finally { ObjectFinallyHandler(); }
    return __obj__;
    }
    

public override void LogosSplusInitialize()
{
    SocketInfo __socketinfo__ = new SocketInfo( 1, this );
    InitialParametersClass.ResolveHostName = __socketinfo__.ResolveHostName;
    _SplusNVRAM = new SplusNVRAM( this );
    DMX_BUFFER  = new ushort[ 65 ];
    PACK_HEAD__DOLLAR__  = new CrestronString( Crestron.Logos.SplusObjects.CrestronStringEncoding.eEncodingASCII, 12, this );
    PACK_BODY__DOLLAR__  = new CrestronString( Crestron.Logos.SplusObjects.CrestronStringEncoding.eEncodingASCII, 5, this );
    PACK_HEADER__DOLLAR__  = new CrestronString( Crestron.Logos.SplusObjects.CrestronStringEncoding.eEncodingASCII, 18, this );
    OUT__DOLLAR__  = new CrestronString( Crestron.Logos.SplusObjects.CrestronStringEncoding.eEncodingASCII, 529, this );
    UDPCLIENT  = new SplusUdpSocket ( 1024, this );
    
    ENABLE = new Crestron.Logos.SplusObjects.DigitalInput( ENABLE__DigitalInput__, this );
    m_DigitalInputList.Add( ENABLE__DigitalInput__, ENABLE );
    
    FORMAT = new Crestron.Logos.SplusObjects.DigitalInput( FORMAT__DigitalInput__, this );
    m_DigitalInputList.Add( FORMAT__DigitalInput__, FORMAT );
    
    AUTOREFRESH = new Crestron.Logos.SplusObjects.DigitalInput( AUTOREFRESH__DigitalInput__, this );
    m_DigitalInputList.Add( AUTOREFRESH__DigitalInput__, AUTOREFRESH );
    
    SOCKET_ERROR = new Crestron.Logos.SplusObjects.DigitalOutput( SOCKET_ERROR__DigitalOutput__, this );
    m_DigitalOutputList.Add( SOCKET_ERROR__DigitalOutput__, SOCKET_ERROR );
    
    CHANNEL = new InOutArray<AnalogInput>( 64, this );
    for( uint i = 0; i < 64; i++ )
    {
        CHANNEL[i+1] = new Crestron.Logos.SplusObjects.AnalogInput( CHANNEL__AnalogSerialInput__ + i, CHANNEL__AnalogSerialInput__, this );
        m_AnalogInputList.Add( CHANNEL__AnalogSerialInput__ + i, CHANNEL[i+1] );
    }
    
    CHANNEL_FB = new InOutArray<AnalogOutput>( 64, this );
    for( uint i = 0; i < 64; i++ )
    {
        CHANNEL_FB[i+1] = new Crestron.Logos.SplusObjects.AnalogOutput( CHANNEL_FB__AnalogSerialOutput__ + i, this );
        m_AnalogOutputList.Add( CHANNEL_FB__AnalogSerialOutput__ + i, CHANNEL_FB[i+1] );
    }
    
    UNIVERSE = new UShortParameter( UNIVERSE__Parameter__, this );
    m_ParameterList.Add( UNIVERSE__Parameter__, UNIVERSE );
    
    BROADCASTIP__DOLLAR__ = new StringParameter( BROADCASTIP__DOLLAR____Parameter__, this );
    m_ParameterList.Add( BROADCASTIP__DOLLAR____Parameter__, BROADCASTIP__DOLLAR__ );
    
    
    for( uint i = 0; i < 64; i++ )
        CHANNEL[i+1].OnAnalogChange.Add( new InputChangeHandlerWrapper( CHANNEL_OnChange_0, false ) );
        
    ENABLE.OnDigitalPush.Add( new InputChangeHandlerWrapper( ENABLE_OnPush_1, false ) );
    ENABLE.OnDigitalRelease.Add( new InputChangeHandlerWrapper( ENABLE_OnRelease_2, false ) );
    
    _SplusNVRAM.PopulateCustomAttributeList( true );
    
    NVRAM = _SplusNVRAM;
    
}

public override void LogosSimplSharpInitialize()
{
    
    
}

public UserModuleClass_ARTNET ( string InstanceName, string ReferenceID, Crestron.Logos.SplusObjects.CrestronStringEncoding nEncodingType ) : base( InstanceName, ReferenceID, nEncodingType ) {}




const uint ENABLE__DigitalInput__ = 0;
const uint FORMAT__DigitalInput__ = 1;
const uint AUTOREFRESH__DigitalInput__ = 2;
const uint CHANNEL__AnalogSerialInput__ = 0;
const uint SOCKET_ERROR__DigitalOutput__ = 0;
const uint CHANNEL_FB__AnalogSerialOutput__ = 0;
const uint BROADCASTIP__DOLLAR____Parameter__ = 10;
const uint UNIVERSE__Parameter__ = 11;

[SplusStructAttribute(-1, true, false)]
public class SplusNVRAM : SplusStructureBase
{

    public SplusNVRAM( SplusObject __caller__ ) : base( __caller__ ) {}
    
    
}

SplusNVRAM _SplusNVRAM = null;

public class __CEvent__ : CEvent
{
    public __CEvent__() {}
    public void Close() { base.Close(); }
    public int Reset() { return base.Reset() ? 1 : 0; }
    public int Set() { return base.Set() ? 1 : 0; }
    public int Wait( int timeOutInMs ) { return base.Wait( timeOutInMs ) ? 1 : 0; }
}
public class __CMutex__ : CMutex
{
    public __CMutex__() {}
    public void Close() { base.Close(); }
    public void ReleaseMutex() { base.ReleaseMutex(); }
    public int WaitForMutex() { return base.WaitForMutex() ? 1 : 0; }
}
 public int IsNull( object obj ){ return (obj == null) ? 1 : 0; }
}


}

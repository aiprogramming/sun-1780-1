using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using System.Linq;
using Crestron;
using Crestron.Logos.SplusLibrary;
using Crestron.Logos.SplusObjects;
using Crestron.SimplSharp;

namespace UserModule_BARCOPULSEPROJECTORCOMMANDS
{
    public class UserModuleClass_BARCOPULSEPROJECTORCOMMANDS : SplusObject
    {
        static CCriticalSection g_criticalSection = new CCriticalSection();
        
        
        
        
        
        
        
        
        Crestron.Logos.SplusObjects.DigitalInput DISTARTCLIENT;
        Crestron.Logos.SplusObjects.DigitalInput DIENABLETRACELOGS;
        Crestron.Logos.SplusObjects.DigitalInput DIPOWERON;
        Crestron.Logos.SplusObjects.DigitalInput DIPOWEROFF;
        Crestron.Logos.SplusObjects.DigitalInput DIREBOOTPROJECTORCOMMAND;
        Crestron.Logos.SplusObjects.DigitalInput DIGOTOSTANDBYECOMODE;
        Crestron.Logos.SplusObjects.DigitalInput DISOURCEDVI1;
        Crestron.Logos.SplusObjects.DigitalInput DISOURCEDVI2;
        Crestron.Logos.SplusObjects.DigitalInput DISOURCEDISPLAYPORT1;
        Crestron.Logos.SplusObjects.DigitalInput DISOURCEDISPLAYPORT2;
        Crestron.Logos.SplusObjects.DigitalInput DISOURCEDUALDVICOLUMNS;
        Crestron.Logos.SplusObjects.DigitalInput DISOURCEDUALDISPLAYPORTCOLUMNS;
        Crestron.Logos.SplusObjects.DigitalInput DISOURCEDUALDVISEQUENTIAL;
        Crestron.Logos.SplusObjects.DigitalInput DISOURCEDUALDISPLAYPORTSEQUENTIAL;
        Crestron.Logos.SplusObjects.DigitalInput DISOURCEHDMI;
        Crestron.Logos.SplusObjects.DigitalInput DISOURCEHDBASET;
        Crestron.Logos.SplusObjects.DigitalInput DISOURCESDI;
        Crestron.Logos.SplusObjects.DigitalInput DIREFRESHEDID;
        Crestron.Logos.SplusObjects.DigitalInput DICENTER_16BY9_ENABLE;
        Crestron.Logos.SplusObjects.DigitalInput DICENTER_16BY9_DISABLE;
        Crestron.Logos.SplusObjects.DigitalInput DISET_FACTOR;
        Crestron.Logos.SplusObjects.DigitalInput DISET_CROPPING_TO_MANUAL;
        Crestron.Logos.SplusObjects.DigitalInput DISET_CROPPING_TO_ASPECT;
        Crestron.Logos.SplusObjects.DigitalInput DISET_CROPPING_TO_AUTO;
        Crestron.Logos.SplusObjects.DigitalInput DISET_CROPPING_ASPECT_RATIO_TO_16_9;
        Crestron.Logos.SplusObjects.DigitalInput DISET_CROPPING_ASPECT_RATIO_TO_1_85;
        Crestron.Logos.SplusObjects.DigitalInput DISET_CROPPING_ASPECT_RATIO_TO_2_2;
        Crestron.Logos.SplusObjects.DigitalInput DISET_CROPPING_ASPECT_RATIO_TO_2_35;
        Crestron.Logos.SplusObjects.DigitalInput DISET_CROPPING_ASPECT_RATIO_TO_2_37;
        Crestron.Logos.SplusObjects.DigitalInput DISET_CROPPING_ASPECT_RATIO_TO_2_39;
        Crestron.Logos.SplusObjects.DigitalInput DISET_MANUAL_CROPPING_ASPECT_RATIO;
        Crestron.Logos.SplusObjects.DigitalInput DIENABLEGAMUTREMAPPING;
        Crestron.Logos.SplusObjects.DigitalInput DIENABLE_CONTENT_BASED_AUTO_CROPPING;
        Crestron.Logos.SplusObjects.DigitalInput DIDISABLE_CONTENT_BASED_AUTO_CROPPING;
        Crestron.Logos.SplusObjects.DigitalInput DIOPENSHUTTER;
        Crestron.Logos.SplusObjects.DigitalInput DICLOSESHUTTER;
        Crestron.Logos.SplusObjects.DigitalInput DIZOOMIN;
        Crestron.Logos.SplusObjects.DigitalInput DIZOOMOUT;
        Crestron.Logos.SplusObjects.DigitalInput DIFOCUSIN;
        Crestron.Logos.SplusObjects.DigitalInput DIFOCUSOUT;
        Crestron.Logos.SplusObjects.DigitalInput DIIRISIN;
        Crestron.Logos.SplusObjects.DigitalInput DIIRISOUT;
        Crestron.Logos.SplusObjects.DigitalInput DILENSSHIFTUP;
        Crestron.Logos.SplusObjects.DigitalInput DILENSSHIFTDOWN;
        Crestron.Logos.SplusObjects.DigitalInput DILENSSHIFTLEFT;
        Crestron.Logos.SplusObjects.DigitalInput DILENSSHIFTRIGHT;
        Crestron.Logos.SplusObjects.DigitalInput DIENABLE_WARP;
        Crestron.Logos.SplusObjects.DigitalInput DIDISABLE_WARP;
        Crestron.Logos.SplusObjects.DigitalInput DIENABLE_4_CORNERS;
        Crestron.Logos.SplusObjects.DigitalInput DIDISABLE_4_CORNERS;
        Crestron.Logos.SplusObjects.DigitalInput DISET_4_CORNERS_TOP_LEFT;
        Crestron.Logos.SplusObjects.DigitalInput DISET_4_CORNERS_BOTTOM_LEFT;
        Crestron.Logos.SplusObjects.DigitalInput DISET_4_CORNERS_TOP_RIGHT;
        Crestron.Logos.SplusObjects.DigitalInput DISET_4_CORNERS_BOTTOM_RIGHT;
        Crestron.Logos.SplusObjects.DigitalInput DIENABLE_BOW;
        Crestron.Logos.SplusObjects.DigitalInput DIDISABLE_BOW;
        Crestron.Logos.SplusObjects.DigitalInput DIENABLE_SYMMETRIC;
        Crestron.Logos.SplusObjects.DigitalInput DIDISABLE_SYMMETRIC;
        Crestron.Logos.SplusObjects.DigitalInput DISET_BOW_TOP;
        Crestron.Logos.SplusObjects.DigitalInput DISET_BOW_BOTTOM;
        Crestron.Logos.SplusObjects.DigitalInput DIISCINEMASCOPE;
        Crestron.Logos.SplusObjects.DigitalInput DIRECALLPROFILE;
        Crestron.Logos.SplusObjects.DigitalInput DIRECALLPRESET;
        Crestron.Logos.SplusObjects.DigitalInput DIENABLE_TRIGGER_1;
        Crestron.Logos.SplusObjects.DigitalInput DIDISABLE_TRIGGER_1;
        Crestron.Logos.SplusObjects.DigitalInput DIENABLE_TRIGGER_2;
        Crestron.Logos.SplusObjects.DigitalInput DIDISABLE_TRIGGER_2;
        Crestron.Logos.SplusObjects.DigitalInput DIENABLE_TRIGGER_3;
        Crestron.Logos.SplusObjects.DigitalInput DIDISABLE_TRIGGER_3;
        Crestron.Logos.SplusObjects.AnalogInput AICROP_VALUE_LEFT;
        Crestron.Logos.SplusObjects.AnalogInput AICROP_VALUE_RIGHT;
        Crestron.Logos.SplusObjects.AnalogInput AICROP_VALUE_TOP;
        Crestron.Logos.SplusObjects.AnalogInput AICROP_VALUE_BOTTOM;
        Crestron.Logos.SplusObjects.AnalogInput AILASERPOWER;
        Crestron.Logos.SplusObjects.AnalogInput AILEDPOWER;
        Crestron.Logos.SplusObjects.AnalogInput AIPRESETNUMBER;
        Crestron.Logos.SplusObjects.AnalogInput AI4CORNERSX;
        Crestron.Logos.SplusObjects.AnalogInput AI4CORNERSY;
        Crestron.Logos.SplusObjects.AnalogInput AIBOWANGLE;
        Crestron.Logos.SplusObjects.AnalogInput AIBOWLENGTH;
        Crestron.Logos.SplusObjects.StringInput SIFACTOR;
        Crestron.Logos.SplusObjects.StringInput SIHDRBOOSTVALUE;
        Crestron.Logos.SplusObjects.StringInput SIPROFILENAME;
        Crestron.Logos.SplusObjects.DigitalOutput DOCLIENTCONNECTED;
        Crestron.Logos.SplusObjects.AnalogOutput AOCONNECTIONSTATUS;
        Crestron.Logos.SplusObjects.AnalogOutput AOLASERPOWER;
        Crestron.Logos.SplusObjects.AnalogOutput AOLEDPOWER;
        Crestron.Logos.SplusObjects.AnalogOutput AOLASERONHOURS;
        Crestron.Logos.SplusObjects.AnalogOutput AOCROPPINGMANUALLEFT;
        Crestron.Logos.SplusObjects.AnalogOutput AOCROPPINGMANUALRIGHT;
        Crestron.Logos.SplusObjects.AnalogOutput AOCROPPINGMANUALTOP;
        Crestron.Logos.SplusObjects.AnalogOutput AOCROPPINGMANUALBOTTOM;
        Crestron.Logos.SplusObjects.StringOutput SOFROMDEVICE;
        Crestron.Logos.SplusObjects.StringOutput SOSYSTEMSTATE;
        Crestron.Logos.SplusObjects.StringOutput SOINPUTSOURCE;
        Crestron.Logos.SplusObjects.StringOutput SODETECTEDSIGNAL;
        Crestron.Logos.SplusObjects.StringOutput SOCOLORSPACE;
        Crestron.Logos.SplusObjects.StringOutput SOMASTERINGLUMINANCE;
        Crestron.Logos.SplusObjects.StringOutput SOCONTENTASPECTRATIO;
        Crestron.Logos.SplusObjects.StringOutput SOGAMMATYPE;
        Crestron.Logos.SplusObjects.StringOutput SOVERTICALRESOLUTION;
        Crestron.Logos.SplusObjects.StringOutput SOCENTER16BY9STATUS;
        Crestron.Logos.SplusObjects.StringOutput SOCENTER16BY9FACTOR;
        Crestron.Logos.SplusObjects.StringOutput SOCROPPINGMODE;
        Crestron.Logos.SplusObjects.StringOutput SOCROPPINGASPECTRATIO;
        Crestron.Logos.SplusObjects.StringOutput SOP7MODE;
        Crestron.Logos.SplusObjects.StringOutput SOHDRBOOSTVALUEFB;
        Crestron.Logos.SplusObjects.StringOutput SOCONTENTBASEDAUTOCROPPINGSTATUS;
        Crestron.Logos.SplusObjects.StringOutput SOSHUTTERPOSITION;
        Crestron.Logos.SplusObjects.StringOutput SOOUTLETTEMP;
        Crestron.Logos.SplusObjects.StringOutput SOINLETTEMP;
        Crestron.Logos.SplusObjects.StringOutput SOHUMIDITY;
        Crestron.Logos.SplusObjects.StringOutput SOPOWERACVOLTAGE;
        Crestron.Logos.SplusObjects.StringOutput SOWARPSTATUS;
        Crestron.Logos.SplusObjects.StringOutput SO4CORNERSSTATUS;
        Crestron.Logos.SplusObjects.StringOutput SOBOWSTATUS;
        Crestron.Logos.SplusObjects.StringOutput SOSYMMETRICSTATUS;
        SplusTcpClient TCPSOCKET;
        StringParameter SPIPADDRESS;
        UShortParameter IPPORT;
        CrestronString GSCURRENTSOURCE;
        ushort SEMAPHORE = 0;
        ushort GITRACELOGSENABLED = 0;
        ushort GIGAMUTREMAPPINGENABLED = 0;
        private void SENDMESSAGE (  SplusExecutionContext __context__, CrestronString SMESSAGE ) 
            { 
            short SISTATUS = 0;
            
            
            __context__.SourceCodeLine = 404;
            if ( Functions.TestForTrue  ( ( Functions.BoolToInt (GITRACELOGSENABLED == 1))  ) ) 
                {
                __context__.SourceCodeLine = 404;
                Trace( "MESSAGE TO BE SENT TO PROJECTOR = {0}", SMESSAGE ) ; 
                }
            
            __context__.SourceCodeLine = 407;
            if ( Functions.TestForTrue  ( ( Functions.BoolToInt (DOCLIENTCONNECTED  .Value == 1))  ) ) 
                { 
                __context__.SourceCodeLine = 409;
                SISTATUS = (short) ( Functions.SocketSend( TCPSOCKET , SMESSAGE ) ) ; 
                __context__.SourceCodeLine = 410;
                if ( Functions.TestForTrue  ( ( Functions.BoolToInt ( (Functions.TestForTrue ( Functions.BoolToInt ( SISTATUS < 0 ) ) && Functions.TestForTrue ( Functions.BoolToInt (GITRACELOGSENABLED == 1) )) ))  ) ) 
                    {
                    __context__.SourceCodeLine = 410;
                    Trace( "Error Sending to tcpSocket: {0:d}\r\n", (short)SISTATUS) ; 
                    }
                
                } 
            
            
            }
            
        private void CALLMETHOD (  SplusExecutionContext __context__, ushort PID , CrestronString PMETHOD , CrestronString PPARAMNAME , CrestronString PPARAMVALUE ) 
            { 
            CrestronString SHEADER;
            CrestronString SMETHOD;
            CrestronString SPARAMS;
            CrestronString SPROPERTY;
            CrestronString SID;
            CrestronString SENDBRACE;
            CrestronString STX;
            SHEADER  = new CrestronString( Crestron.Logos.SplusObjects.CrestronStringEncoding.eEncodingASCII, 50, this );
            SMETHOD  = new CrestronString( Crestron.Logos.SplusObjects.CrestronStringEncoding.eEncodingASCII, 50, this );
            SPARAMS  = new CrestronString( Crestron.Logos.SplusObjects.CrestronStringEncoding.eEncodingASCII, 150, this );
            SPROPERTY  = new CrestronString( Crestron.Logos.SplusObjects.CrestronStringEncoding.eEncodingASCII, 50, this );
            SID  = new CrestronString( Crestron.Logos.SplusObjects.CrestronStringEncoding.eEncodingASCII, 20, this );
            SENDBRACE  = new CrestronString( Crestron.Logos.SplusObjects.CrestronStringEncoding.eEncodingASCII, 1, this );
            STX  = new CrestronString( Crestron.Logos.SplusObjects.CrestronStringEncoding.eEncodingASCII, 350, this );
            
            
            __context__.SourceCodeLine = 419;
            SMETHOD  .UpdateValue ( "\"method\": \"" + PMETHOD + "\", "  ) ; 
            __context__.SourceCodeLine = 420;
            if ( Functions.TestForTrue  ( ( Functions.BoolToInt ( (Functions.TestForTrue ( Functions.BoolToInt (PPARAMNAME == "") ) && Functions.TestForTrue ( Functions.BoolToInt (PPARAMVALUE == "") )) ))  ) ) 
                { 
                __context__.SourceCodeLine = 422;
                SPARAMS  .UpdateValue ( "\"params\": {}, "  ) ; 
                } 
            
            else 
                { 
                __context__.SourceCodeLine = 426;
                if ( Functions.TestForTrue  ( ( Functions.BoolToInt (PPARAMNAME == "steps"))  ) ) 
                    { 
                    __context__.SourceCodeLine = 428;
                    SPARAMS  .UpdateValue ( "\"params\": {\"" + PPARAMNAME + "\":" + PPARAMVALUE + "}, "  ) ; 
                    } 
                
                else 
                    {
                    __context__.SourceCodeLine = 430;
                    if ( Functions.TestForTrue  ( ( Functions.BoolToInt (PPARAMNAME == "code"))  ) ) 
                        { 
                        __context__.SourceCodeLine = 432;
                        SPARAMS  .UpdateValue ( "\"params\": {\"" + PPARAMNAME + "\":\"" + PPARAMVALUE + "\"}, "  ) ; 
                        } 
                    
                    else 
                        { 
                        __context__.SourceCodeLine = 436;
                        SPARAMS  .UpdateValue ( "\"params\": {\"" + PPARAMNAME + "\":\"" + PPARAMVALUE + "\"}, "  ) ; 
                        } 
                    
                    }
                
                } 
            
            __context__.SourceCodeLine = 439;
            SID  .UpdateValue ( "\"id\": " + Functions.ItoA (  (int) ( PID ) ) + ""  ) ; 
            __context__.SourceCodeLine = 441;
            STX  .UpdateValue ( "{\"jsonrpc\": \"2.0\", " + SMETHOD + SPARAMS + SID + "}"  ) ; 
            __context__.SourceCodeLine = 443;
            SENDMESSAGE (  __context__ , STX) ; 
            
            }
            
        private void WRITEONEPROPERTY (  SplusExecutionContext __context__, ushort PID , CrestronString PPROPERTY , CrestronString PVALUE , CrestronString PVALUETYPE ) 
            { 
            CrestronString SHEADER;
            CrestronString SMETHOD;
            CrestronString SPARAMS;
            CrestronString SPROPERTY;
            CrestronString SID;
            CrestronString SENDBRACE;
            CrestronString STX;
            SHEADER  = new CrestronString( Crestron.Logos.SplusObjects.CrestronStringEncoding.eEncodingASCII, 50, this );
            SMETHOD  = new CrestronString( Crestron.Logos.SplusObjects.CrestronStringEncoding.eEncodingASCII, 50, this );
            SPARAMS  = new CrestronString( Crestron.Logos.SplusObjects.CrestronStringEncoding.eEncodingASCII, 150, this );
            SPROPERTY  = new CrestronString( Crestron.Logos.SplusObjects.CrestronStringEncoding.eEncodingASCII, 50, this );
            SID  = new CrestronString( Crestron.Logos.SplusObjects.CrestronStringEncoding.eEncodingASCII, 20, this );
            SENDBRACE  = new CrestronString( Crestron.Logos.SplusObjects.CrestronStringEncoding.eEncodingASCII, 1, this );
            STX  = new CrestronString( Crestron.Logos.SplusObjects.CrestronStringEncoding.eEncodingASCII, 350, this );
            
            
            __context__.SourceCodeLine = 451;
            SMETHOD  .UpdateValue ( "\"method\": \"property.set\", "  ) ; 
            __context__.SourceCodeLine = 452;
            if ( Functions.TestForTrue  ( ( Functions.BoolToInt ( (Functions.TestForTrue ( Functions.BoolToInt (PVALUETYPE == "BOOLEAN") ) || Functions.TestForTrue ( Functions.BoolToInt (PVALUETYPE == "NUMBER") )) ))  ) ) 
                { 
                __context__.SourceCodeLine = 454;
                SPARAMS  .UpdateValue ( "\"params\": {\"property\": \"" + PPROPERTY + "\", \"value\": " + PVALUE + "}, "  ) ; 
                } 
            
            else 
                { 
                __context__.SourceCodeLine = 458;
                SPARAMS  .UpdateValue ( "\"params\": {\"property\": \"" + PPROPERTY + "\", \"value\": \"" + PVALUE + "\"}, "  ) ; 
                } 
            
            __context__.SourceCodeLine = 460;
            SID  .UpdateValue ( "\"id\": " + Functions.ItoA (  (int) ( PID ) ) + ""  ) ; 
            __context__.SourceCodeLine = 462;
            STX  .UpdateValue ( "{\"jsonrpc\": \"2.0\", " + SMETHOD + SPARAMS + SID + "}"  ) ; 
            __context__.SourceCodeLine = 464;
            SENDMESSAGE (  __context__ , STX) ; 
            
            }
            
        private void READONEPROPERTY (  SplusExecutionContext __context__, ushort PID , CrestronString PPROPERTY ) 
            { 
            CrestronString SHEADER;
            CrestronString SMETHOD;
            CrestronString SPARAMS;
            CrestronString SPROPERTY;
            CrestronString SID;
            CrestronString SENDBRACE;
            CrestronString STX;
            SHEADER  = new CrestronString( Crestron.Logos.SplusObjects.CrestronStringEncoding.eEncodingASCII, 50, this );
            SMETHOD  = new CrestronString( Crestron.Logos.SplusObjects.CrestronStringEncoding.eEncodingASCII, 50, this );
            SPARAMS  = new CrestronString( Crestron.Logos.SplusObjects.CrestronStringEncoding.eEncodingASCII, 150, this );
            SPROPERTY  = new CrestronString( Crestron.Logos.SplusObjects.CrestronStringEncoding.eEncodingASCII, 50, this );
            SID  = new CrestronString( Crestron.Logos.SplusObjects.CrestronStringEncoding.eEncodingASCII, 20, this );
            SENDBRACE  = new CrestronString( Crestron.Logos.SplusObjects.CrestronStringEncoding.eEncodingASCII, 1, this );
            STX  = new CrestronString( Crestron.Logos.SplusObjects.CrestronStringEncoding.eEncodingASCII, 350, this );
            
            
            __context__.SourceCodeLine = 472;
            SMETHOD  .UpdateValue ( "\"method\": \"property.get\", "  ) ; 
            __context__.SourceCodeLine = 473;
            SPARAMS  .UpdateValue ( "\"params\": {\"property\": \"" + PPROPERTY + "\"}, "  ) ; 
            __context__.SourceCodeLine = 474;
            SID  .UpdateValue ( "\"id\": " + Functions.ItoA (  (int) ( PID ) ) + ""  ) ; 
            __context__.SourceCodeLine = 476;
            STX  .UpdateValue ( "{\"jsonrpc\": \"2.0\", " + SMETHOD + SPARAMS + SID + "}"  ) ; 
            __context__.SourceCodeLine = 478;
            SENDMESSAGE (  __context__ , STX) ; 
            
            }
            
        private void SUBSCRIBETOPROPERTY (  SplusExecutionContext __context__, ushort PID , CrestronString PPROPERTY ) 
            { 
            CrestronString SHEADER;
            CrestronString SMETHOD;
            CrestronString SPARAMS;
            CrestronString SPROPERTY;
            CrestronString SID;
            CrestronString SENDBRACE;
            CrestronString STX;
            SHEADER  = new CrestronString( Crestron.Logos.SplusObjects.CrestronStringEncoding.eEncodingASCII, 50, this );
            SMETHOD  = new CrestronString( Crestron.Logos.SplusObjects.CrestronStringEncoding.eEncodingASCII, 50, this );
            SPARAMS  = new CrestronString( Crestron.Logos.SplusObjects.CrestronStringEncoding.eEncodingASCII, 150, this );
            SPROPERTY  = new CrestronString( Crestron.Logos.SplusObjects.CrestronStringEncoding.eEncodingASCII, 50, this );
            SID  = new CrestronString( Crestron.Logos.SplusObjects.CrestronStringEncoding.eEncodingASCII, 20, this );
            SENDBRACE  = new CrestronString( Crestron.Logos.SplusObjects.CrestronStringEncoding.eEncodingASCII, 1, this );
            STX  = new CrestronString( Crestron.Logos.SplusObjects.CrestronStringEncoding.eEncodingASCII, 350, this );
            
            
            __context__.SourceCodeLine = 486;
            SMETHOD  .UpdateValue ( "\"method\": \"property.subscribe\", "  ) ; 
            __context__.SourceCodeLine = 487;
            SPARAMS  .UpdateValue ( "\"params\": {\"property\": \"" + PPROPERTY + "\"}, "  ) ; 
            __context__.SourceCodeLine = 488;
            SID  .UpdateValue ( "\"id\": " + Functions.ItoA (  (int) ( PID ) ) + ""  ) ; 
            __context__.SourceCodeLine = 490;
            STX  .UpdateValue ( "{\"jsonrpc\": \"2.0\", " + SMETHOD + SPARAMS + SID + "}"  ) ; 
            __context__.SourceCodeLine = 492;
            SENDMESSAGE (  __context__ , STX) ; 
            
            }
            
        private CrestronString PARSERESPONSE (  SplusExecutionContext __context__, CrestronString PRESPONSE , CrestronString PPARSETYPE ) 
            { 
            CrestronString SRETURN;
            SRETURN  = new CrestronString( Crestron.Logos.SplusObjects.CrestronStringEncoding.eEncodingASCII, 2084, this );
            
            ushort I = 0;
            ushort ILENGTH = 0;
            ushort ISTARTPOSITION = 0;
            ushort IMIDCOUNT = 0;
            ushort INUMBEROFRESULTS = 0;
            
            
            __context__.SourceCodeLine = 500;
            INUMBEROFRESULTS = (ushort) ( 0 ) ; 
            __context__.SourceCodeLine = 501;
            ILENGTH = (ushort) ( Functions.Length( PRESPONSE ) ) ; 
            __context__.SourceCodeLine = 502;
            if ( Functions.TestForTrue  ( ( Functions.BoolToInt (PPARSETYPE == "ERROR"))  ) ) 
                { 
                __context__.SourceCodeLine = 504;
                ISTARTPOSITION = (ushort) ( (Functions.FindNoCase( "\u0022error\u0022:" , PRESPONSE ) + 8) ) ; 
                __context__.SourceCodeLine = 505;
                IMIDCOUNT = (ushort) ( (Functions.ReverseFind( "}" , PRESPONSE ) - ISTARTPOSITION) ) ; 
                __context__.SourceCodeLine = 506;
                SRETURN  .UpdateValue ( Functions.Mid ( PRESPONSE ,  (int) ( ISTARTPOSITION ) ,  (int) ( IMIDCOUNT ) )  ) ; 
                } 
            
            else 
                {
                __context__.SourceCodeLine = 510;
                if ( Functions.TestForTrue  ( ( Functions.BoolToInt (PPARSETYPE == "RESULT"))  ) ) 
                    { 
                    __context__.SourceCodeLine = 512;
                    ISTARTPOSITION = (ushort) ( (Functions.FindNoCase( "\u0022result\u0022:" , PRESPONSE ) + 9) ) ; 
                    __context__.SourceCodeLine = 513;
                    ushort __FN_FORSTART_VAL__1 = (ushort) ( ISTARTPOSITION ) ;
                    ushort __FN_FOREND_VAL__1 = (ushort)Functions.Length( PRESPONSE ); 
                    int __FN_FORSTEP_VAL__1 = (int)1; 
                    for ( I  = __FN_FORSTART_VAL__1; (__FN_FORSTEP_VAL__1 > 0)  ? ( (I  >= __FN_FORSTART_VAL__1) && (I  <= __FN_FOREND_VAL__1) ) : ( (I  <= __FN_FORSTART_VAL__1) && (I  >= __FN_FOREND_VAL__1) ) ; I  += (ushort)__FN_FORSTEP_VAL__1) 
                        { 
                        __context__.SourceCodeLine = 515;
                        if ( Functions.TestForTrue  ( ( Functions.BoolToInt (Functions.Mid( PRESPONSE , (int)( I ) , (int)( 1 ) ) == ":"))  ) ) 
                            {
                            __context__.SourceCodeLine = 515;
                            INUMBEROFRESULTS = (ushort) ( (INUMBEROFRESULTS + 1) ) ; 
                            }
                        
                        __context__.SourceCodeLine = 513;
                        } 
                    
                    __context__.SourceCodeLine = 517;
                    if ( Functions.TestForTrue  ( ( Functions.BoolToInt ( INUMBEROFRESULTS > 1 ))  ) ) 
                        { 
                        __context__.SourceCodeLine = 519;
                        IMIDCOUNT = (ushort) ( (ILENGTH - ISTARTPOSITION) ) ; 
                        __context__.SourceCodeLine = 520;
                        SRETURN  .UpdateValue ( Functions.Mid ( PRESPONSE ,  (int) ( ISTARTPOSITION ) ,  (int) ( IMIDCOUNT ) )  ) ; 
                        __context__.SourceCodeLine = 521;
                        ushort __FN_FORSTART_VAL__2 = (ushort) ( ISTARTPOSITION ) ;
                        ushort __FN_FOREND_VAL__2 = (ushort)INUMBEROFRESULTS; 
                        int __FN_FORSTEP_VAL__2 = (int)1; 
                        for ( I  = __FN_FORSTART_VAL__2; (__FN_FORSTEP_VAL__2 > 0)  ? ( (I  >= __FN_FORSTART_VAL__2) && (I  <= __FN_FOREND_VAL__2) ) : ( (I  <= __FN_FORSTART_VAL__2) && (I  >= __FN_FOREND_VAL__2) ) ; I  += (ushort)__FN_FORSTEP_VAL__2) 
                            { 
                            __context__.SourceCodeLine = 521;
                            } 
                        
                        } 
                    
                    else 
                        { 
                        __context__.SourceCodeLine = 528;
                        IMIDCOUNT = (ushort) ( (ILENGTH - ISTARTPOSITION) ) ; 
                        __context__.SourceCodeLine = 529;
                        SRETURN  .UpdateValue ( Functions.Mid ( PRESPONSE ,  (int) ( ISTARTPOSITION ) ,  (int) ( IMIDCOUNT ) )  ) ; 
                        } 
                    
                    } 
                
                else 
                    {
                    __context__.SourceCodeLine = 532;
                    if ( Functions.TestForTrue  ( ( Functions.BoolToInt (PPARSETYPE == "CHANGE"))  ) ) 
                        { 
                        __context__.SourceCodeLine = 534;
                        ISTARTPOSITION = (ushort) ( (Functions.Find( "[{" , PRESPONSE ) + 1) ) ; 
                        __context__.SourceCodeLine = 535;
                        ushort __FN_FORSTART_VAL__3 = (ushort) ( ISTARTPOSITION ) ;
                        ushort __FN_FOREND_VAL__3 = (ushort)Functions.Length( PRESPONSE ); 
                        int __FN_FORSTEP_VAL__3 = (int)1; 
                        for ( I  = __FN_FORSTART_VAL__3; (__FN_FORSTEP_VAL__3 > 0)  ? ( (I  >= __FN_FORSTART_VAL__3) && (I  <= __FN_FOREND_VAL__3) ) : ( (I  <= __FN_FORSTART_VAL__3) && (I  >= __FN_FOREND_VAL__3) ) ; I  += (ushort)__FN_FORSTEP_VAL__3) 
                            { 
                            __context__.SourceCodeLine = 537;
                            if ( Functions.TestForTrue  ( ( Functions.BoolToInt (Functions.Mid( PRESPONSE , (int)( I ) , (int)( 2 ) ) == "\u0022:"))  ) ) 
                                {
                                __context__.SourceCodeLine = 537;
                                INUMBEROFRESULTS = (ushort) ( (INUMBEROFRESULTS + 1) ) ; 
                                }
                            
                            __context__.SourceCodeLine = 535;
                            } 
                        
                        __context__.SourceCodeLine = 539;
                        if ( Functions.TestForTrue  ( ( Functions.BoolToInt ( INUMBEROFRESULTS > 1 ))  ) ) 
                            { 
                            __context__.SourceCodeLine = 541;
                            IMIDCOUNT = (ushort) ( (Functions.Length( PRESPONSE ) - ISTARTPOSITION) ) ; 
                            __context__.SourceCodeLine = 542;
                            SRETURN  .UpdateValue ( Functions.Mid ( PRESPONSE ,  (int) ( ISTARTPOSITION ) ,  (int) ( IMIDCOUNT ) )  ) ; 
                            __context__.SourceCodeLine = 543;
                            ushort __FN_FORSTART_VAL__4 = (ushort) ( ISTARTPOSITION ) ;
                            ushort __FN_FOREND_VAL__4 = (ushort)INUMBEROFRESULTS; 
                            int __FN_FORSTEP_VAL__4 = (int)1; 
                            for ( I  = __FN_FORSTART_VAL__4; (__FN_FORSTEP_VAL__4 > 0)  ? ( (I  >= __FN_FORSTART_VAL__4) && (I  <= __FN_FOREND_VAL__4) ) : ( (I  <= __FN_FORSTART_VAL__4) && (I  >= __FN_FOREND_VAL__4) ) ; I  += (ushort)__FN_FORSTEP_VAL__4) 
                                { 
                                __context__.SourceCodeLine = 543;
                                } 
                            
                            } 
                        
                        else 
                            { 
                            __context__.SourceCodeLine = 550;
                            ISTARTPOSITION = (ushort) ( (Functions.ReverseFind( "\u0022:" , PRESPONSE ) + 2) ) ; 
                            __context__.SourceCodeLine = 551;
                            IMIDCOUNT = (ushort) ( (Functions.ReverseFind( "}]}}" , PRESPONSE ) - ISTARTPOSITION) ) ; 
                            __context__.SourceCodeLine = 552;
                            SRETURN  .UpdateValue ( Functions.Mid ( PRESPONSE ,  (int) ( ISTARTPOSITION ) ,  (int) ( IMIDCOUNT ) )  ) ; 
                            } 
                        
                        } 
                    
                    }
                
                }
            
            __context__.SourceCodeLine = 555;
            return ( SRETURN ) ; 
            
            }
            
        private CrestronString REMOVEQUOTES (  SplusExecutionContext __context__, CrestronString S ) 
            { 
            CrestronString SBIN;
            CrestronString SNEW;
            SBIN  = new CrestronString( Crestron.Logos.SplusObjects.CrestronStringEncoding.eEncodingASCII, 20, this );
            SNEW  = new CrestronString( Crestron.Logos.SplusObjects.CrestronStringEncoding.eEncodingASCII, 50, this );
            
            
            __context__.SourceCodeLine = 562;
            SBIN  .UpdateValue ( Functions.Remove ( "\u0022" , S )  ) ; 
            __context__.SourceCodeLine = 563;
            SNEW  .UpdateValue ( Functions.Mid ( S ,  (int) ( 1 ) ,  (int) ( (Functions.Length( S ) - 1) ) )  ) ; 
            __context__.SourceCodeLine = 564;
            return ( SNEW ) ; 
            
            }
            
        private CrestronString REMOVESPACES (  SplusExecutionContext __context__, CrestronString S ) 
            { 
            CrestronString SNEW;
            CrestronString STEMP;
            SNEW  = new CrestronString( Crestron.Logos.SplusObjects.CrestronStringEncoding.eEncodingASCII, 30, this );
            STEMP  = new CrestronString( Crestron.Logos.SplusObjects.CrestronStringEncoding.eEncodingASCII, 1, this );
            
            ushort I = 0;
            ushort X = 0;
            
            
            __context__.SourceCodeLine = 572;
            ushort __FN_FORSTART_VAL__1 = (ushort) ( 1 ) ;
            ushort __FN_FOREND_VAL__1 = (ushort)Functions.Length( S ); 
            int __FN_FORSTEP_VAL__1 = (int)1; 
            for ( I  = __FN_FORSTART_VAL__1; (__FN_FORSTEP_VAL__1 > 0)  ? ( (I  >= __FN_FORSTART_VAL__1) && (I  <= __FN_FOREND_VAL__1) ) : ( (I  <= __FN_FORSTART_VAL__1) && (I  >= __FN_FOREND_VAL__1) ) ; I  += (ushort)__FN_FORSTEP_VAL__1) 
                { 
                __context__.SourceCodeLine = 574;
                if ( Functions.TestForTrue  ( ( Functions.BoolToInt (Byte( S , (int)( I ) ) != 32))  ) ) 
                    { 
                    __context__.SourceCodeLine = 576;
                    STEMP  .UpdateValue ( Functions.Chr (  (int) ( Byte( S , (int)( I ) ) ) )  ) ; 
                    __context__.SourceCodeLine = 577;
                    SNEW  .UpdateValue ( SNEW + STEMP  ) ; 
                    } 
                
                __context__.SourceCodeLine = 572;
                } 
            
            __context__.SourceCodeLine = 581;
            return ( SNEW ) ; 
            
            }
            
        private ushort BRACECOUNT (  SplusExecutionContext __context__, CrestronString PBUFFER ) 
            { 
            ushort I = 0;
            ushort ICOUNT = 0;
            
            
            __context__.SourceCodeLine = 588;
            ICOUNT = (ushort) ( 0 ) ; 
            __context__.SourceCodeLine = 589;
            ushort __FN_FORSTART_VAL__1 = (ushort) ( 1 ) ;
            ushort __FN_FOREND_VAL__1 = (ushort)Functions.Length( PBUFFER ); 
            int __FN_FORSTEP_VAL__1 = (int)1; 
            for ( I  = __FN_FORSTART_VAL__1; (__FN_FORSTEP_VAL__1 > 0)  ? ( (I  >= __FN_FORSTART_VAL__1) && (I  <= __FN_FOREND_VAL__1) ) : ( (I  <= __FN_FORSTART_VAL__1) && (I  >= __FN_FOREND_VAL__1) ) ; I  += (ushort)__FN_FORSTEP_VAL__1) 
                { 
                __context__.SourceCodeLine = 591;
                if ( Functions.TestForTrue  ( ( Functions.BoolToInt (Functions.Mid( PBUFFER , (int)( I ) , (int)( 1 ) ) == "{"))  ) ) 
                    {
                    __context__.SourceCodeLine = 591;
                    ICOUNT = (ushort) ( (ICOUNT + 1) ) ; 
                    }
                
                else 
                    {
                    __context__.SourceCodeLine = 592;
                    if ( Functions.TestForTrue  ( ( Functions.BoolToInt (Functions.Mid( PBUFFER , (int)( I ) , (int)( 1 ) ) == "}"))  ) ) 
                        {
                        __context__.SourceCodeLine = 592;
                        ICOUNT = (ushort) ( (ICOUNT - 1) ) ; 
                        }
                    
                    }
                
                __context__.SourceCodeLine = 593;
                if ( Functions.TestForTrue  ( ( Functions.BoolToInt (ICOUNT == 0))  ) ) 
                    {
                    __context__.SourceCodeLine = 593;
                    break ; 
                    }
                
                __context__.SourceCodeLine = 589;
                } 
            
            __context__.SourceCodeLine = 595;
            return (ushort)( ICOUNT) ; 
            
            }
            
        private CrestronString EXTRACTSIGNALINFODATAITEM (  SplusExecutionContext __context__, CrestronString PDETECTEDSIGNALRESPONSE , CrestronString PDATAITEMNAME ) 
            { 
            CrestronString SDATAITEMVALUE;
            CrestronString SSEARCHSTRING;
            SDATAITEMVALUE  = new CrestronString( Crestron.Logos.SplusObjects.CrestronStringEncoding.eEncodingASCII, 50, this );
            SSEARCHSTRING  = new CrestronString( Crestron.Logos.SplusObjects.CrestronStringEncoding.eEncodingASCII, 50, this );
            
            ushort ISTARTPOSITION = 0;
            ushort IENDPOSITION = 0;
            ushort ICOUNT = 0;
            
            
            __context__.SourceCodeLine = 603;
            MakeString ( SSEARCHSTRING , "\u0022{0}\u0022:", PDATAITEMNAME ) ; 
            __context__.SourceCodeLine = 605;
            ISTARTPOSITION = (ushort) ( (Functions.FindNoCase( SSEARCHSTRING , PDETECTEDSIGNALRESPONSE ) + Functions.Length( SSEARCHSTRING )) ) ; 
            __context__.SourceCodeLine = 607;
            if ( Functions.TestForTrue  ( ( Functions.Find( "," , PDETECTEDSIGNALRESPONSE , ISTARTPOSITION ))  ) ) 
                { 
                __context__.SourceCodeLine = 609;
                IENDPOSITION = (ushort) ( Functions.Find( "," , PDETECTEDSIGNALRESPONSE , ISTARTPOSITION ) ) ; 
                } 
            
            else 
                { 
                __context__.SourceCodeLine = 613;
                IENDPOSITION = (ushort) ( Functions.Find( "}" , PDETECTEDSIGNALRESPONSE , ISTARTPOSITION ) ) ; 
                } 
            
            __context__.SourceCodeLine = 615;
            SDATAITEMVALUE  .UpdateValue ( REMOVEQUOTES (  __context__ , Functions.Mid( PDETECTEDSIGNALRESPONSE , (int)( ISTARTPOSITION ) , (int)( (IENDPOSITION - ISTARTPOSITION) ) ))  ) ; 
            __context__.SourceCodeLine = 617;
            return ( SDATAITEMVALUE ) ; 
            
            }
            
        private ushort EXTRACTSIGNALINFODATAITEMINTEGER (  SplusExecutionContext __context__, CrestronString PDETECTEDSIGNALRESPONSE , CrestronString PDATAITEMNAME ) 
            { 
            CrestronString SDATAITEMVALUE;
            CrestronString SSEARCHSTRING;
            SDATAITEMVALUE  = new CrestronString( Crestron.Logos.SplusObjects.CrestronStringEncoding.eEncodingASCII, 50, this );
            SSEARCHSTRING  = new CrestronString( Crestron.Logos.SplusObjects.CrestronStringEncoding.eEncodingASCII, 50, this );
            
            ushort ISTARTPOSITION = 0;
            ushort IENDPOSITION = 0;
            ushort ICOUNT = 0;
            ushort IDATAITEMVALUE = 0;
            
            
            __context__.SourceCodeLine = 625;
            MakeString ( SSEARCHSTRING , "\u0022{0}\u0022:", PDATAITEMNAME ) ; 
            __context__.SourceCodeLine = 627;
            ISTARTPOSITION = (ushort) ( (Functions.FindNoCase( SSEARCHSTRING , PDETECTEDSIGNALRESPONSE ) + Functions.Length( SSEARCHSTRING )) ) ; 
            __context__.SourceCodeLine = 629;
            if ( Functions.TestForTrue  ( ( Functions.Find( "," , PDETECTEDSIGNALRESPONSE , ISTARTPOSITION ))  ) ) 
                { 
                __context__.SourceCodeLine = 631;
                IENDPOSITION = (ushort) ( Functions.Find( "," , PDETECTEDSIGNALRESPONSE , ISTARTPOSITION ) ) ; 
                } 
            
            else 
                { 
                __context__.SourceCodeLine = 635;
                IENDPOSITION = (ushort) ( Functions.Find( "}" , PDETECTEDSIGNALRESPONSE , ISTARTPOSITION ) ) ; 
                } 
            
            __context__.SourceCodeLine = 637;
            SDATAITEMVALUE  .UpdateValue ( Functions.Mid ( PDETECTEDSIGNALRESPONSE ,  (int) ( ISTARTPOSITION ) ,  (int) ( (IENDPOSITION - ISTARTPOSITION) ) )  ) ; 
            __context__.SourceCodeLine = 638;
            IDATAITEMVALUE = (ushort) ( Functions.Atoi( SDATAITEMVALUE ) ) ; 
            __context__.SourceCodeLine = 640;
            return (ushort)( IDATAITEMVALUE) ; 
            
            }
            
        private CrestronString EXTRACTSIGNALINFO (  SplusExecutionContext __context__, CrestronString PRESPONSE , CrestronString PRESPONSETYPE ) 
            { 
            CrestronString SDETECTEDSIGNALRESPONSE;
            CrestronString SDETECTEDSIGNAL;
            CrestronString SMASTERINGLUMINANCE;
            CrestronString SVERTICALRESOLUTION;
            CrestronString SCOLORSPACE;
            CrestronString SCONTENTASPECTRATIO;
            CrestronString SGAMMATYPE;
            CrestronString SCOLORPRIMARIESMODE;
            SDETECTEDSIGNALRESPONSE  = new CrestronString( Crestron.Logos.SplusObjects.CrestronStringEncoding.eEncodingASCII, 2084, this );
            SDETECTEDSIGNAL  = new CrestronString( Crestron.Logos.SplusObjects.CrestronStringEncoding.eEncodingASCII, 50, this );
            SMASTERINGLUMINANCE  = new CrestronString( Crestron.Logos.SplusObjects.CrestronStringEncoding.eEncodingASCII, 20, this );
            SVERTICALRESOLUTION  = new CrestronString( Crestron.Logos.SplusObjects.CrestronStringEncoding.eEncodingASCII, 20, this );
            SCOLORSPACE  = new CrestronString( Crestron.Logos.SplusObjects.CrestronStringEncoding.eEncodingASCII, 20, this );
            SCONTENTASPECTRATIO  = new CrestronString( Crestron.Logos.SplusObjects.CrestronStringEncoding.eEncodingASCII, 20, this );
            SGAMMATYPE  = new CrestronString( Crestron.Logos.SplusObjects.CrestronStringEncoding.eEncodingASCII, 20, this );
            SCOLORPRIMARIESMODE  = new CrestronString( Crestron.Logos.SplusObjects.CrestronStringEncoding.eEncodingASCII, 20, this );
            
            ushort IMASTERINGLUMINANCE = 0;
            ushort IVERTICALRESOLUTION = 0;
            
            
            __context__.SourceCodeLine = 648;
            SDETECTEDSIGNALRESPONSE  .UpdateValue ( PARSERESPONSE (  __context__ , PRESPONSE, PRESPONSETYPE)  ) ; 
            __context__.SourceCodeLine = 651;
            SDETECTEDSIGNAL  .UpdateValue ( EXTRACTSIGNALINFODATAITEM (  __context__ , SDETECTEDSIGNALRESPONSE, "name")  ) ; 
            __context__.SourceCodeLine = 652;
            SODETECTEDSIGNAL  .UpdateValue ( SDETECTEDSIGNAL  ) ; 
            __context__.SourceCodeLine = 655;
            SCONTENTASPECTRATIO  .UpdateValue ( EXTRACTSIGNALINFODATAITEM (  __context__ , SDETECTEDSIGNALRESPONSE, "content_aspect_ratio")  ) ; 
            __context__.SourceCodeLine = 656;
            SOCONTENTASPECTRATIO  .UpdateValue ( SCONTENTASPECTRATIO  ) ; 
            __context__.SourceCodeLine = 659;
            SCOLORSPACE  .UpdateValue ( EXTRACTSIGNALINFODATAITEM (  __context__ , SDETECTEDSIGNALRESPONSE, "color_space")  ) ; 
            __context__.SourceCodeLine = 660;
            SOCOLORSPACE  .UpdateValue ( SCOLORSPACE  ) ; 
            __context__.SourceCodeLine = 663;
            SMASTERINGLUMINANCE  .UpdateValue ( EXTRACTSIGNALINFODATAITEM (  __context__ , SDETECTEDSIGNALRESPONSE, "mastering_luminance")  ) ; 
            __context__.SourceCodeLine = 664;
            IMASTERINGLUMINANCE = (ushort) ( Functions.Atoi( SMASTERINGLUMINANCE ) ) ; 
            __context__.SourceCodeLine = 665;
            SOMASTERINGLUMINANCE  .UpdateValue ( SMASTERINGLUMINANCE  ) ; 
            __context__.SourceCodeLine = 668;
            SGAMMATYPE  .UpdateValue ( EXTRACTSIGNALINFODATAITEM (  __context__ , SDETECTEDSIGNALRESPONSE, "gamma_type")  ) ; 
            __context__.SourceCodeLine = 669;
            SOGAMMATYPE  .UpdateValue ( SGAMMATYPE  ) ; 
            __context__.SourceCodeLine = 672;
            IVERTICALRESOLUTION = (ushort) ( EXTRACTSIGNALINFODATAITEMINTEGER( __context__ , SDETECTEDSIGNALRESPONSE , "vertical_resolution" ) ) ; 
            __context__.SourceCodeLine = 673;
            SVERTICALRESOLUTION  .UpdateValue ( Functions.ItoA (  (int) ( IVERTICALRESOLUTION ) )  ) ; 
            __context__.SourceCodeLine = 674;
            if ( Functions.TestForTrue  ( ( Functions.BoolToInt (GITRACELOGSENABLED == 1))  ) ) 
                {
                __context__.SourceCodeLine = 674;
                Trace( "VERTICAL RESOLUTION AS INTEGER = {0:d}", (short)IVERTICALRESOLUTION) ; 
                }
            
            __context__.SourceCodeLine = 675;
            if ( Functions.TestForTrue  ( ( Functions.BoolToInt (GITRACELOGSENABLED == 1))  ) ) 
                {
                __context__.SourceCodeLine = 675;
                Trace( "VERTICAL RESOLUTION AS STRING = {0}", SVERTICALRESOLUTION ) ; 
                }
            
            __context__.SourceCodeLine = 676;
            SOVERTICALRESOLUTION  .UpdateValue ( SVERTICALRESOLUTION  ) ; 
            __context__.SourceCodeLine = 679;
            if ( Functions.TestForTrue  ( ( (Functions.BoolToInt (SCOLORSPACE == "REC2020") & Functions.BoolToInt (SDETECTEDSIGNAL != "")))  ) ) 
                { 
                __context__.SourceCodeLine = 682;
                if ( Functions.TestForTrue  ( ( Functions.BoolToInt (GIGAMUTREMAPPINGENABLED == 0))  ) ) 
                    { 
                    __context__.SourceCodeLine = 685;
                    WRITEONEPROPERTY (  __context__ , (ushort)( 131 ), "image.color.p7.custom.mode", "HDR", "STRING") ; 
                    } 
                
                __context__.SourceCodeLine = 688;
                WRITEONEPROPERTY (  __context__ , (ushort)( 140 ), "optics.filter.dci.target", "In", "STRING") ; 
                } 
            
            else 
                {
                __context__.SourceCodeLine = 692;
                if ( Functions.TestForTrue  ( ( (Functions.BoolToInt (SCOLORSPACE != "REC2020") & Functions.BoolToInt (SDETECTEDSIGNAL != "")))  ) ) 
                    { 
                    __context__.SourceCodeLine = 695;
                    if ( Functions.TestForTrue  ( ( Functions.BoolToInt (GIGAMUTREMAPPINGENABLED == 0))  ) ) 
                        { 
                        __context__.SourceCodeLine = 698;
                        WRITEONEPROPERTY (  __context__ , (ushort)( 132 ), "image.color.p7.custom.mode", "Rec. 709", "STRING") ; 
                        } 
                    
                    __context__.SourceCodeLine = 701;
                    WRITEONEPROPERTY (  __context__ , (ushort)( 141 ), "optics.filter.dci.target", "Out", "STRING") ; 
                    } 
                
                }
            
            
            return ""; // default return value (none specified in module)
            }
            
        private CrestronString EXTRACTCROPPINGVALUE (  SplusExecutionContext __context__, CrestronString PRESPONSE , CrestronString PPOSITION ) 
            { 
            CrestronString SCROPPINGVALUE;
            CrestronString SSEARCHSTRING;
            SCROPPINGVALUE  = new CrestronString( Crestron.Logos.SplusObjects.CrestronStringEncoding.eEncodingASCII, 10, this );
            SSEARCHSTRING  = new CrestronString( Crestron.Logos.SplusObjects.CrestronStringEncoding.eEncodingASCII, 30, this );
            
            ushort ISTARTPOSITION = 0;
            ushort IENDPOSITION = 0;
            ushort ICOUNT = 0;
            
            
            __context__.SourceCodeLine = 710;
            MakeString ( SSEARCHSTRING , "\u0022{0}\u0022:", PPOSITION ) ; 
            __context__.SourceCodeLine = 712;
            ISTARTPOSITION = (ushort) ( (Functions.FindNoCase( SSEARCHSTRING , PRESPONSE ) + Functions.Length( SSEARCHSTRING )) ) ; 
            __context__.SourceCodeLine = 714;
            if ( Functions.TestForTrue  ( ( Functions.Find( "," , PRESPONSE , ISTARTPOSITION ))  ) ) 
                { 
                __context__.SourceCodeLine = 716;
                IENDPOSITION = (ushort) ( Functions.Find( "," , PRESPONSE , ISTARTPOSITION ) ) ; 
                } 
            
            else 
                { 
                __context__.SourceCodeLine = 720;
                IENDPOSITION = (ushort) ( Functions.Find( "}" , PRESPONSE , ISTARTPOSITION ) ) ; 
                } 
            
            __context__.SourceCodeLine = 722;
            ICOUNT = (ushort) ( (IENDPOSITION - ISTARTPOSITION) ) ; 
            __context__.SourceCodeLine = 723;
            SCROPPINGVALUE  .UpdateValue ( Functions.Mid ( PRESPONSE ,  (int) ( ISTARTPOSITION ) ,  (int) ( ICOUNT ) )  ) ; 
            __context__.SourceCodeLine = 724;
            return ( SCROPPINGVALUE ) ; 
            
            }
            
        private ushort CALCULATEHOURS (  SplusExecutionContext __context__, ushort IMINUITES ) 
            { 
            ushort IHOURS = 0;
            ushort IMODULUS = 0;
            
            CrestronString SLASERHOURS;
            SLASERHOURS  = new CrestronString( Crestron.Logos.SplusObjects.CrestronStringEncoding.eEncodingASCII, 100, this );
            
            
            __context__.SourceCodeLine = 732;
            IHOURS = (ushort) ( (IMINUITES / 60) ) ; 
            __context__.SourceCodeLine = 733;
            IMODULUS = (ushort) ( Mod( IMINUITES , 60 ) ) ; 
            __context__.SourceCodeLine = 734;
            if ( Functions.TestForTrue  ( ( Functions.BoolToInt ( IMODULUS >= 30 ))  ) ) 
                { 
                __context__.SourceCodeLine = 736;
                return (ushort)( (IHOURS + 1)) ; 
                } 
            
            else 
                { 
                __context__.SourceCodeLine = 740;
                return (ushort)( IHOURS) ; 
                } 
            
            
            return 0; // default return value (none specified in module)
            }
            
        private void PROCESSRESPONSE (  SplusExecutionContext __context__, CrestronString PRESPONSE ) 
            { 
            CrestronString SDETECTEDSIGNAL;
            CrestronString SPROPERTY;
            CrestronString SDETECTEDSOURCE;
            SDETECTEDSIGNAL  = new CrestronString( Crestron.Logos.SplusObjects.CrestronStringEncoding.eEncodingASCII, 100, this );
            SPROPERTY  = new CrestronString( Crestron.Logos.SplusObjects.CrestronStringEncoding.eEncodingASCII, 100, this );
            SDETECTEDSOURCE  = new CrestronString( Crestron.Logos.SplusObjects.CrestronStringEncoding.eEncodingASCII, 50, this );
            
            ushort I = 0;
            ushort ISTARTPOSITION = 0;
            ushort IENDPOSITION = 0;
            
            
            __context__.SourceCodeLine = 749;
            if ( Functions.TestForTrue  ( ( Functions.BoolToInt (Functions.Find( "\u0022error\u0022:" , PRESPONSE , 1 ) != 0))  ) ) 
                { 
                __context__.SourceCodeLine = 751;
                if ( Functions.TestForTrue  ( ( Functions.BoolToInt (GITRACELOGSENABLED == 1))  ) ) 
                    {
                    __context__.SourceCodeLine = 751;
                    Trace( "ERROR RESONSE: {0}\r\n", PARSERESPONSE (  __context__ , PRESPONSE, "ERROR") ) ; 
                    }
                
                } 
            
            else 
                {
                __context__.SourceCodeLine = 753;
                if ( Functions.TestForTrue  ( ( Functions.Find( "property.changed" , PRESPONSE ))  ) ) 
                    { 
                    __context__.SourceCodeLine = 755;
                    if ( Functions.TestForTrue  ( ( Functions.BoolToInt (GITRACELOGSENABLED == 1))  ) ) 
                        {
                        __context__.SourceCodeLine = 755;
                        Trace( "PROPERTY CHANGED RESONSE : {0}\r\n", PRESPONSE ) ; 
                        }
                    
                    __context__.SourceCodeLine = 756;
                    if ( Functions.TestForTrue  ( ( Functions.Find( "system.state" , PRESPONSE ))  ) ) 
                        { 
                        __context__.SourceCodeLine = 758;
                        SOSYSTEMSTATE  .UpdateValue ( PARSERESPONSE (  __context__ , PRESPONSE, "CHANGE")  ) ; 
                        } 
                    
                    __context__.SourceCodeLine = 760;
                    if ( Functions.TestForTrue  ( ( Functions.Find( "image.window.main.source" , PRESPONSE ))  ) ) 
                        { 
                        __context__.SourceCodeLine = 762;
                        SDETECTEDSOURCE  .UpdateValue ( REMOVEQUOTES (  __context__ , PARSERESPONSE( __context__ , PRESPONSE , "CHANGE" ))  ) ; 
                        __context__.SourceCodeLine = 763;
                        if ( Functions.TestForTrue  ( ( Functions.BoolToInt (SDETECTEDSOURCE != ""))  ) ) 
                            { 
                            __context__.SourceCodeLine = 765;
                            GSCURRENTSOURCE  .UpdateValue ( SDETECTEDSOURCE  ) ; 
                            __context__.SourceCodeLine = 766;
                            SOINPUTSOURCE  .UpdateValue ( GSCURRENTSOURCE  ) ; 
                            __context__.SourceCodeLine = 767;
                            if ( Functions.TestForTrue  ( ( Functions.BoolToInt ( (Functions.TestForTrue ( Functions.BoolToInt (GSCURRENTSOURCE == "Dual DVI sequential") ) || Functions.TestForTrue ( Functions.BoolToInt (GSCURRENTSOURCE == "Dual DVI columns") )) ))  ) ) 
                                { 
                                __context__.SourceCodeLine = 769;
                                READONEPROPERTY (  __context__ , (ushort)( 111 ), "image.connector.dvi1.detectedsignal") ; 
                                } 
                            
                            else 
                                {
                                __context__.SourceCodeLine = 771;
                                if ( Functions.TestForTrue  ( ( Functions.BoolToInt ( (Functions.TestForTrue ( Functions.BoolToInt (GSCURRENTSOURCE == "Dual DisplayPort sequential") ) || Functions.TestForTrue ( Functions.BoolToInt (GSCURRENTSOURCE == "Dual DisplayPort columns") )) ))  ) ) 
                                    { 
                                    __context__.SourceCodeLine = 773;
                                    READONEPROPERTY (  __context__ , (ushort)( 111 ), "image.connector.displayport1.detectedsignal") ; 
                                    } 
                                
                                else 
                                    { 
                                    __context__.SourceCodeLine = 777;
                                    MakeString ( SPROPERTY , "image.connector.{0}.detectedsignal", Functions.Lower ( REMOVESPACES (  __context__ , GSCURRENTSOURCE) ) ) ; 
                                    __context__.SourceCodeLine = 778;
                                    READONEPROPERTY (  __context__ , (ushort)( 111 ), SPROPERTY) ; 
                                    } 
                                
                                }
                            
                            } 
                        
                        } 
                    
                    __context__.SourceCodeLine = 782;
                    if ( Functions.TestForTrue  ( ( Functions.BoolToInt ( (Functions.TestForTrue ( Functions.Find( "image.connector.dvi1.detectedsignal" , PRESPONSE ) ) && Functions.TestForTrue ( Functions.BoolToInt ( (Functions.TestForTrue ( Functions.BoolToInt ( (Functions.TestForTrue ( Functions.BoolToInt (GSCURRENTSOURCE == "DVI 1") ) || Functions.TestForTrue ( Functions.BoolToInt (GSCURRENTSOURCE == "Dual DVI sequential") )) ) ) || Functions.TestForTrue ( Functions.BoolToInt (GSCURRENTSOURCE == "Dual DVI columns") )) ) )) ))  ) ) 
                        { 
                        __context__.SourceCodeLine = 784;
                        EXTRACTSIGNALINFO (  __context__ , PRESPONSE, "CHANGE") ; 
                        } 
                    
                    __context__.SourceCodeLine = 786;
                    if ( Functions.TestForTrue  ( ( Functions.BoolToInt ( (Functions.TestForTrue ( Functions.Find( "image.connector.dvi2.detectedsignal" , PRESPONSE ) ) && Functions.TestForTrue ( Functions.BoolToInt (GSCURRENTSOURCE == "DVI 2") )) ))  ) ) 
                        { 
                        __context__.SourceCodeLine = 788;
                        EXTRACTSIGNALINFO (  __context__ , PRESPONSE, "CHANGE") ; 
                        } 
                    
                    __context__.SourceCodeLine = 790;
                    if ( Functions.TestForTrue  ( ( Functions.BoolToInt ( (Functions.TestForTrue ( Functions.Find( "image.connector.displayport1.detectedsignal" , PRESPONSE ) ) && Functions.TestForTrue ( Functions.BoolToInt ( (Functions.TestForTrue ( Functions.BoolToInt ( (Functions.TestForTrue ( Functions.BoolToInt (GSCURRENTSOURCE == "DisplayPort 1") ) || Functions.TestForTrue ( Functions.BoolToInt (GSCURRENTSOURCE == "Dual DisplayPort sequential") )) ) ) || Functions.TestForTrue ( Functions.BoolToInt (GSCURRENTSOURCE == "Dual DisplayPort columns") )) ) )) ))  ) ) 
                        { 
                        __context__.SourceCodeLine = 792;
                        EXTRACTSIGNALINFO (  __context__ , PRESPONSE, "CHANGE") ; 
                        } 
                    
                    __context__.SourceCodeLine = 794;
                    if ( Functions.TestForTrue  ( ( Functions.BoolToInt ( (Functions.TestForTrue ( Functions.Find( "image.connector.displayport2.detectedsignal" , PRESPONSE ) ) && Functions.TestForTrue ( Functions.BoolToInt (GSCURRENTSOURCE == "DisplayPort 2") )) ))  ) ) 
                        { 
                        __context__.SourceCodeLine = 796;
                        EXTRACTSIGNALINFO (  __context__ , PRESPONSE, "CHANGE") ; 
                        } 
                    
                    __context__.SourceCodeLine = 798;
                    if ( Functions.TestForTrue  ( ( Functions.BoolToInt ( (Functions.TestForTrue ( Functions.Find( "image.connector.hdmi.detectedsignal" , PRESPONSE ) ) && Functions.TestForTrue ( Functions.BoolToInt (GSCURRENTSOURCE == "HDMI") )) ))  ) ) 
                        { 
                        __context__.SourceCodeLine = 800;
                        EXTRACTSIGNALINFO (  __context__ , PRESPONSE, "CHANGE") ; 
                        } 
                    
                    __context__.SourceCodeLine = 802;
                    if ( Functions.TestForTrue  ( ( Functions.BoolToInt ( (Functions.TestForTrue ( Functions.Find( "image.connector.hdbaset.detectedsignal" , PRESPONSE ) ) && Functions.TestForTrue ( Functions.BoolToInt (GSCURRENTSOURCE == "HDBaseT") )) ))  ) ) 
                        { 
                        __context__.SourceCodeLine = 804;
                        EXTRACTSIGNALINFO (  __context__ , PRESPONSE, "CHANGE") ; 
                        } 
                    
                    __context__.SourceCodeLine = 806;
                    if ( Functions.TestForTrue  ( ( Functions.BoolToInt ( (Functions.TestForTrue ( Functions.Find( "image.connector.sdi.detectedsignal" , PRESPONSE ) ) && Functions.TestForTrue ( Functions.BoolToInt (GSCURRENTSOURCE == "SDI") )) ))  ) ) 
                        { 
                        __context__.SourceCodeLine = 808;
                        EXTRACTSIGNALINFO (  __context__ , PRESPONSE, "CHANGE") ; 
                        } 
                    
                    __context__.SourceCodeLine = 811;
                    if ( Functions.TestForTrue  ( ( Functions.Find( "image.window.main.center16_9.enable" , PRESPONSE ))  ) ) 
                        { 
                        __context__.SourceCodeLine = 811;
                        SOCENTER16BY9STATUS  .UpdateValue ( PARSERESPONSE (  __context__ , PRESPONSE, "CHANGE")  ) ; 
                        } 
                    
                    __context__.SourceCodeLine = 812;
                    if ( Functions.TestForTrue  ( ( Functions.Find( "image.window.main.center16_9.factor" , PRESPONSE ))  ) ) 
                        { 
                        __context__.SourceCodeLine = 812;
                        SOCENTER16BY9FACTOR  .UpdateValue ( PARSERESPONSE (  __context__ , PRESPONSE, "CHANGE")  ) ; 
                        } 
                    
                    __context__.SourceCodeLine = 813;
                    if ( Functions.TestForTrue  ( ( Functions.Find( "image.window.main.cropping.mode" , PRESPONSE ))  ) ) 
                        { 
                        __context__.SourceCodeLine = 813;
                        SOCROPPINGMODE  .UpdateValue ( PARSERESPONSE (  __context__ , PRESPONSE, "CHANGE")  ) ; 
                        } 
                    
                    __context__.SourceCodeLine = 814;
                    if ( Functions.TestForTrue  ( ( Functions.Find( "image.window.main.cropping.aspectratio" , PRESPONSE ))  ) ) 
                        { 
                        __context__.SourceCodeLine = 814;
                        SOCROPPINGASPECTRATIO  .UpdateValue ( PARSERESPONSE (  __context__ , PRESPONSE, "CHANGE")  ) ; 
                        } 
                    
                    __context__.SourceCodeLine = 815;
                    if ( Functions.TestForTrue  ( ( Functions.Find( "image.window.main.cropping.manual" , PRESPONSE ))  ) ) 
                        { 
                        __context__.SourceCodeLine = 817;
                        AOCROPPINGMANUALLEFT  .Value = (ushort) ( Functions.Atoi( EXTRACTCROPPINGVALUE( __context__ , PRESPONSE , "LEFT" ) ) ) ; 
                        __context__.SourceCodeLine = 818;
                        AOCROPPINGMANUALRIGHT  .Value = (ushort) ( Functions.Atoi( EXTRACTCROPPINGVALUE( __context__ , PRESPONSE , "RIGHT" ) ) ) ; 
                        __context__.SourceCodeLine = 819;
                        AOCROPPINGMANUALTOP  .Value = (ushort) ( Functions.Atoi( EXTRACTCROPPINGVALUE( __context__ , PRESPONSE , "TOP" ) ) ) ; 
                        __context__.SourceCodeLine = 820;
                        AOCROPPINGMANUALBOTTOM  .Value = (ushort) ( Functions.Atoi( EXTRACTCROPPINGVALUE( __context__ , PRESPONSE , "BOTTOM" ) ) ) ; 
                        } 
                    
                    __context__.SourceCodeLine = 822;
                    if ( Functions.TestForTrue  ( ( Functions.Find( "image.color.p7.custom.mode" , PRESPONSE ))  ) ) 
                        { 
                        __context__.SourceCodeLine = 822;
                        SOP7MODE  .UpdateValue ( REMOVEQUOTES (  __context__ , PARSERESPONSE( __context__ , PRESPONSE , "CHANGE" ))  ) ; 
                        } 
                    
                    __context__.SourceCodeLine = 823;
                    if ( Functions.TestForTrue  ( ( Functions.Find( "screen.hdrboost" , PRESPONSE ))  ) ) 
                        { 
                        __context__.SourceCodeLine = 823;
                        SOHDRBOOSTVALUEFB  .UpdateValue ( PARSERESPONSE (  __context__ , PRESPONSE, "CHANGE")  ) ; 
                        } 
                    
                    __context__.SourceCodeLine = 824;
                    if ( Functions.TestForTrue  ( ( Functions.Find( "image.window.main.cropping.contentbasedautocropping" , PRESPONSE ))  ) ) 
                        { 
                        __context__.SourceCodeLine = 824;
                        SOCONTENTBASEDAUTOCROPPINGSTATUS  .UpdateValue ( PARSERESPONSE (  __context__ , PRESPONSE, "CHANGE")  ) ; 
                        } 
                    
                    __context__.SourceCodeLine = 825;
                    if ( Functions.TestForTrue  ( ( Functions.Find( "optics.shutter.position" , PRESPONSE ))  ) ) 
                        { 
                        __context__.SourceCodeLine = 825;
                        SOSHUTTERPOSITION  .UpdateValue ( PARSERESPONSE (  __context__ , PRESPONSE, "CHANGE")  ) ; 
                        } 
                    
                    __context__.SourceCodeLine = 826;
                    if ( Functions.TestForTrue  ( ( Functions.Find( "illumination.sources.laser.power" , PRESPONSE ))  ) ) 
                        { 
                        __context__.SourceCodeLine = 826;
                        AOLASERPOWER  .Value = (ushort) ( Functions.Atoi( PARSERESPONSE( __context__ , PRESPONSE , "CHANGE" ) ) ) ; 
                        } 
                    
                    __context__.SourceCodeLine = 827;
                    if ( Functions.TestForTrue  ( ( Functions.Find( "illumination.sources.led.power" , PRESPONSE ))  ) ) 
                        { 
                        __context__.SourceCodeLine = 827;
                        AOLEDPOWER  .Value = (ushort) ( Functions.Atoi( PARSERESPONSE( __context__ , PRESPONSE , "CHANGE" ) ) ) ; 
                        } 
                    
                    __context__.SourceCodeLine = 828;
                    if ( Functions.TestForTrue  ( ( Functions.Find( "environment.temperature.outlet.observableoutput" , PRESPONSE ))  ) ) 
                        { 
                        __context__.SourceCodeLine = 828;
                        SOOUTLETTEMP  .UpdateValue ( Functions.Left ( PARSERESPONSE (  __context__ , PRESPONSE, "CHANGE") ,  (int) ( 4 ) )  ) ; 
                        } 
                    
                    __context__.SourceCodeLine = 829;
                    if ( Functions.TestForTrue  ( ( Functions.Find( "environment.temperature.inlet.observableoutput" , PRESPONSE ))  ) ) 
                        { 
                        __context__.SourceCodeLine = 829;
                        SOINLETTEMP  .UpdateValue ( Functions.Left ( PARSERESPONSE (  __context__ , PRESPONSE, "CHANGE") ,  (int) ( 4 ) )  ) ; 
                        } 
                    
                    __context__.SourceCodeLine = 830;
                    if ( Functions.TestForTrue  ( ( Functions.Find( "environment.humidity.observableoutput" , PRESPONSE ))  ) ) 
                        { 
                        __context__.SourceCodeLine = 830;
                        SOHUMIDITY  .UpdateValue ( PARSERESPONSE (  __context__ , PRESPONSE, "CHANGE")  ) ; 
                        } 
                    
                    __context__.SourceCodeLine = 831;
                    if ( Functions.TestForTrue  ( ( Functions.Find( "environment.power.ac.voltage.observableoutput" , PRESPONSE ))  ) ) 
                        { 
                        __context__.SourceCodeLine = 831;
                        SOPOWERACVOLTAGE  .UpdateValue ( PARSERESPONSE (  __context__ , PRESPONSE, "CHANGE")  ) ; 
                        } 
                    
                    __context__.SourceCodeLine = 832;
                    if ( Functions.TestForTrue  ( ( Functions.Find( "statistics.operating.laseron.value" , PRESPONSE ))  ) ) 
                        { 
                        __context__.SourceCodeLine = 834;
                        AOLASERONHOURS  .Value = (ushort) ( CALCULATEHOURS( __context__ , (ushort)( Functions.Atoi( PARSERESPONSE( __context__ , PRESPONSE , "CHANGE" ) ) ) ) ) ; 
                        } 
                    
                    __context__.SourceCodeLine = 836;
                    if ( Functions.TestForTrue  ( ( Functions.Find( "image.processing.warp.enable" , PRESPONSE ))  ) ) 
                        { 
                        __context__.SourceCodeLine = 836;
                        SOWARPSTATUS  .UpdateValue ( PARSERESPONSE (  __context__ , PRESPONSE, "CHANGE")  ) ; 
                        } 
                    
                    __context__.SourceCodeLine = 837;
                    if ( Functions.TestForTrue  ( ( Functions.Find( "image.processing.warp.fourcorners.enable" , PRESPONSE ))  ) ) 
                        { 
                        __context__.SourceCodeLine = 837;
                        SO4CORNERSSTATUS  .UpdateValue ( PARSERESPONSE (  __context__ , PRESPONSE, "CHANGE")  ) ; 
                        } 
                    
                    __context__.SourceCodeLine = 838;
                    if ( Functions.TestForTrue  ( ( Functions.Find( "image.processing.warp.bow.enable" , PRESPONSE ))  ) ) 
                        { 
                        __context__.SourceCodeLine = 838;
                        SOBOWSTATUS  .UpdateValue ( PARSERESPONSE (  __context__ , PRESPONSE, "CHANGE")  ) ; 
                        } 
                    
                    __context__.SourceCodeLine = 839;
                    if ( Functions.TestForTrue  ( ( Functions.Find( "image.processing.warp.bow.symmetric" , PRESPONSE ))  ) ) 
                        { 
                        __context__.SourceCodeLine = 839;
                        SOSYMMETRICSTATUS  .UpdateValue ( PARSERESPONSE (  __context__ , PRESPONSE, "CHANGE")  ) ; 
                        } 
                    
                    } 
                
                else 
                    {
                    __context__.SourceCodeLine = 841;
                    if ( Functions.TestForTrue  ( ( Functions.Find( "\u0022result\u0022:" , PRESPONSE ))  ) ) 
                        { 
                        __context__.SourceCodeLine = 843;
                        if ( Functions.TestForTrue  ( ( Functions.BoolToInt (GITRACELOGSENABLED == 1))  ) ) 
                            {
                            __context__.SourceCodeLine = 843;
                            Trace( "RESULT RESONSE : {0}\r\n", PRESPONSE ) ; 
                            }
                        
                        __context__.SourceCodeLine = 844;
                        if ( Functions.TestForTrue  ( ( Functions.BoolToInt (Functions.Find( "\u0022id\u0022:3," , PRESPONSE , 1 ) != 0))  ) ) 
                            { 
                            __context__.SourceCodeLine = 844;
                            SOSYSTEMSTATE  .UpdateValue ( PARSERESPONSE (  __context__ , PRESPONSE, "RESULT")  ) ; 
                            } 
                        
                        else 
                            {
                            __context__.SourceCodeLine = 845;
                            if ( Functions.TestForTrue  ( ( Functions.BoolToInt (Functions.Find( "\u0022id\u0022:110," , PRESPONSE , 1 ) != 0))  ) ) 
                                { 
                                __context__.SourceCodeLine = 847;
                                GSCURRENTSOURCE  .UpdateValue ( REMOVEQUOTES (  __context__ , PARSERESPONSE( __context__ , PRESPONSE , "RESULT" ))  ) ; 
                                __context__.SourceCodeLine = 848;
                                SOINPUTSOURCE  .UpdateValue ( GSCURRENTSOURCE  ) ; 
                                __context__.SourceCodeLine = 849;
                                if ( Functions.TestForTrue  ( ( Functions.BoolToInt ( (Functions.TestForTrue ( Functions.BoolToInt (GSCURRENTSOURCE == "Dual DVI sequential") ) || Functions.TestForTrue ( Functions.BoolToInt (GSCURRENTSOURCE == "Dual DVI columns") )) ))  ) ) 
                                    { 
                                    __context__.SourceCodeLine = 851;
                                    READONEPROPERTY (  __context__ , (ushort)( 111 ), "image.connector.dvi1.detectedsignal") ; 
                                    } 
                                
                                else 
                                    {
                                    __context__.SourceCodeLine = 853;
                                    if ( Functions.TestForTrue  ( ( Functions.BoolToInt ( (Functions.TestForTrue ( Functions.BoolToInt (GSCURRENTSOURCE == "Dual DisplayPort sequential") ) || Functions.TestForTrue ( Functions.BoolToInt (GSCURRENTSOURCE == "Dual DisplayPort columns") )) ))  ) ) 
                                        { 
                                        __context__.SourceCodeLine = 855;
                                        READONEPROPERTY (  __context__ , (ushort)( 111 ), "image.connector.displayport1.detectedsignal") ; 
                                        } 
                                    
                                    else 
                                        { 
                                        __context__.SourceCodeLine = 859;
                                        MakeString ( SPROPERTY , "image.connector.{0}.detectedsignal", Functions.Lower ( REMOVESPACES (  __context__ , GSCURRENTSOURCE) ) ) ; 
                                        __context__.SourceCodeLine = 860;
                                        READONEPROPERTY (  __context__ , (ushort)( 111 ), SPROPERTY) ; 
                                        } 
                                    
                                    }
                                
                                } 
                            
                            else 
                                {
                                __context__.SourceCodeLine = 863;
                                if ( Functions.TestForTrue  ( ( Functions.BoolToInt (Functions.Find( "\u0022id\u0022:111," , PRESPONSE , 1 ) != 0))  ) ) 
                                    { 
                                    __context__.SourceCodeLine = 865;
                                    EXTRACTSIGNALINFO (  __context__ , PRESPONSE, "RESULT") ; 
                                    } 
                                
                                else 
                                    {
                                    __context__.SourceCodeLine = 867;
                                    if ( Functions.TestForTrue  ( ( Functions.BoolToInt (Functions.Find( "\u0022id\u0022:114," , PRESPONSE , 1 ) != 0))  ) ) 
                                        { 
                                        __context__.SourceCodeLine = 867;
                                        SOCENTER16BY9STATUS  .UpdateValue ( PARSERESPONSE (  __context__ , PRESPONSE, "RESULT")  ) ; 
                                        } 
                                    
                                    else 
                                        {
                                        __context__.SourceCodeLine = 868;
                                        if ( Functions.TestForTrue  ( ( Functions.BoolToInt (Functions.Find( "\u0022id\u0022:116," , PRESPONSE , 1 ) != 0))  ) ) 
                                            { 
                                            __context__.SourceCodeLine = 868;
                                            SOCENTER16BY9FACTOR  .UpdateValue ( PARSERESPONSE (  __context__ , PRESPONSE, "RESULT")  ) ; 
                                            } 
                                        
                                        else 
                                            {
                                            __context__.SourceCodeLine = 869;
                                            if ( Functions.TestForTrue  ( ( Functions.BoolToInt (Functions.Find( "\u0022id\u0022:126," , PRESPONSE , 1 ) != 0))  ) ) 
                                                { 
                                                __context__.SourceCodeLine = 869;
                                                SOCROPPINGMODE  .UpdateValue ( PARSERESPONSE (  __context__ , PRESPONSE, "RESULT")  ) ; 
                                                } 
                                            
                                            else 
                                                {
                                                __context__.SourceCodeLine = 870;
                                                if ( Functions.TestForTrue  ( ( Functions.BoolToInt (Functions.Find( "\u0022id\u0022:127," , PRESPONSE , 1 ) != 0))  ) ) 
                                                    { 
                                                    __context__.SourceCodeLine = 870;
                                                    SOCROPPINGASPECTRATIO  .UpdateValue ( PARSERESPONSE (  __context__ , PRESPONSE, "RESULT")  ) ; 
                                                    } 
                                                
                                                else 
                                                    {
                                                    __context__.SourceCodeLine = 871;
                                                    if ( Functions.TestForTrue  ( ( Functions.BoolToInt (Functions.Find( "\u0022id\u0022:128," , PRESPONSE , 1 ) != 0))  ) ) 
                                                        { 
                                                        __context__.SourceCodeLine = 873;
                                                        AOCROPPINGMANUALLEFT  .Value = (ushort) ( Functions.Atoi( EXTRACTCROPPINGVALUE( __context__ , PRESPONSE , "LEFT" ) ) ) ; 
                                                        __context__.SourceCodeLine = 874;
                                                        AOCROPPINGMANUALRIGHT  .Value = (ushort) ( Functions.Atoi( EXTRACTCROPPINGVALUE( __context__ , PRESPONSE , "RIGHT" ) ) ) ; 
                                                        __context__.SourceCodeLine = 875;
                                                        AOCROPPINGMANUALTOP  .Value = (ushort) ( Functions.Atoi( EXTRACTCROPPINGVALUE( __context__ , PRESPONSE , "TOP" ) ) ) ; 
                                                        __context__.SourceCodeLine = 876;
                                                        AOCROPPINGMANUALBOTTOM  .Value = (ushort) ( Functions.Atoi( EXTRACTCROPPINGVALUE( __context__ , PRESPONSE , "BOTTOM" ) ) ) ; 
                                                        } 
                                                    
                                                    else 
                                                        {
                                                        __context__.SourceCodeLine = 878;
                                                        if ( Functions.TestForTrue  ( ( Functions.BoolToInt (Functions.Find( "\u0022id\u0022:130," , PRESPONSE , 1 ) != 0))  ) ) 
                                                            { 
                                                            __context__.SourceCodeLine = 878;
                                                            SOP7MODE  .UpdateValue ( REMOVEQUOTES (  __context__ , PARSERESPONSE( __context__ , PRESPONSE , "RESULT" ))  ) ; 
                                                            } 
                                                        
                                                        else 
                                                            {
                                                            __context__.SourceCodeLine = 879;
                                                            if ( Functions.TestForTrue  ( ( Functions.BoolToInt (Functions.Find( "\u0022id\u0022:136," , PRESPONSE , 1 ) != 0))  ) ) 
                                                                { 
                                                                __context__.SourceCodeLine = 879;
                                                                SOHDRBOOSTVALUEFB  .UpdateValue ( PARSERESPONSE (  __context__ , PRESPONSE, "RESULT")  ) ; 
                                                                } 
                                                            
                                                            else 
                                                                {
                                                                __context__.SourceCodeLine = 880;
                                                                if ( Functions.TestForTrue  ( ( Functions.BoolToInt (Functions.Find( "\u0022id\u0022:139," , PRESPONSE , 1 ) != 0))  ) ) 
                                                                    { 
                                                                    __context__.SourceCodeLine = 880;
                                                                    SOCONTENTBASEDAUTOCROPPINGSTATUS  .UpdateValue ( PARSERESPONSE (  __context__ , PRESPONSE, "RESULT")  ) ; 
                                                                    } 
                                                                
                                                                else 
                                                                    {
                                                                    __context__.SourceCodeLine = 881;
                                                                    if ( Functions.TestForTrue  ( ( Functions.BoolToInt (Functions.Find( "\u0022id\u0022:203," , PRESPONSE , 1 ) != 0))  ) ) 
                                                                        { 
                                                                        __context__.SourceCodeLine = 881;
                                                                        SOSHUTTERPOSITION  .UpdateValue ( PARSERESPONSE (  __context__ , PRESPONSE, "RESULT")  ) ; 
                                                                        } 
                                                                    
                                                                    else 
                                                                        {
                                                                        __context__.SourceCodeLine = 882;
                                                                        if ( Functions.TestForTrue  ( ( Functions.BoolToInt (Functions.Find( "\u0022id\u0022:302," , PRESPONSE , 1 ) != 0))  ) ) 
                                                                            { 
                                                                            __context__.SourceCodeLine = 882;
                                                                            AOLASERPOWER  .Value = (ushort) ( Functions.Atoi( PARSERESPONSE( __context__ , PRESPONSE , "RESULT" ) ) ) ; 
                                                                            } 
                                                                        
                                                                        else 
                                                                            {
                                                                            __context__.SourceCodeLine = 883;
                                                                            if ( Functions.TestForTrue  ( ( Functions.BoolToInt (Functions.Find( "\u0022id\u0022:304," , PRESPONSE , 1 ) != 0))  ) ) 
                                                                                { 
                                                                                __context__.SourceCodeLine = 883;
                                                                                AOLEDPOWER  .Value = (ushort) ( Functions.Atoi( PARSERESPONSE( __context__ , PRESPONSE , "RESULT" ) ) ) ; 
                                                                                } 
                                                                            
                                                                            else 
                                                                                {
                                                                                __context__.SourceCodeLine = 884;
                                                                                if ( Functions.TestForTrue  ( ( Functions.BoolToInt (Functions.Find( "\u0022id\u0022:406," , PRESPONSE , 1 ) != 0))  ) ) 
                                                                                    { 
                                                                                    __context__.SourceCodeLine = 884;
                                                                                    SOOUTLETTEMP  .UpdateValue ( Functions.Left ( PARSERESPONSE (  __context__ , PRESPONSE, "RESULT") ,  (int) ( 4 ) )  ) ; 
                                                                                    } 
                                                                                
                                                                                else 
                                                                                    {
                                                                                    __context__.SourceCodeLine = 885;
                                                                                    if ( Functions.TestForTrue  ( ( Functions.BoolToInt (Functions.Find( "\u0022id\u0022:407," , PRESPONSE , 1 ) != 0))  ) ) 
                                                                                        { 
                                                                                        __context__.SourceCodeLine = 885;
                                                                                        SOINLETTEMP  .UpdateValue ( Functions.Left ( PARSERESPONSE (  __context__ , PRESPONSE, "RESULT") ,  (int) ( 4 ) )  ) ; 
                                                                                        } 
                                                                                    
                                                                                    else 
                                                                                        {
                                                                                        __context__.SourceCodeLine = 886;
                                                                                        if ( Functions.TestForTrue  ( ( Functions.BoolToInt (Functions.Find( "\u0022id\u0022:408," , PRESPONSE , 1 ) != 0))  ) ) 
                                                                                            { 
                                                                                            __context__.SourceCodeLine = 886;
                                                                                            SOHUMIDITY  .UpdateValue ( PARSERESPONSE (  __context__ , PRESPONSE, "RESULT")  ) ; 
                                                                                            } 
                                                                                        
                                                                                        else 
                                                                                            {
                                                                                            __context__.SourceCodeLine = 887;
                                                                                            if ( Functions.TestForTrue  ( ( Functions.BoolToInt (Functions.Find( "\u0022id\u0022:409," , PRESPONSE , 1 ) != 0))  ) ) 
                                                                                                { 
                                                                                                __context__.SourceCodeLine = 887;
                                                                                                SOPOWERACVOLTAGE  .UpdateValue ( PARSERESPONSE (  __context__ , PRESPONSE, "RESULT")  ) ; 
                                                                                                } 
                                                                                            
                                                                                            else 
                                                                                                {
                                                                                                __context__.SourceCodeLine = 888;
                                                                                                if ( Functions.TestForTrue  ( ( Functions.BoolToInt (Functions.Find( "\u0022id\u0022:501," , PRESPONSE , 1 ) != 0))  ) ) 
                                                                                                    { 
                                                                                                    __context__.SourceCodeLine = 888;
                                                                                                    AOLASERONHOURS  .Value = (ushort) ( CALCULATEHOURS( __context__ , (ushort)( Functions.Atoi( PARSERESPONSE( __context__ , PRESPONSE , "RESULT" ) ) ) ) ) ; 
                                                                                                    } 
                                                                                                
                                                                                                else 
                                                                                                    {
                                                                                                    __context__.SourceCodeLine = 889;
                                                                                                    if ( Functions.TestForTrue  ( ( Functions.BoolToInt (Functions.Find( "\u0022id\u0022:575," , PRESPONSE , 1 ) != 0))  ) ) 
                                                                                                        { 
                                                                                                        __context__.SourceCodeLine = 889;
                                                                                                        SOWARPSTATUS  .UpdateValue ( PARSERESPONSE (  __context__ , PRESPONSE, "RESULT")  ) ; 
                                                                                                        } 
                                                                                                    
                                                                                                    else 
                                                                                                        {
                                                                                                        __context__.SourceCodeLine = 890;
                                                                                                        if ( Functions.TestForTrue  ( ( Functions.BoolToInt (Functions.Find( "\u0022id\u0022:576," , PRESPONSE , 1 ) != 0))  ) ) 
                                                                                                            { 
                                                                                                            __context__.SourceCodeLine = 890;
                                                                                                            SO4CORNERSSTATUS  .UpdateValue ( PARSERESPONSE (  __context__ , PRESPONSE, "RESULT")  ) ; 
                                                                                                            } 
                                                                                                        
                                                                                                        else 
                                                                                                            {
                                                                                                            __context__.SourceCodeLine = 891;
                                                                                                            if ( Functions.TestForTrue  ( ( Functions.BoolToInt (Functions.Find( "\u0022id\u0022:577," , PRESPONSE , 1 ) != 0))  ) ) 
                                                                                                                { 
                                                                                                                __context__.SourceCodeLine = 891;
                                                                                                                SOBOWSTATUS  .UpdateValue ( PARSERESPONSE (  __context__ , PRESPONSE, "RESULT")  ) ; 
                                                                                                                } 
                                                                                                            
                                                                                                            else 
                                                                                                                {
                                                                                                                __context__.SourceCodeLine = 892;
                                                                                                                if ( Functions.TestForTrue  ( ( Functions.BoolToInt (Functions.Find( "\u0022id\u0022:578," , PRESPONSE , 1 ) != 0))  ) ) 
                                                                                                                    { 
                                                                                                                    __context__.SourceCodeLine = 892;
                                                                                                                    SOSYMMETRICSTATUS  .UpdateValue ( PARSERESPONSE (  __context__ , PRESPONSE, "RESULT")  ) ; 
                                                                                                                    } 
                                                                                                                
                                                                                                                else 
                                                                                                                    { 
                                                                                                                    __context__.SourceCodeLine = 893;
                                                                                                                    SOFROMDEVICE  .UpdateValue ( PRESPONSE  ) ; 
                                                                                                                    } 
                                                                                                                
                                                                                                                }
                                                                                                            
                                                                                                            }
                                                                                                        
                                                                                                        }
                                                                                                    
                                                                                                    }
                                                                                                
                                                                                                }
                                                                                            
                                                                                            }
                                                                                        
                                                                                        }
                                                                                    
                                                                                    }
                                                                                
                                                                                }
                                                                            
                                                                            }
                                                                        
                                                                        }
                                                                    
                                                                    }
                                                                
                                                                }
                                                            
                                                            }
                                                        
                                                        }
                                                    
                                                    }
                                                
                                                }
                                            
                                            }
                                        
                                        }
                                    
                                    }
                                
                                }
                            
                            }
                        
                        } 
                    
                    }
                
                }
            
            
            }
            
        object DISTARTCLIENT_OnPush_0 ( Object __EventInfo__ )
        
            { 
            Crestron.Logos.SplusObjects.SignalEventArgs __SignalEventArg__ = (Crestron.Logos.SplusObjects.SignalEventArgs)__EventInfo__;
            try
            {
                SplusExecutionContext __context__ = SplusThreadStartCode(__SignalEventArg__);
                short SICONNECTSTATUS = 0;
                
                
                __context__.SourceCodeLine = 903;
                SICONNECTSTATUS = (short) ( Functions.SocketConnectClient( TCPSOCKET , SPIPADDRESS  , (ushort)( IPPORT  .Value ) , (ushort)( 1 ) ) ) ; 
                __context__.SourceCodeLine = 904;
                if ( Functions.TestForTrue  ( ( Functions.BoolToInt (GITRACELOGSENABLED == 1))  ) ) 
                    {
                    __context__.SourceCodeLine = 904;
                    Trace( "SOCKETCONNECTCLIENT STATUS: {0:d}\r\n", (short)SICONNECTSTATUS) ; 
                    }
                
                __context__.SourceCodeLine = 905;
                if ( Functions.TestForTrue  ( ( Functions.BoolToInt ( (Functions.TestForTrue ( Functions.BoolToInt ( SICONNECTSTATUS < 0 ) ) && Functions.TestForTrue ( Functions.BoolToInt (GITRACELOGSENABLED == 1) )) ))  ) ) 
                    {
                    __context__.SourceCodeLine = 906;
                    Trace( "Error connecting socket [{0:d}] to address {1} on port {2:d}\r\n", (short)SICONNECTSTATUS, SPIPADDRESS , (ushort)IPPORT  .Value) ; 
                    }
                
                
                
            }
            catch(Exception e) { ObjectCatchHandler(e); }
            finally { ObjectFinallyHandler( __SignalEventArg__ ); }
            return this;
            
        }
        
    object DISTARTCLIENT_OnRelease_1 ( Object __EventInfo__ )
    
        { 
        Crestron.Logos.SplusObjects.SignalEventArgs __SignalEventArg__ = (Crestron.Logos.SplusObjects.SignalEventArgs)__EventInfo__;
        try
        {
            SplusExecutionContext __context__ = SplusThreadStartCode(__SignalEventArg__);
            short SIDISCONNECTSTATUS = 0;
            
            
            __context__.SourceCodeLine = 913;
            SIDISCONNECTSTATUS = (short) ( Functions.SocketDisconnectClient( TCPSOCKET ) ) ; 
            __context__.SourceCodeLine = 914;
            if ( Functions.TestForTrue  ( ( Functions.BoolToInt (GITRACELOGSENABLED == 1))  ) ) 
                {
                __context__.SourceCodeLine = 914;
                Trace( "SOCKETDISCONNECTCLIENT STATUS: {0:d}\r\n", (short)SIDISCONNECTSTATUS) ; 
                }
            
            __context__.SourceCodeLine = 915;
            if ( Functions.TestForTrue  ( ( Functions.BoolToInt ( (Functions.TestForTrue ( Functions.BoolToInt ( SIDISCONNECTSTATUS < 0 ) ) && Functions.TestForTrue ( Functions.BoolToInt (GITRACELOGSENABLED == 1) )) ))  ) ) 
                {
                __context__.SourceCodeLine = 916;
                Trace( "Error connecting socket [{0:d}] to address {1} on port {2:d}\r\n", (short)SIDISCONNECTSTATUS, SPIPADDRESS , (ushort)IPPORT  .Value) ; 
                }
            
            
            
        }
        catch(Exception e) { ObjectCatchHandler(e); }
        finally { ObjectFinallyHandler( __SignalEventArg__ ); }
        return this;
        
    }
    
object DIENABLETRACELOGS_OnPush_2 ( Object __EventInfo__ )

    { 
    Crestron.Logos.SplusObjects.SignalEventArgs __SignalEventArg__ = (Crestron.Logos.SplusObjects.SignalEventArgs)__EventInfo__;
    try
    {
        SplusExecutionContext __context__ = SplusThreadStartCode(__SignalEventArg__);
        
        __context__.SourceCodeLine = 921;
        GITRACELOGSENABLED = (ushort) ( 1 ) ; 
        
        
    }
    catch(Exception e) { ObjectCatchHandler(e); }
    finally { ObjectFinallyHandler( __SignalEventArg__ ); }
    return this;
    
}

object DIENABLETRACELOGS_OnRelease_3 ( Object __EventInfo__ )

    { 
    Crestron.Logos.SplusObjects.SignalEventArgs __SignalEventArg__ = (Crestron.Logos.SplusObjects.SignalEventArgs)__EventInfo__;
    try
    {
        SplusExecutionContext __context__ = SplusThreadStartCode(__SignalEventArg__);
        
        __context__.SourceCodeLine = 926;
        GITRACELOGSENABLED = (ushort) ( 0 ) ; 
        
        
    }
    catch(Exception e) { ObjectCatchHandler(e); }
    finally { ObjectFinallyHandler( __SignalEventArg__ ); }
    return this;
    
}

object DIPOWERON_OnPush_4 ( Object __EventInfo__ )

    { 
    Crestron.Logos.SplusObjects.SignalEventArgs __SignalEventArg__ = (Crestron.Logos.SplusObjects.SignalEventArgs)__EventInfo__;
    try
    {
        SplusExecutionContext __context__ = SplusThreadStartCode(__SignalEventArg__);
        
        __context__.SourceCodeLine = 930;
        CALLMETHOD (  __context__ , (ushort)( 1 ), "system.poweron", "", "") ; 
        
        
    }
    catch(Exception e) { ObjectCatchHandler(e); }
    finally { ObjectFinallyHandler( __SignalEventArg__ ); }
    return this;
    
}

object DIPOWEROFF_OnPush_5 ( Object __EventInfo__ )

    { 
    Crestron.Logos.SplusObjects.SignalEventArgs __SignalEventArg__ = (Crestron.Logos.SplusObjects.SignalEventArgs)__EventInfo__;
    try
    {
        SplusExecutionContext __context__ = SplusThreadStartCode(__SignalEventArg__);
        
        __context__.SourceCodeLine = 931;
        CALLMETHOD (  __context__ , (ushort)( 2 ), "system.poweroff", "", "") ; 
        
        
    }
    catch(Exception e) { ObjectCatchHandler(e); }
    finally { ObjectFinallyHandler( __SignalEventArg__ ); }
    return this;
    
}

object DIREBOOTPROJECTORCOMMAND_OnPush_6 ( Object __EventInfo__ )

    { 
    Crestron.Logos.SplusObjects.SignalEventArgs __SignalEventArg__ = (Crestron.Logos.SplusObjects.SignalEventArgs)__EventInfo__;
    try
    {
        SplusExecutionContext __context__ = SplusThreadStartCode(__SignalEventArg__);
        CrestronString SMETHOD;
        CrestronString SPARAMS;
        CrestronString SID;
        CrestronString STX;
        SMETHOD  = new CrestronString( Crestron.Logos.SplusObjects.CrestronStringEncoding.eEncodingASCII, 50, this );
        SPARAMS  = new CrestronString( Crestron.Logos.SplusObjects.CrestronStringEncoding.eEncodingASCII, 50, this );
        SID  = new CrestronString( Crestron.Logos.SplusObjects.CrestronStringEncoding.eEncodingASCII, 20, this );
        STX  = new CrestronString( Crestron.Logos.SplusObjects.CrestronStringEncoding.eEncodingASCII, 200, this );
        
        
        __context__.SourceCodeLine = 937;
        SMETHOD  .UpdateValue ( "\"method\": \"authenticate\", "  ) ; 
        __context__.SourceCodeLine = 938;
        SPARAMS  .UpdateValue ( "\"params\": {\"code\":292920}, "  ) ; 
        __context__.SourceCodeLine = 939;
        SID  .UpdateValue ( "\"id\": " + Functions.ItoA (  (int) ( 550 ) ) + ""  ) ; 
        __context__.SourceCodeLine = 941;
        STX  .UpdateValue ( "{\"jsonrpc\": \"2.0\", " + SMETHOD + SPARAMS + SID + "}"  ) ; 
        __context__.SourceCodeLine = 943;
        SENDMESSAGE (  __context__ , STX) ; 
        __context__.SourceCodeLine = 945;
        Functions.Delay (  (int) ( 100 ) ) ; 
        __context__.SourceCodeLine = 947;
        CALLMETHOD (  __context__ , (ushort)( 6 ), "system.reboot", "", "") ; 
        
        
    }
    catch(Exception e) { ObjectCatchHandler(e); }
    finally { ObjectFinallyHandler( __SignalEventArg__ ); }
    return this;
    
}

object DIGOTOSTANDBYECOMODE_OnPush_7 ( Object __EventInfo__ )

    { 
    Crestron.Logos.SplusObjects.SignalEventArgs __SignalEventArg__ = (Crestron.Logos.SplusObjects.SignalEventArgs)__EventInfo__;
    try
    {
        SplusExecutionContext __context__ = SplusThreadStartCode(__SignalEventArg__);
        
        __context__.SourceCodeLine = 951;
        WRITEONEPROPERTY (  __context__ , (ushort)( 7 ), "system.eco.enable", "true", "BOOLEAN") ; 
        __context__.SourceCodeLine = 952;
        Functions.Delay (  (int) ( 100 ) ) ; 
        __context__.SourceCodeLine = 953;
        CALLMETHOD (  __context__ , (ushort)( 8 ), "system.gotoeco", "", "") ; 
        
        
    }
    catch(Exception e) { ObjectCatchHandler(e); }
    finally { ObjectFinallyHandler( __SignalEventArg__ ); }
    return this;
    
}

object DISOURCEDVI1_OnPush_8 ( Object __EventInfo__ )

    { 
    Crestron.Logos.SplusObjects.SignalEventArgs __SignalEventArg__ = (Crestron.Logos.SplusObjects.SignalEventArgs)__EventInfo__;
    try
    {
        SplusExecutionContext __context__ = SplusThreadStartCode(__SignalEventArg__);
        
        __context__.SourceCodeLine = 959;
        WRITEONEPROPERTY (  __context__ , (ushort)( 101 ), "image.window.main.source", "DVI 1", "STRING") ; 
        
        
    }
    catch(Exception e) { ObjectCatchHandler(e); }
    finally { ObjectFinallyHandler( __SignalEventArg__ ); }
    return this;
    
}

object DISOURCEDVI2_OnPush_9 ( Object __EventInfo__ )

    { 
    Crestron.Logos.SplusObjects.SignalEventArgs __SignalEventArg__ = (Crestron.Logos.SplusObjects.SignalEventArgs)__EventInfo__;
    try
    {
        SplusExecutionContext __context__ = SplusThreadStartCode(__SignalEventArg__);
        
        __context__.SourceCodeLine = 960;
        WRITEONEPROPERTY (  __context__ , (ushort)( 102 ), "image.window.main.source", "DVI 2", "STRING") ; 
        
        
    }
    catch(Exception e) { ObjectCatchHandler(e); }
    finally { ObjectFinallyHandler( __SignalEventArg__ ); }
    return this;
    
}

object DISOURCEDISPLAYPORT1_OnPush_10 ( Object __EventInfo__ )

    { 
    Crestron.Logos.SplusObjects.SignalEventArgs __SignalEventArg__ = (Crestron.Logos.SplusObjects.SignalEventArgs)__EventInfo__;
    try
    {
        SplusExecutionContext __context__ = SplusThreadStartCode(__SignalEventArg__);
        
        __context__.SourceCodeLine = 961;
        WRITEONEPROPERTY (  __context__ , (ushort)( 103 ), "image.window.main.source", "DisplayPort 1", "STRING") ; 
        
        
    }
    catch(Exception e) { ObjectCatchHandler(e); }
    finally { ObjectFinallyHandler( __SignalEventArg__ ); }
    return this;
    
}

object DISOURCEDISPLAYPORT2_OnPush_11 ( Object __EventInfo__ )

    { 
    Crestron.Logos.SplusObjects.SignalEventArgs __SignalEventArg__ = (Crestron.Logos.SplusObjects.SignalEventArgs)__EventInfo__;
    try
    {
        SplusExecutionContext __context__ = SplusThreadStartCode(__SignalEventArg__);
        
        __context__.SourceCodeLine = 962;
        WRITEONEPROPERTY (  __context__ , (ushort)( 104 ), "image.window.main.source", "DisplayPort 2", "STRING") ; 
        
        
    }
    catch(Exception e) { ObjectCatchHandler(e); }
    finally { ObjectFinallyHandler( __SignalEventArg__ ); }
    return this;
    
}

object DISOURCEDUALDVISEQUENTIAL_OnPush_12 ( Object __EventInfo__ )

    { 
    Crestron.Logos.SplusObjects.SignalEventArgs __SignalEventArg__ = (Crestron.Logos.SplusObjects.SignalEventArgs)__EventInfo__;
    try
    {
        SplusExecutionContext __context__ = SplusThreadStartCode(__SignalEventArg__);
        
        __context__.SourceCodeLine = 963;
        WRITEONEPROPERTY (  __context__ , (ushort)( 105 ), "image.window.main.source", "Dual DVI sequential", "STRING") ; 
        
        
    }
    catch(Exception e) { ObjectCatchHandler(e); }
    finally { ObjectFinallyHandler( __SignalEventArg__ ); }
    return this;
    
}

object DISOURCEDUALDISPLAYPORTSEQUENTIAL_OnPush_13 ( Object __EventInfo__ )

    { 
    Crestron.Logos.SplusObjects.SignalEventArgs __SignalEventArg__ = (Crestron.Logos.SplusObjects.SignalEventArgs)__EventInfo__;
    try
    {
        SplusExecutionContext __context__ = SplusThreadStartCode(__SignalEventArg__);
        
        __context__.SourceCodeLine = 964;
        WRITEONEPROPERTY (  __context__ , (ushort)( 106 ), "image.window.main.source", "Dual DisplayPort sequential", "STRING") ; 
        
        
    }
    catch(Exception e) { ObjectCatchHandler(e); }
    finally { ObjectFinallyHandler( __SignalEventArg__ ); }
    return this;
    
}

object DISOURCEDUALDVICOLUMNS_OnPush_14 ( Object __EventInfo__ )

    { 
    Crestron.Logos.SplusObjects.SignalEventArgs __SignalEventArg__ = (Crestron.Logos.SplusObjects.SignalEventArgs)__EventInfo__;
    try
    {
        SplusExecutionContext __context__ = SplusThreadStartCode(__SignalEventArg__);
        
        __context__.SourceCodeLine = 965;
        WRITEONEPROPERTY (  __context__ , (ushort)( 105 ), "image.window.main.source", "Dual DVI columns", "STRING") ; 
        
        
    }
    catch(Exception e) { ObjectCatchHandler(e); }
    finally { ObjectFinallyHandler( __SignalEventArg__ ); }
    return this;
    
}

object DISOURCEDUALDISPLAYPORTCOLUMNS_OnPush_15 ( Object __EventInfo__ )

    { 
    Crestron.Logos.SplusObjects.SignalEventArgs __SignalEventArg__ = (Crestron.Logos.SplusObjects.SignalEventArgs)__EventInfo__;
    try
    {
        SplusExecutionContext __context__ = SplusThreadStartCode(__SignalEventArg__);
        
        __context__.SourceCodeLine = 966;
        WRITEONEPROPERTY (  __context__ , (ushort)( 106 ), "image.window.main.source", "Dual DisplayPort columns", "STRING") ; 
        
        
    }
    catch(Exception e) { ObjectCatchHandler(e); }
    finally { ObjectFinallyHandler( __SignalEventArg__ ); }
    return this;
    
}

object DISOURCEHDMI_OnPush_16 ( Object __EventInfo__ )

    { 
    Crestron.Logos.SplusObjects.SignalEventArgs __SignalEventArg__ = (Crestron.Logos.SplusObjects.SignalEventArgs)__EventInfo__;
    try
    {
        SplusExecutionContext __context__ = SplusThreadStartCode(__SignalEventArg__);
        
        __context__.SourceCodeLine = 967;
        WRITEONEPROPERTY (  __context__ , (ushort)( 107 ), "image.window.main.source", "HDMI", "STRING") ; 
        
        
    }
    catch(Exception e) { ObjectCatchHandler(e); }
    finally { ObjectFinallyHandler( __SignalEventArg__ ); }
    return this;
    
}

object DISOURCEHDBASET_OnPush_17 ( Object __EventInfo__ )

    { 
    Crestron.Logos.SplusObjects.SignalEventArgs __SignalEventArg__ = (Crestron.Logos.SplusObjects.SignalEventArgs)__EventInfo__;
    try
    {
        SplusExecutionContext __context__ = SplusThreadStartCode(__SignalEventArg__);
        
        __context__.SourceCodeLine = 968;
        WRITEONEPROPERTY (  __context__ , (ushort)( 108 ), "image.window.main.source", "HDBaseT", "STRING") ; 
        
        
    }
    catch(Exception e) { ObjectCatchHandler(e); }
    finally { ObjectFinallyHandler( __SignalEventArg__ ); }
    return this;
    
}

object DISOURCESDI_OnPush_18 ( Object __EventInfo__ )

    { 
    Crestron.Logos.SplusObjects.SignalEventArgs __SignalEventArg__ = (Crestron.Logos.SplusObjects.SignalEventArgs)__EventInfo__;
    try
    {
        SplusExecutionContext __context__ = SplusThreadStartCode(__SignalEventArg__);
        
        __context__.SourceCodeLine = 969;
        WRITEONEPROPERTY (  __context__ , (ushort)( 109 ), "image.window.main.source", "SDI", "STRING") ; 
        
        
    }
    catch(Exception e) { ObjectCatchHandler(e); }
    finally { ObjectFinallyHandler( __SignalEventArg__ ); }
    return this;
    
}

object DICENTER_16BY9_ENABLE_OnPush_19 ( Object __EventInfo__ )

    { 
    Crestron.Logos.SplusObjects.SignalEventArgs __SignalEventArg__ = (Crestron.Logos.SplusObjects.SignalEventArgs)__EventInfo__;
    try
    {
        SplusExecutionContext __context__ = SplusThreadStartCode(__SignalEventArg__);
        
        __context__.SourceCodeLine = 970;
        WRITEONEPROPERTY (  __context__ , (ushort)( 112 ), "image.window.main.center16_9.enable", "true", "BOOLEAN") ; 
        
        
    }
    catch(Exception e) { ObjectCatchHandler(e); }
    finally { ObjectFinallyHandler( __SignalEventArg__ ); }
    return this;
    
}

object DICENTER_16BY9_DISABLE_OnPush_20 ( Object __EventInfo__ )

    { 
    Crestron.Logos.SplusObjects.SignalEventArgs __SignalEventArg__ = (Crestron.Logos.SplusObjects.SignalEventArgs)__EventInfo__;
    try
    {
        SplusExecutionContext __context__ = SplusThreadStartCode(__SignalEventArg__);
        
        __context__.SourceCodeLine = 971;
        WRITEONEPROPERTY (  __context__ , (ushort)( 113 ), "image.window.main.center16_9.enable", "false", "BOOLEAN") ; 
        
        
    }
    catch(Exception e) { ObjectCatchHandler(e); }
    finally { ObjectFinallyHandler( __SignalEventArg__ ); }
    return this;
    
}

object DISET_FACTOR_OnPush_21 ( Object __EventInfo__ )

    { 
    Crestron.Logos.SplusObjects.SignalEventArgs __SignalEventArg__ = (Crestron.Logos.SplusObjects.SignalEventArgs)__EventInfo__;
    try
    {
        SplusExecutionContext __context__ = SplusThreadStartCode(__SignalEventArg__);
        
        __context__.SourceCodeLine = 972;
        WRITEONEPROPERTY (  __context__ , (ushort)( 115 ), "image.window.main.center16_9.factor", SIFACTOR, "NUMBER") ; 
        
        
    }
    catch(Exception e) { ObjectCatchHandler(e); }
    finally { ObjectFinallyHandler( __SignalEventArg__ ); }
    return this;
    
}

object DISET_CROPPING_TO_MANUAL_OnPush_22 ( Object __EventInfo__ )

    { 
    Crestron.Logos.SplusObjects.SignalEventArgs __SignalEventArg__ = (Crestron.Logos.SplusObjects.SignalEventArgs)__EventInfo__;
    try
    {
        SplusExecutionContext __context__ = SplusThreadStartCode(__SignalEventArg__);
        
        __context__.SourceCodeLine = 973;
        WRITEONEPROPERTY (  __context__ , (ushort)( 123 ), "image.window.main.cropping.mode", "MANUAL", "STRING") ; 
        
        
    }
    catch(Exception e) { ObjectCatchHandler(e); }
    finally { ObjectFinallyHandler( __SignalEventArg__ ); }
    return this;
    
}

object DISET_CROPPING_TO_ASPECT_OnPush_23 ( Object __EventInfo__ )

    { 
    Crestron.Logos.SplusObjects.SignalEventArgs __SignalEventArg__ = (Crestron.Logos.SplusObjects.SignalEventArgs)__EventInfo__;
    try
    {
        SplusExecutionContext __context__ = SplusThreadStartCode(__SignalEventArg__);
        
        __context__.SourceCodeLine = 974;
        WRITEONEPROPERTY (  __context__ , (ushort)( 124 ), "image.window.main.cropping.mode", "ASPECT", "STRING") ; 
        
        
    }
    catch(Exception e) { ObjectCatchHandler(e); }
    finally { ObjectFinallyHandler( __SignalEventArg__ ); }
    return this;
    
}

object DISET_CROPPING_TO_AUTO_OnPush_24 ( Object __EventInfo__ )

    { 
    Crestron.Logos.SplusObjects.SignalEventArgs __SignalEventArg__ = (Crestron.Logos.SplusObjects.SignalEventArgs)__EventInfo__;
    try
    {
        SplusExecutionContext __context__ = SplusThreadStartCode(__SignalEventArg__);
        
        __context__.SourceCodeLine = 975;
        WRITEONEPROPERTY (  __context__ , (ushort)( 129 ), "image.window.main.cropping.mode", "AUTO", "STRING") ; 
        
        
    }
    catch(Exception e) { ObjectCatchHandler(e); }
    finally { ObjectFinallyHandler( __SignalEventArg__ ); }
    return this;
    
}

object DISET_CROPPING_ASPECT_RATIO_TO_16_9_OnPush_25 ( Object __EventInfo__ )

    { 
    Crestron.Logos.SplusObjects.SignalEventArgs __SignalEventArg__ = (Crestron.Logos.SplusObjects.SignalEventArgs)__EventInfo__;
    try
    {
        SplusExecutionContext __context__ = SplusThreadStartCode(__SignalEventArg__);
        
        __context__.SourceCodeLine = 976;
        WRITEONEPROPERTY (  __context__ , (ushort)( 116 ), "image.window.main.cropping.aspectratio", "16:9", "STRING") ; 
        
        
    }
    catch(Exception e) { ObjectCatchHandler(e); }
    finally { ObjectFinallyHandler( __SignalEventArg__ ); }
    return this;
    
}

object DISET_CROPPING_ASPECT_RATIO_TO_1_85_OnPush_26 ( Object __EventInfo__ )

    { 
    Crestron.Logos.SplusObjects.SignalEventArgs __SignalEventArg__ = (Crestron.Logos.SplusObjects.SignalEventArgs)__EventInfo__;
    try
    {
        SplusExecutionContext __context__ = SplusThreadStartCode(__SignalEventArg__);
        
        __context__.SourceCodeLine = 977;
        WRITEONEPROPERTY (  __context__ , (ushort)( 117 ), "image.window.main.cropping.aspectratio", "1.85:1", "STRING") ; 
        
        
    }
    catch(Exception e) { ObjectCatchHandler(e); }
    finally { ObjectFinallyHandler( __SignalEventArg__ ); }
    return this;
    
}

object DISET_CROPPING_ASPECT_RATIO_TO_2_2_OnPush_27 ( Object __EventInfo__ )

    { 
    Crestron.Logos.SplusObjects.SignalEventArgs __SignalEventArg__ = (Crestron.Logos.SplusObjects.SignalEventArgs)__EventInfo__;
    try
    {
        SplusExecutionContext __context__ = SplusThreadStartCode(__SignalEventArg__);
        
        __context__.SourceCodeLine = 978;
        WRITEONEPROPERTY (  __context__ , (ushort)( 118 ), "image.window.main.cropping.aspectratio", "2.2:1", "STRING") ; 
        
        
    }
    catch(Exception e) { ObjectCatchHandler(e); }
    finally { ObjectFinallyHandler( __SignalEventArg__ ); }
    return this;
    
}

object DISET_CROPPING_ASPECT_RATIO_TO_2_35_OnPush_28 ( Object __EventInfo__ )

    { 
    Crestron.Logos.SplusObjects.SignalEventArgs __SignalEventArg__ = (Crestron.Logos.SplusObjects.SignalEventArgs)__EventInfo__;
    try
    {
        SplusExecutionContext __context__ = SplusThreadStartCode(__SignalEventArg__);
        
        __context__.SourceCodeLine = 979;
        WRITEONEPROPERTY (  __context__ , (ushort)( 119 ), "image.window.main.cropping.aspectratio", "2.35:1", "STRING") ; 
        
        
    }
    catch(Exception e) { ObjectCatchHandler(e); }
    finally { ObjectFinallyHandler( __SignalEventArg__ ); }
    return this;
    
}

object DISET_CROPPING_ASPECT_RATIO_TO_2_37_OnPush_29 ( Object __EventInfo__ )

    { 
    Crestron.Logos.SplusObjects.SignalEventArgs __SignalEventArg__ = (Crestron.Logos.SplusObjects.SignalEventArgs)__EventInfo__;
    try
    {
        SplusExecutionContext __context__ = SplusThreadStartCode(__SignalEventArg__);
        
        __context__.SourceCodeLine = 980;
        WRITEONEPROPERTY (  __context__ , (ushort)( 120 ), "image.window.main.cropping.aspectratio", "2.37:1", "STRING") ; 
        
        
    }
    catch(Exception e) { ObjectCatchHandler(e); }
    finally { ObjectFinallyHandler( __SignalEventArg__ ); }
    return this;
    
}

object DISET_CROPPING_ASPECT_RATIO_TO_2_39_OnPush_30 ( Object __EventInfo__ )

    { 
    Crestron.Logos.SplusObjects.SignalEventArgs __SignalEventArg__ = (Crestron.Logos.SplusObjects.SignalEventArgs)__EventInfo__;
    try
    {
        SplusExecutionContext __context__ = SplusThreadStartCode(__SignalEventArg__);
        
        __context__.SourceCodeLine = 981;
        WRITEONEPROPERTY (  __context__ , (ushort)( 121 ), "image.window.main.cropping.aspectratio", "2.39:1", "STRING") ; 
        
        
    }
    catch(Exception e) { ObjectCatchHandler(e); }
    finally { ObjectFinallyHandler( __SignalEventArg__ ); }
    return this;
    
}

object DISET_MANUAL_CROPPING_ASPECT_RATIO_OnPush_31 ( Object __EventInfo__ )

    { 
    Crestron.Logos.SplusObjects.SignalEventArgs __SignalEventArg__ = (Crestron.Logos.SplusObjects.SignalEventArgs)__EventInfo__;
    try
    {
        SplusExecutionContext __context__ = SplusThreadStartCode(__SignalEventArg__);
        CrestronString SMETHOD;
        CrestronString SPARAMS;
        CrestronString SID;
        CrestronString SPROPERTY;
        CrestronString STX;
        SMETHOD  = new CrestronString( Crestron.Logos.SplusObjects.CrestronStringEncoding.eEncodingASCII, 50, this );
        SPARAMS  = new CrestronString( Crestron.Logos.SplusObjects.CrestronStringEncoding.eEncodingASCII, 200, this );
        SID  = new CrestronString( Crestron.Logos.SplusObjects.CrestronStringEncoding.eEncodingASCII, 20, this );
        SPROPERTY  = new CrestronString( Crestron.Logos.SplusObjects.CrestronStringEncoding.eEncodingASCII, 40, this );
        STX  = new CrestronString( Crestron.Logos.SplusObjects.CrestronStringEncoding.eEncodingASCII, 400, this );
        
        
        __context__.SourceCodeLine = 987;
        SPROPERTY  .UpdateValue ( "image.window.main.cropping.manual"  ) ; 
        __context__.SourceCodeLine = 990;
        SMETHOD  .UpdateValue ( "\"method\": \"property.set\", "  ) ; 
        __context__.SourceCodeLine = 991;
        SPARAMS  .UpdateValue ( "\"params\": {\"property\": \"" + SPROPERTY + "\", \"value\": { \"left\": " + Functions.ItoA (  (int) ( AICROP_VALUE_LEFT  .UshortValue ) ) + ", \"right\": " + Functions.ItoA (  (int) ( AICROP_VALUE_RIGHT  .UshortValue ) ) + ", \"top\": " + Functions.ItoA (  (int) ( AICROP_VALUE_TOP  .UshortValue ) ) + ", \"bottom\": " + Functions.ItoA (  (int) ( AICROP_VALUE_BOTTOM  .UshortValue ) ) + "}}, "  ) ; 
        __context__.SourceCodeLine = 992;
        SID  .UpdateValue ( "\"id\": " + Functions.ItoA (  (int) ( 122 ) ) + ""  ) ; 
        __context__.SourceCodeLine = 994;
        STX  .UpdateValue ( "{\"jsonrpc\": \"2.0\", " + SMETHOD + SPARAMS + SID + "}"  ) ; 
        __context__.SourceCodeLine = 996;
        SENDMESSAGE (  __context__ , STX) ; 
        
        
    }
    catch(Exception e) { ObjectCatchHandler(e); }
    finally { ObjectFinallyHandler( __SignalEventArg__ ); }
    return this;
    
}

object DIENABLEGAMUTREMAPPING_OnPush_32 ( Object __EventInfo__ )

    { 
    Crestron.Logos.SplusObjects.SignalEventArgs __SignalEventArg__ = (Crestron.Logos.SplusObjects.SignalEventArgs)__EventInfo__;
    try
    {
        SplusExecutionContext __context__ = SplusThreadStartCode(__SignalEventArg__);
        
        __context__.SourceCodeLine = 1001;
        GIGAMUTREMAPPINGENABLED = (ushort) ( 1 ) ; 
        __context__.SourceCodeLine = 1002;
        WRITEONEPROPERTY (  __context__ , (ushort)( 131 ), "image.color.p7.custom.mode", "HDR", "STRING") ; 
        
        
    }
    catch(Exception e) { ObjectCatchHandler(e); }
    finally { ObjectFinallyHandler( __SignalEventArg__ ); }
    return this;
    
}

object DIENABLEGAMUTREMAPPING_OnRelease_33 ( Object __EventInfo__ )

    { 
    Crestron.Logos.SplusObjects.SignalEventArgs __SignalEventArg__ = (Crestron.Logos.SplusObjects.SignalEventArgs)__EventInfo__;
    try
    {
        SplusExecutionContext __context__ = SplusThreadStartCode(__SignalEventArg__);
        
        __context__.SourceCodeLine = 1007;
        GIGAMUTREMAPPINGENABLED = (ushort) ( 0 ) ; 
        __context__.SourceCodeLine = 1008;
        READONEPROPERTY (  __context__ , (ushort)( 110 ), "image.window.main.source") ; 
        
        
    }
    catch(Exception e) { ObjectCatchHandler(e); }
    finally { ObjectFinallyHandler( __SignalEventArg__ ); }
    return this;
    
}

object DIENABLE_CONTENT_BASED_AUTO_CROPPING_OnPush_34 ( Object __EventInfo__ )

    { 
    Crestron.Logos.SplusObjects.SignalEventArgs __SignalEventArg__ = (Crestron.Logos.SplusObjects.SignalEventArgs)__EventInfo__;
    try
    {
        SplusExecutionContext __context__ = SplusThreadStartCode(__SignalEventArg__);
        
        __context__.SourceCodeLine = 1011;
        WRITEONEPROPERTY (  __context__ , (ushort)( 137 ), "image.window.main.cropping.contentbasedautocropping", "true", "BOOLEAN") ; 
        
        
    }
    catch(Exception e) { ObjectCatchHandler(e); }
    finally { ObjectFinallyHandler( __SignalEventArg__ ); }
    return this;
    
}

object DIDISABLE_CONTENT_BASED_AUTO_CROPPING_OnPush_35 ( Object __EventInfo__ )

    { 
    Crestron.Logos.SplusObjects.SignalEventArgs __SignalEventArg__ = (Crestron.Logos.SplusObjects.SignalEventArgs)__EventInfo__;
    try
    {
        SplusExecutionContext __context__ = SplusThreadStartCode(__SignalEventArg__);
        
        __context__.SourceCodeLine = 1012;
        WRITEONEPROPERTY (  __context__ , (ushort)( 138 ), "image.window.main.cropping.contentbasedautocropping", "false", "BOOLEAN") ; 
        
        
    }
    catch(Exception e) { ObjectCatchHandler(e); }
    finally { ObjectFinallyHandler( __SignalEventArg__ ); }
    return this;
    
}

object SIHDRBOOSTVALUE_OnChange_36 ( Object __EventInfo__ )

    { 
    Crestron.Logos.SplusObjects.SignalEventArgs __SignalEventArg__ = (Crestron.Logos.SplusObjects.SignalEventArgs)__EventInfo__;
    try
    {
        SplusExecutionContext __context__ = SplusThreadStartCode(__SignalEventArg__);
        
        __context__.SourceCodeLine = 1016;
        WRITEONEPROPERTY (  __context__ , (ushort)( 135 ), "screen.hdrboost", SIHDRBOOSTVALUE, "NUMBER") ; 
        
        
    }
    catch(Exception e) { ObjectCatchHandler(e); }
    finally { ObjectFinallyHandler( __SignalEventArg__ ); }
    return this;
    
}

object DIREFRESHEDID_OnPush_37 ( Object __EventInfo__ )

    { 
    Crestron.Logos.SplusObjects.SignalEventArgs __SignalEventArg__ = (Crestron.Logos.SplusObjects.SignalEventArgs)__EventInfo__;
    try
    {
        SplusExecutionContext __context__ = SplusThreadStartCode(__SignalEventArg__);
        
        __context__.SourceCodeLine = 1021;
        WRITEONEPROPERTY (  __context__ , (ushort)( 108 ), "image.window.main.source", "HDBaseT", "STRING") ; 
        __context__.SourceCodeLine = 1022;
        Functions.Delay (  (int) ( 200 ) ) ; 
        __context__.SourceCodeLine = 1023;
        WRITEONEPROPERTY (  __context__ , (ushort)( 107 ), "image.window.main.source", "HDMI", "STRING") ; 
        
        
    }
    catch(Exception e) { ObjectCatchHandler(e); }
    finally { ObjectFinallyHandler( __SignalEventArg__ ); }
    return this;
    
}

object DIOPENSHUTTER_OnPush_38 ( Object __EventInfo__ )

    { 
    Crestron.Logos.SplusObjects.SignalEventArgs __SignalEventArg__ = (Crestron.Logos.SplusObjects.SignalEventArgs)__EventInfo__;
    try
    {
        SplusExecutionContext __context__ = SplusThreadStartCode(__SignalEventArg__);
        
        __context__.SourceCodeLine = 1029;
        WRITEONEPROPERTY (  __context__ , (ushort)( 201 ), "optics.shutter.target", "Open", "STRING") ; 
        
        
    }
    catch(Exception e) { ObjectCatchHandler(e); }
    finally { ObjectFinallyHandler( __SignalEventArg__ ); }
    return this;
    
}

object DICLOSESHUTTER_OnPush_39 ( Object __EventInfo__ )

    { 
    Crestron.Logos.SplusObjects.SignalEventArgs __SignalEventArg__ = (Crestron.Logos.SplusObjects.SignalEventArgs)__EventInfo__;
    try
    {
        SplusExecutionContext __context__ = SplusThreadStartCode(__SignalEventArg__);
        
        __context__.SourceCodeLine = 1030;
        WRITEONEPROPERTY (  __context__ , (ushort)( 202 ), "optics.shutter.target", "Closed", "STRING") ; 
        
        
    }
    catch(Exception e) { ObjectCatchHandler(e); }
    finally { ObjectFinallyHandler( __SignalEventArg__ ); }
    return this;
    
}

object DIZOOMIN_OnPush_40 ( Object __EventInfo__ )

    { 
    Crestron.Logos.SplusObjects.SignalEventArgs __SignalEventArg__ = (Crestron.Logos.SplusObjects.SignalEventArgs)__EventInfo__;
    try
    {
        SplusExecutionContext __context__ = SplusThreadStartCode(__SignalEventArg__);
        
        __context__.SourceCodeLine = 1031;
        CALLMETHOD (  __context__ , (ushort)( 204 ), "optics.zoom.stepforward", "steps", "1") ; 
        
        
    }
    catch(Exception e) { ObjectCatchHandler(e); }
    finally { ObjectFinallyHandler( __SignalEventArg__ ); }
    return this;
    
}

object DIZOOMOUT_OnPush_41 ( Object __EventInfo__ )

    { 
    Crestron.Logos.SplusObjects.SignalEventArgs __SignalEventArg__ = (Crestron.Logos.SplusObjects.SignalEventArgs)__EventInfo__;
    try
    {
        SplusExecutionContext __context__ = SplusThreadStartCode(__SignalEventArg__);
        
        __context__.SourceCodeLine = 1032;
        CALLMETHOD (  __context__ , (ushort)( 205 ), "optics.zoom.stepreverse", "steps", "1") ; 
        
        
    }
    catch(Exception e) { ObjectCatchHandler(e); }
    finally { ObjectFinallyHandler( __SignalEventArg__ ); }
    return this;
    
}

object DIFOCUSIN_OnPush_42 ( Object __EventInfo__ )

    { 
    Crestron.Logos.SplusObjects.SignalEventArgs __SignalEventArg__ = (Crestron.Logos.SplusObjects.SignalEventArgs)__EventInfo__;
    try
    {
        SplusExecutionContext __context__ = SplusThreadStartCode(__SignalEventArg__);
        
        __context__.SourceCodeLine = 1033;
        CALLMETHOD (  __context__ , (ushort)( 206 ), "optics.focus.stepforward", "steps", "1") ; 
        
        
    }
    catch(Exception e) { ObjectCatchHandler(e); }
    finally { ObjectFinallyHandler( __SignalEventArg__ ); }
    return this;
    
}

object DIFOCUSOUT_OnPush_43 ( Object __EventInfo__ )

    { 
    Crestron.Logos.SplusObjects.SignalEventArgs __SignalEventArg__ = (Crestron.Logos.SplusObjects.SignalEventArgs)__EventInfo__;
    try
    {
        SplusExecutionContext __context__ = SplusThreadStartCode(__SignalEventArg__);
        
        __context__.SourceCodeLine = 1034;
        CALLMETHOD (  __context__ , (ushort)( 207 ), "optics.focus.stepreverse", "steps", "1") ; 
        
        
    }
    catch(Exception e) { ObjectCatchHandler(e); }
    finally { ObjectFinallyHandler( __SignalEventArg__ ); }
    return this;
    
}

object DIIRISIN_OnPush_44 ( Object __EventInfo__ )

    { 
    Crestron.Logos.SplusObjects.SignalEventArgs __SignalEventArg__ = (Crestron.Logos.SplusObjects.SignalEventArgs)__EventInfo__;
    try
    {
        SplusExecutionContext __context__ = SplusThreadStartCode(__SignalEventArg__);
        
        __context__.SourceCodeLine = 1035;
        CALLMETHOD (  __context__ , (ushort)( 208 ), "optics.iris.stepforward", "steps", "1") ; 
        
        
    }
    catch(Exception e) { ObjectCatchHandler(e); }
    finally { ObjectFinallyHandler( __SignalEventArg__ ); }
    return this;
    
}

object DIIRISOUT_OnPush_45 ( Object __EventInfo__ )

    { 
    Crestron.Logos.SplusObjects.SignalEventArgs __SignalEventArg__ = (Crestron.Logos.SplusObjects.SignalEventArgs)__EventInfo__;
    try
    {
        SplusExecutionContext __context__ = SplusThreadStartCode(__SignalEventArg__);
        
        __context__.SourceCodeLine = 1036;
        CALLMETHOD (  __context__ , (ushort)( 209 ), "optics.iris.stepreverse", "steps", "1") ; 
        
        
    }
    catch(Exception e) { ObjectCatchHandler(e); }
    finally { ObjectFinallyHandler( __SignalEventArg__ ); }
    return this;
    
}

object DILENSSHIFTUP_OnPush_46 ( Object __EventInfo__ )

    { 
    Crestron.Logos.SplusObjects.SignalEventArgs __SignalEventArg__ = (Crestron.Logos.SplusObjects.SignalEventArgs)__EventInfo__;
    try
    {
        SplusExecutionContext __context__ = SplusThreadStartCode(__SignalEventArg__);
        
        __context__.SourceCodeLine = 1037;
        CALLMETHOD (  __context__ , (ushort)( 210 ), "optics.lensshift.vertical.stepreverse", "steps", "1") ; 
        
        
    }
    catch(Exception e) { ObjectCatchHandler(e); }
    finally { ObjectFinallyHandler( __SignalEventArg__ ); }
    return this;
    
}

object DILENSSHIFTDOWN_OnPush_47 ( Object __EventInfo__ )

    { 
    Crestron.Logos.SplusObjects.SignalEventArgs __SignalEventArg__ = (Crestron.Logos.SplusObjects.SignalEventArgs)__EventInfo__;
    try
    {
        SplusExecutionContext __context__ = SplusThreadStartCode(__SignalEventArg__);
        
        __context__.SourceCodeLine = 1038;
        CALLMETHOD (  __context__ , (ushort)( 211 ), "optics.lensshift.vertical.stepforward", "steps", "1") ; 
        
        
    }
    catch(Exception e) { ObjectCatchHandler(e); }
    finally { ObjectFinallyHandler( __SignalEventArg__ ); }
    return this;
    
}

object DILENSSHIFTLEFT_OnPush_48 ( Object __EventInfo__ )

    { 
    Crestron.Logos.SplusObjects.SignalEventArgs __SignalEventArg__ = (Crestron.Logos.SplusObjects.SignalEventArgs)__EventInfo__;
    try
    {
        SplusExecutionContext __context__ = SplusThreadStartCode(__SignalEventArg__);
        
        __context__.SourceCodeLine = 1039;
        CALLMETHOD (  __context__ , (ushort)( 212 ), "optics.lensshift.horizontal.stepreverse", "steps", "1") ; 
        
        
    }
    catch(Exception e) { ObjectCatchHandler(e); }
    finally { ObjectFinallyHandler( __SignalEventArg__ ); }
    return this;
    
}

object DILENSSHIFTRIGHT_OnPush_49 ( Object __EventInfo__ )

    { 
    Crestron.Logos.SplusObjects.SignalEventArgs __SignalEventArg__ = (Crestron.Logos.SplusObjects.SignalEventArgs)__EventInfo__;
    try
    {
        SplusExecutionContext __context__ = SplusThreadStartCode(__SignalEventArg__);
        
        __context__.SourceCodeLine = 1040;
        CALLMETHOD (  __context__ , (ushort)( 213 ), "optics.lensshift.horizontal.stepforward", "steps", "1") ; 
        
        
    }
    catch(Exception e) { ObjectCatchHandler(e); }
    finally { ObjectFinallyHandler( __SignalEventArg__ ); }
    return this;
    
}

object DIENABLE_WARP_OnPush_50 ( Object __EventInfo__ )

    { 
    Crestron.Logos.SplusObjects.SignalEventArgs __SignalEventArg__ = (Crestron.Logos.SplusObjects.SignalEventArgs)__EventInfo__;
    try
    {
        SplusExecutionContext __context__ = SplusThreadStartCode(__SignalEventArg__);
        
        __context__.SourceCodeLine = 1044;
        WRITEONEPROPERTY (  __context__ , (ushort)( 586 ), "image.processing.warp.enable", "true", "BOOLEAN") ; 
        
        
    }
    catch(Exception e) { ObjectCatchHandler(e); }
    finally { ObjectFinallyHandler( __SignalEventArg__ ); }
    return this;
    
}

object DIDISABLE_WARP_OnPush_51 ( Object __EventInfo__ )

    { 
    Crestron.Logos.SplusObjects.SignalEventArgs __SignalEventArg__ = (Crestron.Logos.SplusObjects.SignalEventArgs)__EventInfo__;
    try
    {
        SplusExecutionContext __context__ = SplusThreadStartCode(__SignalEventArg__);
        
        __context__.SourceCodeLine = 1045;
        WRITEONEPROPERTY (  __context__ , (ushort)( 587 ), "image.processing.warp.enable", "false", "BOOLEAN") ; 
        
        
    }
    catch(Exception e) { ObjectCatchHandler(e); }
    finally { ObjectFinallyHandler( __SignalEventArg__ ); }
    return this;
    
}

object DIENABLE_4_CORNERS_OnPush_52 ( Object __EventInfo__ )

    { 
    Crestron.Logos.SplusObjects.SignalEventArgs __SignalEventArg__ = (Crestron.Logos.SplusObjects.SignalEventArgs)__EventInfo__;
    try
    {
        SplusExecutionContext __context__ = SplusThreadStartCode(__SignalEventArg__);
        
        __context__.SourceCodeLine = 1046;
        WRITEONEPROPERTY (  __context__ , (ushort)( 588 ), "image.processing.warp.fourcorners.enable", "true", "BOOLEAN") ; 
        
        
    }
    catch(Exception e) { ObjectCatchHandler(e); }
    finally { ObjectFinallyHandler( __SignalEventArg__ ); }
    return this;
    
}

object DIDISABLE_4_CORNERS_OnPush_53 ( Object __EventInfo__ )

    { 
    Crestron.Logos.SplusObjects.SignalEventArgs __SignalEventArg__ = (Crestron.Logos.SplusObjects.SignalEventArgs)__EventInfo__;
    try
    {
        SplusExecutionContext __context__ = SplusThreadStartCode(__SignalEventArg__);
        
        __context__.SourceCodeLine = 1047;
        WRITEONEPROPERTY (  __context__ , (ushort)( 589 ), "image.processing.warp.fourcorners.enable", "false", "BOOLEAN") ; 
        
        
    }
    catch(Exception e) { ObjectCatchHandler(e); }
    finally { ObjectFinallyHandler( __SignalEventArg__ ); }
    return this;
    
}

object DISET_4_CORNERS_TOP_LEFT_OnPush_54 ( Object __EventInfo__ )

    { 
    Crestron.Logos.SplusObjects.SignalEventArgs __SignalEventArg__ = (Crestron.Logos.SplusObjects.SignalEventArgs)__EventInfo__;
    try
    {
        SplusExecutionContext __context__ = SplusThreadStartCode(__SignalEventArg__);
        CrestronString SMETHOD;
        CrestronString SPARAMS;
        CrestronString SID;
        CrestronString SPROPERTY;
        CrestronString STX;
        SMETHOD  = new CrestronString( Crestron.Logos.SplusObjects.CrestronStringEncoding.eEncodingASCII, 50, this );
        SPARAMS  = new CrestronString( Crestron.Logos.SplusObjects.CrestronStringEncoding.eEncodingASCII, 200, this );
        SID  = new CrestronString( Crestron.Logos.SplusObjects.CrestronStringEncoding.eEncodingASCII, 20, this );
        SPROPERTY  = new CrestronString( Crestron.Logos.SplusObjects.CrestronStringEncoding.eEncodingASCII, 50, this );
        STX  = new CrestronString( Crestron.Logos.SplusObjects.CrestronStringEncoding.eEncodingASCII, 400, this );
        
        
        __context__.SourceCodeLine = 1052;
        SPROPERTY  .UpdateValue ( "image.processing.warp.fourcorners.topleft"  ) ; 
        __context__.SourceCodeLine = 1055;
        SMETHOD  .UpdateValue ( "\"method\": \"property.set\", "  ) ; 
        __context__.SourceCodeLine = 1056;
        SPARAMS  .UpdateValue ( "\"params\": {\"property\": \"" + SPROPERTY + "\", \"value\": { \"x\": " + Functions.ItoA (  (int) ( AI4CORNERSX  .UshortValue ) ) + ", \"y\": " + Functions.ItoA (  (int) ( AI4CORNERSY  .UshortValue ) ) + "}}, "  ) ; 
        __context__.SourceCodeLine = 1057;
        SID  .UpdateValue ( "\"id\": " + Functions.ItoA (  (int) ( 590 ) ) + ""  ) ; 
        __context__.SourceCodeLine = 1059;
        STX  .UpdateValue ( "{\"jsonrpc\": \"2.0\", " + SMETHOD + SPARAMS + SID + "}"  ) ; 
        __context__.SourceCodeLine = 1061;
        SENDMESSAGE (  __context__ , STX) ; 
        
        
    }
    catch(Exception e) { ObjectCatchHandler(e); }
    finally { ObjectFinallyHandler( __SignalEventArg__ ); }
    return this;
    
}

object DISET_4_CORNERS_BOTTOM_LEFT_OnPush_55 ( Object __EventInfo__ )

    { 
    Crestron.Logos.SplusObjects.SignalEventArgs __SignalEventArg__ = (Crestron.Logos.SplusObjects.SignalEventArgs)__EventInfo__;
    try
    {
        SplusExecutionContext __context__ = SplusThreadStartCode(__SignalEventArg__);
        CrestronString SMETHOD;
        CrestronString SPARAMS;
        CrestronString SID;
        CrestronString SPROPERTY;
        CrestronString STX;
        SMETHOD  = new CrestronString( Crestron.Logos.SplusObjects.CrestronStringEncoding.eEncodingASCII, 50, this );
        SPARAMS  = new CrestronString( Crestron.Logos.SplusObjects.CrestronStringEncoding.eEncodingASCII, 200, this );
        SID  = new CrestronString( Crestron.Logos.SplusObjects.CrestronStringEncoding.eEncodingASCII, 20, this );
        SPROPERTY  = new CrestronString( Crestron.Logos.SplusObjects.CrestronStringEncoding.eEncodingASCII, 50, this );
        STX  = new CrestronString( Crestron.Logos.SplusObjects.CrestronStringEncoding.eEncodingASCII, 400, this );
        
        
        __context__.SourceCodeLine = 1067;
        SPROPERTY  .UpdateValue ( "image.processing.warp.fourcorners.bottomleft"  ) ; 
        __context__.SourceCodeLine = 1070;
        SMETHOD  .UpdateValue ( "\"method\": \"property.set\", "  ) ; 
        __context__.SourceCodeLine = 1071;
        SPARAMS  .UpdateValue ( "\"params\": {\"property\": \"" + SPROPERTY + "\", \"value\": { \"x\": " + Functions.ItoA (  (int) ( AI4CORNERSX  .UshortValue ) ) + ", \"y\": " + Functions.ItoA (  (int) ( AI4CORNERSY  .UshortValue ) ) + "}}, "  ) ; 
        __context__.SourceCodeLine = 1072;
        SID  .UpdateValue ( "\"id\": " + Functions.ItoA (  (int) ( 590 ) ) + ""  ) ; 
        __context__.SourceCodeLine = 1074;
        STX  .UpdateValue ( "{\"jsonrpc\": \"2.0\", " + SMETHOD + SPARAMS + SID + "}"  ) ; 
        __context__.SourceCodeLine = 1076;
        SENDMESSAGE (  __context__ , STX) ; 
        
        
    }
    catch(Exception e) { ObjectCatchHandler(e); }
    finally { ObjectFinallyHandler( __SignalEventArg__ ); }
    return this;
    
}

object DISET_4_CORNERS_TOP_RIGHT_OnPush_56 ( Object __EventInfo__ )

    { 
    Crestron.Logos.SplusObjects.SignalEventArgs __SignalEventArg__ = (Crestron.Logos.SplusObjects.SignalEventArgs)__EventInfo__;
    try
    {
        SplusExecutionContext __context__ = SplusThreadStartCode(__SignalEventArg__);
        CrestronString SMETHOD;
        CrestronString SPARAMS;
        CrestronString SID;
        CrestronString SPROPERTY;
        CrestronString STX;
        SMETHOD  = new CrestronString( Crestron.Logos.SplusObjects.CrestronStringEncoding.eEncodingASCII, 50, this );
        SPARAMS  = new CrestronString( Crestron.Logos.SplusObjects.CrestronStringEncoding.eEncodingASCII, 200, this );
        SID  = new CrestronString( Crestron.Logos.SplusObjects.CrestronStringEncoding.eEncodingASCII, 20, this );
        SPROPERTY  = new CrestronString( Crestron.Logos.SplusObjects.CrestronStringEncoding.eEncodingASCII, 50, this );
        STX  = new CrestronString( Crestron.Logos.SplusObjects.CrestronStringEncoding.eEncodingASCII, 400, this );
        
        
        __context__.SourceCodeLine = 1082;
        SPROPERTY  .UpdateValue ( "image.processing.warp.fourcorners.topright"  ) ; 
        __context__.SourceCodeLine = 1085;
        SMETHOD  .UpdateValue ( "\"method\": \"property.set\", "  ) ; 
        __context__.SourceCodeLine = 1086;
        SPARAMS  .UpdateValue ( "\"params\": {\"property\": \"" + SPROPERTY + "\", \"value\": { \"x\": " + Functions.ItoA (  (int) ( AI4CORNERSX  .UshortValue ) ) + ", \"y\": " + Functions.ItoA (  (int) ( AI4CORNERSY  .UshortValue ) ) + "}}, "  ) ; 
        __context__.SourceCodeLine = 1087;
        SID  .UpdateValue ( "\"id\": " + Functions.ItoA (  (int) ( 590 ) ) + ""  ) ; 
        __context__.SourceCodeLine = 1089;
        STX  .UpdateValue ( "{\"jsonrpc\": \"2.0\", " + SMETHOD + SPARAMS + SID + "}"  ) ; 
        __context__.SourceCodeLine = 1091;
        SENDMESSAGE (  __context__ , STX) ; 
        
        
    }
    catch(Exception e) { ObjectCatchHandler(e); }
    finally { ObjectFinallyHandler( __SignalEventArg__ ); }
    return this;
    
}

object DISET_4_CORNERS_BOTTOM_RIGHT_OnPush_57 ( Object __EventInfo__ )

    { 
    Crestron.Logos.SplusObjects.SignalEventArgs __SignalEventArg__ = (Crestron.Logos.SplusObjects.SignalEventArgs)__EventInfo__;
    try
    {
        SplusExecutionContext __context__ = SplusThreadStartCode(__SignalEventArg__);
        CrestronString SMETHOD;
        CrestronString SPARAMS;
        CrestronString SID;
        CrestronString SPROPERTY;
        CrestronString STX;
        SMETHOD  = new CrestronString( Crestron.Logos.SplusObjects.CrestronStringEncoding.eEncodingASCII, 50, this );
        SPARAMS  = new CrestronString( Crestron.Logos.SplusObjects.CrestronStringEncoding.eEncodingASCII, 200, this );
        SID  = new CrestronString( Crestron.Logos.SplusObjects.CrestronStringEncoding.eEncodingASCII, 20, this );
        SPROPERTY  = new CrestronString( Crestron.Logos.SplusObjects.CrestronStringEncoding.eEncodingASCII, 50, this );
        STX  = new CrestronString( Crestron.Logos.SplusObjects.CrestronStringEncoding.eEncodingASCII, 400, this );
        
        
        __context__.SourceCodeLine = 1097;
        SPROPERTY  .UpdateValue ( "image.processing.warp.fourcorners.bottomright"  ) ; 
        __context__.SourceCodeLine = 1100;
        SMETHOD  .UpdateValue ( "\"method\": \"property.set\", "  ) ; 
        __context__.SourceCodeLine = 1101;
        SPARAMS  .UpdateValue ( "\"params\": {\"property\": \"" + SPROPERTY + "\", \"value\": { \"x\": " + Functions.ItoA (  (int) ( AI4CORNERSX  .UshortValue ) ) + ", \"y\": " + Functions.ItoA (  (int) ( AI4CORNERSY  .UshortValue ) ) + "}}, "  ) ; 
        __context__.SourceCodeLine = 1102;
        SID  .UpdateValue ( "\"id\": " + Functions.ItoA (  (int) ( 590 ) ) + ""  ) ; 
        __context__.SourceCodeLine = 1104;
        STX  .UpdateValue ( "{\"jsonrpc\": \"2.0\", " + SMETHOD + SPARAMS + SID + "}"  ) ; 
        __context__.SourceCodeLine = 1106;
        SENDMESSAGE (  __context__ , STX) ; 
        
        
    }
    catch(Exception e) { ObjectCatchHandler(e); }
    finally { ObjectFinallyHandler( __SignalEventArg__ ); }
    return this;
    
}

object DIENABLE_BOW_OnPush_58 ( Object __EventInfo__ )

    { 
    Crestron.Logos.SplusObjects.SignalEventArgs __SignalEventArg__ = (Crestron.Logos.SplusObjects.SignalEventArgs)__EventInfo__;
    try
    {
        SplusExecutionContext __context__ = SplusThreadStartCode(__SignalEventArg__);
        
        __context__.SourceCodeLine = 1108;
        WRITEONEPROPERTY (  __context__ , (ushort)( 594 ), "image.processing.warp.bow.enable", "true", "BOOLEAN") ; 
        
        
    }
    catch(Exception e) { ObjectCatchHandler(e); }
    finally { ObjectFinallyHandler( __SignalEventArg__ ); }
    return this;
    
}

object DIDISABLE_BOW_OnPush_59 ( Object __EventInfo__ )

    { 
    Crestron.Logos.SplusObjects.SignalEventArgs __SignalEventArg__ = (Crestron.Logos.SplusObjects.SignalEventArgs)__EventInfo__;
    try
    {
        SplusExecutionContext __context__ = SplusThreadStartCode(__SignalEventArg__);
        
        __context__.SourceCodeLine = 1109;
        WRITEONEPROPERTY (  __context__ , (ushort)( 595 ), "image.processing.warp.bow.enable", "false", "BOOLEAN") ; 
        
        
    }
    catch(Exception e) { ObjectCatchHandler(e); }
    finally { ObjectFinallyHandler( __SignalEventArg__ ); }
    return this;
    
}

object DIENABLE_SYMMETRIC_OnPush_60 ( Object __EventInfo__ )

    { 
    Crestron.Logos.SplusObjects.SignalEventArgs __SignalEventArg__ = (Crestron.Logos.SplusObjects.SignalEventArgs)__EventInfo__;
    try
    {
        SplusExecutionContext __context__ = SplusThreadStartCode(__SignalEventArg__);
        
        __context__.SourceCodeLine = 1110;
        WRITEONEPROPERTY (  __context__ , (ushort)( 596 ), "image.processing.warp.bow.symmetric", "true", "BOOLEAN") ; 
        
        
    }
    catch(Exception e) { ObjectCatchHandler(e); }
    finally { ObjectFinallyHandler( __SignalEventArg__ ); }
    return this;
    
}

object DIDISABLE_SYMMETRIC_OnPush_61 ( Object __EventInfo__ )

    { 
    Crestron.Logos.SplusObjects.SignalEventArgs __SignalEventArg__ = (Crestron.Logos.SplusObjects.SignalEventArgs)__EventInfo__;
    try
    {
        SplusExecutionContext __context__ = SplusThreadStartCode(__SignalEventArg__);
        
        __context__.SourceCodeLine = 1111;
        WRITEONEPROPERTY (  __context__ , (ushort)( 597 ), "image.processing.warp.bow.symmetric", "false", "BOOLEAN") ; 
        
        
    }
    catch(Exception e) { ObjectCatchHandler(e); }
    finally { ObjectFinallyHandler( __SignalEventArg__ ); }
    return this;
    
}

object DISET_BOW_TOP_OnPush_62 ( Object __EventInfo__ )

    { 
    Crestron.Logos.SplusObjects.SignalEventArgs __SignalEventArg__ = (Crestron.Logos.SplusObjects.SignalEventArgs)__EventInfo__;
    try
    {
        SplusExecutionContext __context__ = SplusThreadStartCode(__SignalEventArg__);
        CrestronString SMETHOD;
        CrestronString SPARAMS;
        CrestronString SID;
        CrestronString SPROPERTY;
        CrestronString STX;
        SMETHOD  = new CrestronString( Crestron.Logos.SplusObjects.CrestronStringEncoding.eEncodingASCII, 50, this );
        SPARAMS  = new CrestronString( Crestron.Logos.SplusObjects.CrestronStringEncoding.eEncodingASCII, 200, this );
        SID  = new CrestronString( Crestron.Logos.SplusObjects.CrestronStringEncoding.eEncodingASCII, 20, this );
        SPROPERTY  = new CrestronString( Crestron.Logos.SplusObjects.CrestronStringEncoding.eEncodingASCII, 50, this );
        STX  = new CrestronString( Crestron.Logos.SplusObjects.CrestronStringEncoding.eEncodingASCII, 400, this );
        
        
        __context__.SourceCodeLine = 1116;
        SPROPERTY  .UpdateValue ( "image.processing.warp.bow.topleftu"  ) ; 
        __context__.SourceCodeLine = 1119;
        SMETHOD  .UpdateValue ( "\"method\": \"property.set\", "  ) ; 
        __context__.SourceCodeLine = 1120;
        SPARAMS  .UpdateValue ( "\"params\": {\"property\": \"" + SPROPERTY + "\", \"value\": { \"angle\": " + Functions.ItoA (  (int) ( AIBOWANGLE  .UshortValue ) ) + ", \"length\": " + Functions.ItoA (  (int) ( AIBOWLENGTH  .UshortValue ) ) + "}}, "  ) ; 
        __context__.SourceCodeLine = 1121;
        SID  .UpdateValue ( "\"id\": " + Functions.ItoA (  (int) ( 598 ) ) + ""  ) ; 
        __context__.SourceCodeLine = 1123;
        STX  .UpdateValue ( "{\"jsonrpc\": \"2.0\", " + SMETHOD + SPARAMS + SID + "}"  ) ; 
        __context__.SourceCodeLine = 1125;
        SENDMESSAGE (  __context__ , STX) ; 
        
        
    }
    catch(Exception e) { ObjectCatchHandler(e); }
    finally { ObjectFinallyHandler( __SignalEventArg__ ); }
    return this;
    
}

object DISET_BOW_BOTTOM_OnPush_63 ( Object __EventInfo__ )

    { 
    Crestron.Logos.SplusObjects.SignalEventArgs __SignalEventArg__ = (Crestron.Logos.SplusObjects.SignalEventArgs)__EventInfo__;
    try
    {
        SplusExecutionContext __context__ = SplusThreadStartCode(__SignalEventArg__);
        CrestronString SMETHOD;
        CrestronString SPARAMS;
        CrestronString SID;
        CrestronString SPROPERTY;
        CrestronString STX;
        SMETHOD  = new CrestronString( Crestron.Logos.SplusObjects.CrestronStringEncoding.eEncodingASCII, 50, this );
        SPARAMS  = new CrestronString( Crestron.Logos.SplusObjects.CrestronStringEncoding.eEncodingASCII, 200, this );
        SID  = new CrestronString( Crestron.Logos.SplusObjects.CrestronStringEncoding.eEncodingASCII, 20, this );
        SPROPERTY  = new CrestronString( Crestron.Logos.SplusObjects.CrestronStringEncoding.eEncodingASCII, 50, this );
        STX  = new CrestronString( Crestron.Logos.SplusObjects.CrestronStringEncoding.eEncodingASCII, 400, this );
        
        
        __context__.SourceCodeLine = 1131;
        SPROPERTY  .UpdateValue ( "image.processing.warp.bow.bottomleftu"  ) ; 
        __context__.SourceCodeLine = 1134;
        SMETHOD  .UpdateValue ( "\"method\": \"property.set\", "  ) ; 
        __context__.SourceCodeLine = 1135;
        SPARAMS  .UpdateValue ( "\"params\": {\"property\": \"" + SPROPERTY + "\", \"value\": { \"angle\": " + Functions.ItoA (  (int) ( AIBOWANGLE  .UshortValue ) ) + ", \"length\": " + Functions.ItoA (  (int) ( AIBOWLENGTH  .UshortValue ) ) + "}}, "  ) ; 
        __context__.SourceCodeLine = 1136;
        SID  .UpdateValue ( "\"id\": " + Functions.ItoA (  (int) ( 599 ) ) + ""  ) ; 
        __context__.SourceCodeLine = 1138;
        STX  .UpdateValue ( "{\"jsonrpc\": \"2.0\", " + SMETHOD + SPARAMS + SID + "}"  ) ; 
        __context__.SourceCodeLine = 1140;
        SENDMESSAGE (  __context__ , STX) ; 
        
        
    }
    catch(Exception e) { ObjectCatchHandler(e); }
    finally { ObjectFinallyHandler( __SignalEventArg__ ); }
    return this;
    
}

object AILASERPOWER_OnChange_64 ( Object __EventInfo__ )

    { 
    Crestron.Logos.SplusObjects.SignalEventArgs __SignalEventArg__ = (Crestron.Logos.SplusObjects.SignalEventArgs)__EventInfo__;
    try
    {
        SplusExecutionContext __context__ = SplusThreadStartCode(__SignalEventArg__);
        
        __context__.SourceCodeLine = 1145;
        WRITEONEPROPERTY (  __context__ , (ushort)( 301 ), "illumination.sources.laser.power", Functions.ItoA( (int)( AILASERPOWER  .UshortValue ) ), "NUMBER") ; 
        
        
    }
    catch(Exception e) { ObjectCatchHandler(e); }
    finally { ObjectFinallyHandler( __SignalEventArg__ ); }
    return this;
    
}

object AILEDPOWER_OnChange_65 ( Object __EventInfo__ )

    { 
    Crestron.Logos.SplusObjects.SignalEventArgs __SignalEventArg__ = (Crestron.Logos.SplusObjects.SignalEventArgs)__EventInfo__;
    try
    {
        SplusExecutionContext __context__ = SplusThreadStartCode(__SignalEventArg__);
        
        __context__.SourceCodeLine = 1146;
        WRITEONEPROPERTY (  __context__ , (ushort)( 303 ), "illumination.sources.led.power", Functions.ItoA( (int)( AILEDPOWER  .UshortValue ) ), "NUMBER") ; 
        
        
    }
    catch(Exception e) { ObjectCatchHandler(e); }
    finally { ObjectFinallyHandler( __SignalEventArg__ ); }
    return this;
    
}

object DIRECALLPROFILE_OnPush_66 ( Object __EventInfo__ )

    { 
    Crestron.Logos.SplusObjects.SignalEventArgs __SignalEventArg__ = (Crestron.Logos.SplusObjects.SignalEventArgs)__EventInfo__;
    try
    {
        SplusExecutionContext __context__ = SplusThreadStartCode(__SignalEventArg__);
        CrestronString SMETHOD;
        CrestronString SPARAMS;
        CrestronString SID;
        CrestronString STX;
        SMETHOD  = new CrestronString( Crestron.Logos.SplusObjects.CrestronStringEncoding.eEncodingASCII, 50, this );
        SPARAMS  = new CrestronString( Crestron.Logos.SplusObjects.CrestronStringEncoding.eEncodingASCII, 50, this );
        SID  = new CrestronString( Crestron.Logos.SplusObjects.CrestronStringEncoding.eEncodingASCII, 20, this );
        STX  = new CrestronString( Crestron.Logos.SplusObjects.CrestronStringEncoding.eEncodingASCII, 200, this );
        
        
        __context__.SourceCodeLine = 1154;
        if ( Functions.TestForTrue  ( ( Functions.BoolToInt (SIPROFILENAME != ""))  ) ) 
            { 
            __context__.SourceCodeLine = 1157;
            SMETHOD  .UpdateValue ( "\"method\": \"profile.activateprofile\", "  ) ; 
            __context__.SourceCodeLine = 1158;
            SPARAMS  .UpdateValue ( "\"params\": \"" + SIPROFILENAME + "\", "  ) ; 
            __context__.SourceCodeLine = 1159;
            SID  .UpdateValue ( "\"id\": " + Functions.ItoA (  (int) ( 550 ) ) + ""  ) ; 
            __context__.SourceCodeLine = 1161;
            STX  .UpdateValue ( "{\"jsonrpc\": \"2.0\", " + SMETHOD + SPARAMS + SID + "}"  ) ; 
            __context__.SourceCodeLine = 1163;
            SENDMESSAGE (  __context__ , STX) ; 
            } 
        
        
        
    }
    catch(Exception e) { ObjectCatchHandler(e); }
    finally { ObjectFinallyHandler( __SignalEventArg__ ); }
    return this;
    
}

object DIRECALLPRESET_OnPush_67 ( Object __EventInfo__ )

    { 
    Crestron.Logos.SplusObjects.SignalEventArgs __SignalEventArg__ = (Crestron.Logos.SplusObjects.SignalEventArgs)__EventInfo__;
    try
    {
        SplusExecutionContext __context__ = SplusThreadStartCode(__SignalEventArg__);
        CrestronString SMETHOD;
        CrestronString SPARAMS;
        CrestronString SID;
        CrestronString STX;
        SMETHOD  = new CrestronString( Crestron.Logos.SplusObjects.CrestronStringEncoding.eEncodingASCII, 50, this );
        SPARAMS  = new CrestronString( Crestron.Logos.SplusObjects.CrestronStringEncoding.eEncodingASCII, 50, this );
        SID  = new CrestronString( Crestron.Logos.SplusObjects.CrestronStringEncoding.eEncodingASCII, 20, this );
        STX  = new CrestronString( Crestron.Logos.SplusObjects.CrestronStringEncoding.eEncodingASCII, 200, this );
        
        
        __context__.SourceCodeLine = 1170;
        if ( Functions.TestForTrue  ( ( Functions.BoolToInt ( (Functions.TestForTrue ( Functions.BoolToInt ( AIPRESETNUMBER  .UshortValue >= 0 ) ) && Functions.TestForTrue ( Functions.BoolToInt ( AIPRESETNUMBER  .UshortValue <= 29 ) )) ))  ) ) 
            { 
            __context__.SourceCodeLine = 1173;
            SMETHOD  .UpdateValue ( "\"method\": \"profile.activatepreset\", "  ) ; 
            __context__.SourceCodeLine = 1174;
            SPARAMS  .UpdateValue ( "\"params\": " + Functions.ItoA (  (int) ( AIPRESETNUMBER  .UshortValue ) ) + ", "  ) ; 
            __context__.SourceCodeLine = 1175;
            SID  .UpdateValue ( "\"id\": " + Functions.ItoA (  (int) ( 551 ) ) + ""  ) ; 
            __context__.SourceCodeLine = 1177;
            STX  .UpdateValue ( "{\"jsonrpc\": \"2.0\", " + SMETHOD + SPARAMS + SID + "}"  ) ; 
            __context__.SourceCodeLine = 1179;
            SENDMESSAGE (  __context__ , STX) ; 
            } 
        
        
        
    }
    catch(Exception e) { ObjectCatchHandler(e); }
    finally { ObjectFinallyHandler( __SignalEventArg__ ); }
    return this;
    
}

object DIENABLE_TRIGGER_1_OnPush_68 ( Object __EventInfo__ )

    { 
    Crestron.Logos.SplusObjects.SignalEventArgs __SignalEventArg__ = (Crestron.Logos.SplusObjects.SignalEventArgs)__EventInfo__;
    try
    {
        SplusExecutionContext __context__ = SplusThreadStartCode(__SignalEventArg__);
        
        __context__.SourceCodeLine = 1185;
        WRITEONEPROPERTY (  __context__ , (ushort)( 9 ), "gpio.gpo.trigger1.value", "true", "BOOLEAN") ; 
        
        
    }
    catch(Exception e) { ObjectCatchHandler(e); }
    finally { ObjectFinallyHandler( __SignalEventArg__ ); }
    return this;
    
}

object DIDISABLE_TRIGGER_1_OnPush_69 ( Object __EventInfo__ )

    { 
    Crestron.Logos.SplusObjects.SignalEventArgs __SignalEventArg__ = (Crestron.Logos.SplusObjects.SignalEventArgs)__EventInfo__;
    try
    {
        SplusExecutionContext __context__ = SplusThreadStartCode(__SignalEventArg__);
        
        __context__.SourceCodeLine = 1186;
        WRITEONEPROPERTY (  __context__ , (ushort)( 10 ), "gpio.gpo.trigger1.value", "false", "BOOLEAN") ; 
        
        
    }
    catch(Exception e) { ObjectCatchHandler(e); }
    finally { ObjectFinallyHandler( __SignalEventArg__ ); }
    return this;
    
}

object DIENABLE_TRIGGER_2_OnPush_70 ( Object __EventInfo__ )

    { 
    Crestron.Logos.SplusObjects.SignalEventArgs __SignalEventArg__ = (Crestron.Logos.SplusObjects.SignalEventArgs)__EventInfo__;
    try
    {
        SplusExecutionContext __context__ = SplusThreadStartCode(__SignalEventArg__);
        
        __context__.SourceCodeLine = 1187;
        WRITEONEPROPERTY (  __context__ , (ushort)( 11 ), "gpio.gpo.trigger2.value", "true", "BOOLEAN") ; 
        
        
    }
    catch(Exception e) { ObjectCatchHandler(e); }
    finally { ObjectFinallyHandler( __SignalEventArg__ ); }
    return this;
    
}

object DIDISABLE_TRIGGER_2_OnPush_71 ( Object __EventInfo__ )

    { 
    Crestron.Logos.SplusObjects.SignalEventArgs __SignalEventArg__ = (Crestron.Logos.SplusObjects.SignalEventArgs)__EventInfo__;
    try
    {
        SplusExecutionContext __context__ = SplusThreadStartCode(__SignalEventArg__);
        
        __context__.SourceCodeLine = 1188;
        WRITEONEPROPERTY (  __context__ , (ushort)( 12 ), "gpio.gpo.trigger2.value", "false", "BOOLEAN") ; 
        
        
    }
    catch(Exception e) { ObjectCatchHandler(e); }
    finally { ObjectFinallyHandler( __SignalEventArg__ ); }
    return this;
    
}

object DIENABLE_TRIGGER_3_OnPush_72 ( Object __EventInfo__ )

    { 
    Crestron.Logos.SplusObjects.SignalEventArgs __SignalEventArg__ = (Crestron.Logos.SplusObjects.SignalEventArgs)__EventInfo__;
    try
    {
        SplusExecutionContext __context__ = SplusThreadStartCode(__SignalEventArg__);
        
        __context__.SourceCodeLine = 1189;
        WRITEONEPROPERTY (  __context__ , (ushort)( 13 ), "gpio.gpo.trigger3.value", "true", "BOOLEAN") ; 
        
        
    }
    catch(Exception e) { ObjectCatchHandler(e); }
    finally { ObjectFinallyHandler( __SignalEventArg__ ); }
    return this;
    
}

object DIDISABLE_TRIGGER_3_OnPush_73 ( Object __EventInfo__ )

    { 
    Crestron.Logos.SplusObjects.SignalEventArgs __SignalEventArg__ = (Crestron.Logos.SplusObjects.SignalEventArgs)__EventInfo__;
    try
    {
        SplusExecutionContext __context__ = SplusThreadStartCode(__SignalEventArg__);
        
        __context__.SourceCodeLine = 1190;
        WRITEONEPROPERTY (  __context__ , (ushort)( 14 ), "gpio.gpo.trigger4.value", "false", "BOOLEAN") ; 
        
        
    }
    catch(Exception e) { ObjectCatchHandler(e); }
    finally { ObjectFinallyHandler( __SignalEventArg__ ); }
    return this;
    
}

object TCPSOCKET_OnSocketConnect_74 ( Object __Info__ )

    { 
    SocketEventInfo __SocketInfo__ = (SocketEventInfo)__Info__;
    try
    {
        SplusExecutionContext __context__ = SplusThreadStartCode(__SocketInfo__);
        int PORTNUMBER = 0;
        
        short LOCALSTATUS = 0;
        
        CrestronString REMOTEIPADDRESS;
        REMOTEIPADDRESS  = new CrestronString( Crestron.Logos.SplusObjects.CrestronStringEncoding.eEncodingASCII, 20, this );
        
        CrestronString REQUESTEDADDRESS;
        REQUESTEDADDRESS  = new CrestronString( Crestron.Logos.SplusObjects.CrestronStringEncoding.eEncodingASCII, 256, this );
        
        
        __context__.SourceCodeLine = 1200;
        DOCLIENTCONNECTED  .Value = (ushort) ( 1 ) ; 
        __context__.SourceCodeLine = 1202;
        LOCALSTATUS = (short) ( Functions.SocketGetAddressAsRequested( TCPSOCKET , ref REQUESTEDADDRESS ) ) ; 
        __context__.SourceCodeLine = 1203;
        if ( Functions.TestForTrue  ( ( Functions.BoolToInt ( (Functions.TestForTrue ( Functions.BoolToInt ( LOCALSTATUS < 0 ) ) && Functions.TestForTrue ( Functions.BoolToInt (GITRACELOGSENABLED == 1) )) ))  ) ) 
            {
            __context__.SourceCodeLine = 1203;
            Trace( "Error getting remote ip address. {0:d}\r\n", (short)LOCALSTATUS) ; 
            }
        
        __context__.SourceCodeLine = 1204;
        if ( Functions.TestForTrue  ( ( Functions.BoolToInt (GITRACELOGSENABLED == 1))  ) ) 
            {
            __context__.SourceCodeLine = 1204;
            Trace( "OnConnect: Connect call to {0} successful\r\n", REQUESTEDADDRESS ) ; 
            }
        
        __context__.SourceCodeLine = 1206;
        PORTNUMBER = (int) ( Functions.SocketGetPortNumber( TCPSOCKET ) ) ; 
        __context__.SourceCodeLine = 1207;
        if ( Functions.TestForTrue  ( ( Functions.BoolToInt ( (Functions.TestForTrue ( Functions.BoolToInt ( PORTNUMBER < 0 ) ) && Functions.TestForTrue ( Functions.BoolToInt (GITRACELOGSENABLED == 1) )) ))  ) ) 
            {
            __context__.SourceCodeLine = 1207;
            Trace( "Error getting client port number. {0:d}\r\n", (int)PORTNUMBER) ; 
            }
        
        __context__.SourceCodeLine = 1209;
        LOCALSTATUS = (short) ( Functions.SocketGetRemoteIPAddress( TCPSOCKET , ref REMOTEIPADDRESS ) ) ; 
        __context__.SourceCodeLine = 1210;
        if ( Functions.TestForTrue  ( ( Functions.BoolToInt ( (Functions.TestForTrue ( Functions.BoolToInt ( LOCALSTATUS < 0 ) ) && Functions.TestForTrue ( Functions.BoolToInt (GITRACELOGSENABLED == 1) )) ))  ) ) 
            {
            __context__.SourceCodeLine = 1210;
            Trace( "Error getting remote ip address. {0:d}\r\n", (short)LOCALSTATUS) ; 
            }
        
        __context__.SourceCodeLine = 1211;
        if ( Functions.TestForTrue  ( ( Functions.BoolToInt (GITRACELOGSENABLED == 1))  ) ) 
            {
            __context__.SourceCodeLine = 1211;
            Trace( "OnConnect: Connected to port {0:d} on address {1}\r\n", (int)PORTNUMBER, REMOTEIPADDRESS ) ; 
            }
        
        __context__.SourceCodeLine = 1213;
        Functions.Delay (  (int) ( 100 ) ) ; 
        __context__.SourceCodeLine = 1216;
        SUBSCRIBETOPROPERTY (  __context__ , (ushort)( 600 ), "system.state") ; 
        __context__.SourceCodeLine = 1217;
        SUBSCRIBETOPROPERTY (  __context__ , (ushort)( 601 ), "image.window.main.source") ; 
        __context__.SourceCodeLine = 1218;
        SUBSCRIBETOPROPERTY (  __context__ , (ushort)( 602 ), "image.window.main.center16_9.enable") ; 
        __context__.SourceCodeLine = 1219;
        SUBSCRIBETOPROPERTY (  __context__ , (ushort)( 603 ), "image.window.main.center16_9.factor") ; 
        __context__.SourceCodeLine = 1220;
        SUBSCRIBETOPROPERTY (  __context__ , (ushort)( 604 ), "optics.shutter.position") ; 
        __context__.SourceCodeLine = 1221;
        SUBSCRIBETOPROPERTY (  __context__ , (ushort)( 605 ), "illumination.sources.laser.power") ; 
        __context__.SourceCodeLine = 1222;
        SUBSCRIBETOPROPERTY (  __context__ , (ushort)( 606 ), "illumination.sources.led.power") ; 
        __context__.SourceCodeLine = 1223;
        SUBSCRIBETOPROPERTY (  __context__ , (ushort)( 611 ), "environment.temperature.outlet.observableoutput") ; 
        __context__.SourceCodeLine = 1224;
        SUBSCRIBETOPROPERTY (  __context__ , (ushort)( 613 ), "statistics.operating.laseron.value") ; 
        __context__.SourceCodeLine = 1225;
        SUBSCRIBETOPROPERTY (  __context__ , (ushort)( 614 ), "image.connector.dvi1.detectedsignal") ; 
        __context__.SourceCodeLine = 1226;
        SUBSCRIBETOPROPERTY (  __context__ , (ushort)( 615 ), "image.connector.dvi2.detectedsignal") ; 
        __context__.SourceCodeLine = 1227;
        SUBSCRIBETOPROPERTY (  __context__ , (ushort)( 616 ), "image.connector.displayport1.detectedsignal") ; 
        __context__.SourceCodeLine = 1228;
        SUBSCRIBETOPROPERTY (  __context__ , (ushort)( 617 ), "image.connector.displayport2.detectedsignal") ; 
        __context__.SourceCodeLine = 1229;
        SUBSCRIBETOPROPERTY (  __context__ , (ushort)( 618 ), "image.connector.hdmi.detectedsignal") ; 
        __context__.SourceCodeLine = 1230;
        SUBSCRIBETOPROPERTY (  __context__ , (ushort)( 619 ), "image.connector.hdbaset.detectedsignal") ; 
        __context__.SourceCodeLine = 1231;
        SUBSCRIBETOPROPERTY (  __context__ , (ushort)( 620 ), "image.connector.sdi.detectedsignal") ; 
        __context__.SourceCodeLine = 1232;
        SUBSCRIBETOPROPERTY (  __context__ , (ushort)( 621 ), "image.window.main.cropping.mode") ; 
        __context__.SourceCodeLine = 1233;
        SUBSCRIBETOPROPERTY (  __context__ , (ushort)( 622 ), "image.window.main.cropping.aspectratio") ; 
        __context__.SourceCodeLine = 1234;
        SUBSCRIBETOPROPERTY (  __context__ , (ushort)( 623 ), "image.window.main.cropping.manual") ; 
        __context__.SourceCodeLine = 1235;
        SUBSCRIBETOPROPERTY (  __context__ , (ushort)( 624 ), "image.color.p7.custom.mode") ; 
        __context__.SourceCodeLine = 1236;
        SUBSCRIBETOPROPERTY (  __context__ , (ushort)( 625 ), "screen.hdrboost") ; 
        __context__.SourceCodeLine = 1237;
        SUBSCRIBETOPROPERTY (  __context__ , (ushort)( 626 ), "environment.temperature.inlet.observableoutput") ; 
        __context__.SourceCodeLine = 1238;
        SUBSCRIBETOPROPERTY (  __context__ , (ushort)( 627 ), "environment.humidity.observableoutput") ; 
        __context__.SourceCodeLine = 1239;
        SUBSCRIBETOPROPERTY (  __context__ , (ushort)( 628 ), "environment.power.ac.voltage.observableoutput") ; 
        __context__.SourceCodeLine = 1240;
        SUBSCRIBETOPROPERTY (  __context__ , (ushort)( 629 ), "image.window.main.cropping.contentbasedautocropping") ; 
        __context__.SourceCodeLine = 1241;
        SUBSCRIBETOPROPERTY (  __context__ , (ushort)( 630 ), "image.processing.warp.enable") ; 
        __context__.SourceCodeLine = 1242;
        SUBSCRIBETOPROPERTY (  __context__ , (ushort)( 631 ), "image.processing.warp.fourcorners.enable") ; 
        __context__.SourceCodeLine = 1243;
        SUBSCRIBETOPROPERTY (  __context__ , (ushort)( 632 ), "image.processing.warp.bow.enable") ; 
        __context__.SourceCodeLine = 1244;
        SUBSCRIBETOPROPERTY (  __context__ , (ushort)( 633 ), "image.processing.warp.bow.symmetric") ; 
        __context__.SourceCodeLine = 1246;
        READONEPROPERTY (  __context__ , (ushort)( 3 ), "system.state") ; 
        __context__.SourceCodeLine = 1247;
        READONEPROPERTY (  __context__ , (ushort)( 110 ), "image.window.main.source") ; 
        __context__.SourceCodeLine = 1248;
        READONEPROPERTY (  __context__ , (ushort)( 114 ), "image.window.main.center16_9.enable") ; 
        __context__.SourceCodeLine = 1249;
        READONEPROPERTY (  __context__ , (ushort)( 116 ), "image.window.main.center16_9.factor") ; 
        __context__.SourceCodeLine = 1250;
        READONEPROPERTY (  __context__ , (ushort)( 126 ), "image.window.main.cropping.mode") ; 
        __context__.SourceCodeLine = 1251;
        READONEPROPERTY (  __context__ , (ushort)( 127 ), "image.window.main.cropping.aspectratio") ; 
        __context__.SourceCodeLine = 1252;
        READONEPROPERTY (  __context__ , (ushort)( 128 ), "image.window.main.cropping.manual") ; 
        __context__.SourceCodeLine = 1253;
        READONEPROPERTY (  __context__ , (ushort)( 130 ), "image.color.p7.custom.mode") ; 
        __context__.SourceCodeLine = 1254;
        READONEPROPERTY (  __context__ , (ushort)( 136 ), "screen.hdrboost") ; 
        __context__.SourceCodeLine = 1255;
        READONEPROPERTY (  __context__ , (ushort)( 139 ), "image.window.main.cropping.contentbasedautocropping") ; 
        __context__.SourceCodeLine = 1256;
        READONEPROPERTY (  __context__ , (ushort)( 203 ), "optics.shutter.position") ; 
        __context__.SourceCodeLine = 1257;
        READONEPROPERTY (  __context__ , (ushort)( 302 ), "illumination.sources.laser.power") ; 
        __context__.SourceCodeLine = 1258;
        READONEPROPERTY (  __context__ , (ushort)( 304 ), "illumination.sources.led.power") ; 
        __context__.SourceCodeLine = 1259;
        READONEPROPERTY (  __context__ , (ushort)( 406 ), "environment.temperature.outlet.observableoutput") ; 
        __context__.SourceCodeLine = 1260;
        READONEPROPERTY (  __context__ , (ushort)( 407 ), "environment.temperature.inlet.observableoutput") ; 
        __context__.SourceCodeLine = 1261;
        READONEPROPERTY (  __context__ , (ushort)( 408 ), "environment.humidity.observableoutput") ; 
        __context__.SourceCodeLine = 1262;
        READONEPROPERTY (  __context__ , (ushort)( 409 ), "environment.power.ac.voltage.observableoutput") ; 
        __context__.SourceCodeLine = 1263;
        READONEPROPERTY (  __context__ , (ushort)( 501 ), "statistics.operating.laseron.value") ; 
        __context__.SourceCodeLine = 1264;
        READONEPROPERTY (  __context__ , (ushort)( 575 ), "image.processing.warp.enable") ; 
        __context__.SourceCodeLine = 1265;
        READONEPROPERTY (  __context__ , (ushort)( 576 ), "image.processing.warp.fourcorners.enable") ; 
        __context__.SourceCodeLine = 1266;
        READONEPROPERTY (  __context__ , (ushort)( 577 ), "image.processing.warp.bow.enable") ; 
        __context__.SourceCodeLine = 1267;
        READONEPROPERTY (  __context__ , (ushort)( 578 ), "image.processing.warp.bow.symmetric") ; 
        
        
    }
    catch(Exception e) { ObjectCatchHandler(e); }
    finally { ObjectFinallyHandler( __SocketInfo__ ); }
    return this;
    
}

object TCPSOCKET_OnSocketDisconnect_75 ( Object __Info__ )

    { 
    SocketEventInfo __SocketInfo__ = (SocketEventInfo)__Info__;
    try
    {
        SplusExecutionContext __context__ = SplusThreadStartCode(__SocketInfo__);
        
        __context__.SourceCodeLine = 1273;
        DOCLIENTCONNECTED  .Value = (ushort) ( 0 ) ; 
        __context__.SourceCodeLine = 1275;
        if ( Functions.TestForTrue  ( ( DISTARTCLIENT  .Value)  ) ) 
            { 
            __context__.SourceCodeLine = 1277;
            if ( Functions.TestForTrue  ( ( Functions.BoolToInt (GITRACELOGSENABLED == 1))  ) ) 
                {
                __context__.SourceCodeLine = 1277;
                Trace( "Socket disconnected remotely") ; 
                }
            
            } 
        
        else 
            { 
            __context__.SourceCodeLine = 1281;
            if ( Functions.TestForTrue  ( ( Functions.BoolToInt (GITRACELOGSENABLED == 1))  ) ) 
                {
                __context__.SourceCodeLine = 1281;
                Trace( "Local disconnect complete.") ; 
                }
            
            } 
        
        
        
    }
    catch(Exception e) { ObjectCatchHandler(e); }
    finally { ObjectFinallyHandler( __SocketInfo__ ); }
    return this;
    
}

object TCPSOCKET_OnSocketStatus_76 ( Object __Info__ )

    { 
    SocketEventInfo __SocketInfo__ = (SocketEventInfo)__Info__;
    try
    {
        SplusExecutionContext __context__ = SplusThreadStartCode(__SocketInfo__);
        short SISTATUS = 0;
        
        
        __context__.SourceCodeLine = 1289;
        SISTATUS = (short) ( __SocketInfo__.SocketStatus ) ; 
        __context__.SourceCodeLine = 1291;
        AOCONNECTIONSTATUS  .Value = (ushort) ( SISTATUS ) ; 
        __context__.SourceCodeLine = 1293;
        if ( Functions.TestForTrue  ( ( Functions.BoolToInt (GITRACELOGSENABLED == 1))  ) ) 
            {
            __context__.SourceCodeLine = 1293;
            Trace( "The SocketGetStatus returns:       {0:d}\r\n", (short)SISTATUS) ; 
            }
        
        __context__.SourceCodeLine = 1294;
        if ( Functions.TestForTrue  ( ( Functions.BoolToInt (GITRACELOGSENABLED == 1))  ) ) 
            {
            __context__.SourceCodeLine = 1294;
            Trace( "The tcpSocket.SocketStatus returns: {0:d}\r\n", (short)TCPSOCKET.SocketStatus) ; 
            }
        
        
        
    }
    catch(Exception e) { ObjectCatchHandler(e); }
    finally { ObjectFinallyHandler( __SocketInfo__ ); }
    return this;
    
}

object TCPSOCKET_OnSocketReceive_77 ( Object __Info__ )

    { 
    SocketEventInfo __SocketInfo__ = (SocketEventInfo)__Info__;
    try
    {
        SplusExecutionContext __context__ = SplusThreadStartCode(__SocketInfo__);
        CrestronString SBUFFER;
        CrestronString SFULLRESPONSE;
        CrestronString SDELIMITER;
        SBUFFER  = new CrestronString( Crestron.Logos.SplusObjects.CrestronStringEncoding.eEncodingASCII, 35000, this );
        SFULLRESPONSE  = new CrestronString( Crestron.Logos.SplusObjects.CrestronStringEncoding.eEncodingASCII, 35000, this );
        SDELIMITER  = new CrestronString( Crestron.Logos.SplusObjects.CrestronStringEncoding.eEncodingASCII, 5, this );
        
        ushort I = 0;
        ushort IRESPONSELENGTH = 0;
        
        
        __context__.SourceCodeLine = 1302;
        if ( Functions.TestForTrue  ( ( Functions.BoolToInt (SEMAPHORE == 0))  ) ) 
            { 
            __context__.SourceCodeLine = 1304;
            SEMAPHORE = (ushort) ( 1 ) ; 
            __context__.SourceCodeLine = 1305;
            SDELIMITER  .UpdateValue ( "}"  ) ; 
            __context__.SourceCodeLine = 1307;
            while ( Functions.TestForTrue  ( ( 1)  ) ) 
                { 
                __context__.SourceCodeLine = 1310;
                SBUFFER  .UpdateValue ( SBUFFER + Functions.Gather ( SDELIMITER , TCPSOCKET .  SocketRxBuf )  ) ; 
                __context__.SourceCodeLine = 1311;
                I = (ushort) ( BRACECOUNT( __context__ , SBUFFER ) ) ; 
                __context__.SourceCodeLine = 1312;
                if ( Functions.TestForTrue  ( ( Functions.BoolToInt (I == 0))  ) ) 
                    { 
                    __context__.SourceCodeLine = 1314;
                    SFULLRESPONSE  .UpdateValue ( SBUFFER  ) ; 
                    __context__.SourceCodeLine = 1315;
                    SBUFFER  .UpdateValue ( ""  ) ; 
                    __context__.SourceCodeLine = 1316;
                    PROCESSRESPONSE (  __context__ , SFULLRESPONSE) ; 
                    __context__.SourceCodeLine = 1317;
                    IRESPONSELENGTH = (ushort) ( Functions.Length( SFULLRESPONSE ) ) ; 
                    } 
                
                __context__.SourceCodeLine = 1307;
                } 
            
            __context__.SourceCodeLine = 1320;
            SEMAPHORE = (ushort) ( 0 ) ; 
            } 
        
        
        
    }
    catch(Exception e) { ObjectCatchHandler(e); }
    finally { ObjectFinallyHandler( __SocketInfo__ ); }
    return this;
    
}

public override object FunctionMain (  object __obj__ ) 
    { 
    try
    {
        SplusExecutionContext __context__ = SplusFunctionMainStartCode();
        
        __context__.SourceCodeLine = 1327;
        SEMAPHORE = (ushort) ( 0 ) ; 
        __context__.SourceCodeLine = 1328;
        GSCURRENTSOURCE  .UpdateValue ( ""  ) ; 
        __context__.SourceCodeLine = 1329;
        WaitForInitializationComplete ( ) ; 
        
        
    }
    catch(Exception e) { ObjectCatchHandler(e); }
    finally { ObjectFinallyHandler(); }
    return __obj__;
    }
    

public override void LogosSplusInitialize()
{
    _SplusNVRAM = new SplusNVRAM( this );
    GSCURRENTSOURCE  = new CrestronString( Crestron.Logos.SplusObjects.CrestronStringEncoding.eEncodingASCII, 30, this );
    TCPSOCKET  = new SplusTcpClient ( 35000, this );
    
    DISTARTCLIENT = new Crestron.Logos.SplusObjects.DigitalInput( DISTARTCLIENT__DigitalInput__, this );
    m_DigitalInputList.Add( DISTARTCLIENT__DigitalInput__, DISTARTCLIENT );
    
    DIENABLETRACELOGS = new Crestron.Logos.SplusObjects.DigitalInput( DIENABLETRACELOGS__DigitalInput__, this );
    m_DigitalInputList.Add( DIENABLETRACELOGS__DigitalInput__, DIENABLETRACELOGS );
    
    DIPOWERON = new Crestron.Logos.SplusObjects.DigitalInput( DIPOWERON__DigitalInput__, this );
    m_DigitalInputList.Add( DIPOWERON__DigitalInput__, DIPOWERON );
    
    DIPOWEROFF = new Crestron.Logos.SplusObjects.DigitalInput( DIPOWEROFF__DigitalInput__, this );
    m_DigitalInputList.Add( DIPOWEROFF__DigitalInput__, DIPOWEROFF );
    
    DIREBOOTPROJECTORCOMMAND = new Crestron.Logos.SplusObjects.DigitalInput( DIREBOOTPROJECTORCOMMAND__DigitalInput__, this );
    m_DigitalInputList.Add( DIREBOOTPROJECTORCOMMAND__DigitalInput__, DIREBOOTPROJECTORCOMMAND );
    
    DIGOTOSTANDBYECOMODE = new Crestron.Logos.SplusObjects.DigitalInput( DIGOTOSTANDBYECOMODE__DigitalInput__, this );
    m_DigitalInputList.Add( DIGOTOSTANDBYECOMODE__DigitalInput__, DIGOTOSTANDBYECOMODE );
    
    DISOURCEDVI1 = new Crestron.Logos.SplusObjects.DigitalInput( DISOURCEDVI1__DigitalInput__, this );
    m_DigitalInputList.Add( DISOURCEDVI1__DigitalInput__, DISOURCEDVI1 );
    
    DISOURCEDVI2 = new Crestron.Logos.SplusObjects.DigitalInput( DISOURCEDVI2__DigitalInput__, this );
    m_DigitalInputList.Add( DISOURCEDVI2__DigitalInput__, DISOURCEDVI2 );
    
    DISOURCEDISPLAYPORT1 = new Crestron.Logos.SplusObjects.DigitalInput( DISOURCEDISPLAYPORT1__DigitalInput__, this );
    m_DigitalInputList.Add( DISOURCEDISPLAYPORT1__DigitalInput__, DISOURCEDISPLAYPORT1 );
    
    DISOURCEDISPLAYPORT2 = new Crestron.Logos.SplusObjects.DigitalInput( DISOURCEDISPLAYPORT2__DigitalInput__, this );
    m_DigitalInputList.Add( DISOURCEDISPLAYPORT2__DigitalInput__, DISOURCEDISPLAYPORT2 );
    
    DISOURCEDUALDVICOLUMNS = new Crestron.Logos.SplusObjects.DigitalInput( DISOURCEDUALDVICOLUMNS__DigitalInput__, this );
    m_DigitalInputList.Add( DISOURCEDUALDVICOLUMNS__DigitalInput__, DISOURCEDUALDVICOLUMNS );
    
    DISOURCEDUALDISPLAYPORTCOLUMNS = new Crestron.Logos.SplusObjects.DigitalInput( DISOURCEDUALDISPLAYPORTCOLUMNS__DigitalInput__, this );
    m_DigitalInputList.Add( DISOURCEDUALDISPLAYPORTCOLUMNS__DigitalInput__, DISOURCEDUALDISPLAYPORTCOLUMNS );
    
    DISOURCEDUALDVISEQUENTIAL = new Crestron.Logos.SplusObjects.DigitalInput( DISOURCEDUALDVISEQUENTIAL__DigitalInput__, this );
    m_DigitalInputList.Add( DISOURCEDUALDVISEQUENTIAL__DigitalInput__, DISOURCEDUALDVISEQUENTIAL );
    
    DISOURCEDUALDISPLAYPORTSEQUENTIAL = new Crestron.Logos.SplusObjects.DigitalInput( DISOURCEDUALDISPLAYPORTSEQUENTIAL__DigitalInput__, this );
    m_DigitalInputList.Add( DISOURCEDUALDISPLAYPORTSEQUENTIAL__DigitalInput__, DISOURCEDUALDISPLAYPORTSEQUENTIAL );
    
    DISOURCEHDMI = new Crestron.Logos.SplusObjects.DigitalInput( DISOURCEHDMI__DigitalInput__, this );
    m_DigitalInputList.Add( DISOURCEHDMI__DigitalInput__, DISOURCEHDMI );
    
    DISOURCEHDBASET = new Crestron.Logos.SplusObjects.DigitalInput( DISOURCEHDBASET__DigitalInput__, this );
    m_DigitalInputList.Add( DISOURCEHDBASET__DigitalInput__, DISOURCEHDBASET );
    
    DISOURCESDI = new Crestron.Logos.SplusObjects.DigitalInput( DISOURCESDI__DigitalInput__, this );
    m_DigitalInputList.Add( DISOURCESDI__DigitalInput__, DISOURCESDI );
    
    DIREFRESHEDID = new Crestron.Logos.SplusObjects.DigitalInput( DIREFRESHEDID__DigitalInput__, this );
    m_DigitalInputList.Add( DIREFRESHEDID__DigitalInput__, DIREFRESHEDID );
    
    DICENTER_16BY9_ENABLE = new Crestron.Logos.SplusObjects.DigitalInput( DICENTER_16BY9_ENABLE__DigitalInput__, this );
    m_DigitalInputList.Add( DICENTER_16BY9_ENABLE__DigitalInput__, DICENTER_16BY9_ENABLE );
    
    DICENTER_16BY9_DISABLE = new Crestron.Logos.SplusObjects.DigitalInput( DICENTER_16BY9_DISABLE__DigitalInput__, this );
    m_DigitalInputList.Add( DICENTER_16BY9_DISABLE__DigitalInput__, DICENTER_16BY9_DISABLE );
    
    DISET_FACTOR = new Crestron.Logos.SplusObjects.DigitalInput( DISET_FACTOR__DigitalInput__, this );
    m_DigitalInputList.Add( DISET_FACTOR__DigitalInput__, DISET_FACTOR );
    
    DISET_CROPPING_TO_MANUAL = new Crestron.Logos.SplusObjects.DigitalInput( DISET_CROPPING_TO_MANUAL__DigitalInput__, this );
    m_DigitalInputList.Add( DISET_CROPPING_TO_MANUAL__DigitalInput__, DISET_CROPPING_TO_MANUAL );
    
    DISET_CROPPING_TO_ASPECT = new Crestron.Logos.SplusObjects.DigitalInput( DISET_CROPPING_TO_ASPECT__DigitalInput__, this );
    m_DigitalInputList.Add( DISET_CROPPING_TO_ASPECT__DigitalInput__, DISET_CROPPING_TO_ASPECT );
    
    DISET_CROPPING_TO_AUTO = new Crestron.Logos.SplusObjects.DigitalInput( DISET_CROPPING_TO_AUTO__DigitalInput__, this );
    m_DigitalInputList.Add( DISET_CROPPING_TO_AUTO__DigitalInput__, DISET_CROPPING_TO_AUTO );
    
    DISET_CROPPING_ASPECT_RATIO_TO_16_9 = new Crestron.Logos.SplusObjects.DigitalInput( DISET_CROPPING_ASPECT_RATIO_TO_16_9__DigitalInput__, this );
    m_DigitalInputList.Add( DISET_CROPPING_ASPECT_RATIO_TO_16_9__DigitalInput__, DISET_CROPPING_ASPECT_RATIO_TO_16_9 );
    
    DISET_CROPPING_ASPECT_RATIO_TO_1_85 = new Crestron.Logos.SplusObjects.DigitalInput( DISET_CROPPING_ASPECT_RATIO_TO_1_85__DigitalInput__, this );
    m_DigitalInputList.Add( DISET_CROPPING_ASPECT_RATIO_TO_1_85__DigitalInput__, DISET_CROPPING_ASPECT_RATIO_TO_1_85 );
    
    DISET_CROPPING_ASPECT_RATIO_TO_2_2 = new Crestron.Logos.SplusObjects.DigitalInput( DISET_CROPPING_ASPECT_RATIO_TO_2_2__DigitalInput__, this );
    m_DigitalInputList.Add( DISET_CROPPING_ASPECT_RATIO_TO_2_2__DigitalInput__, DISET_CROPPING_ASPECT_RATIO_TO_2_2 );
    
    DISET_CROPPING_ASPECT_RATIO_TO_2_35 = new Crestron.Logos.SplusObjects.DigitalInput( DISET_CROPPING_ASPECT_RATIO_TO_2_35__DigitalInput__, this );
    m_DigitalInputList.Add( DISET_CROPPING_ASPECT_RATIO_TO_2_35__DigitalInput__, DISET_CROPPING_ASPECT_RATIO_TO_2_35 );
    
    DISET_CROPPING_ASPECT_RATIO_TO_2_37 = new Crestron.Logos.SplusObjects.DigitalInput( DISET_CROPPING_ASPECT_RATIO_TO_2_37__DigitalInput__, this );
    m_DigitalInputList.Add( DISET_CROPPING_ASPECT_RATIO_TO_2_37__DigitalInput__, DISET_CROPPING_ASPECT_RATIO_TO_2_37 );
    
    DISET_CROPPING_ASPECT_RATIO_TO_2_39 = new Crestron.Logos.SplusObjects.DigitalInput( DISET_CROPPING_ASPECT_RATIO_TO_2_39__DigitalInput__, this );
    m_DigitalInputList.Add( DISET_CROPPING_ASPECT_RATIO_TO_2_39__DigitalInput__, DISET_CROPPING_ASPECT_RATIO_TO_2_39 );
    
    DISET_MANUAL_CROPPING_ASPECT_RATIO = new Crestron.Logos.SplusObjects.DigitalInput( DISET_MANUAL_CROPPING_ASPECT_RATIO__DigitalInput__, this );
    m_DigitalInputList.Add( DISET_MANUAL_CROPPING_ASPECT_RATIO__DigitalInput__, DISET_MANUAL_CROPPING_ASPECT_RATIO );
    
    DIENABLEGAMUTREMAPPING = new Crestron.Logos.SplusObjects.DigitalInput( DIENABLEGAMUTREMAPPING__DigitalInput__, this );
    m_DigitalInputList.Add( DIENABLEGAMUTREMAPPING__DigitalInput__, DIENABLEGAMUTREMAPPING );
    
    DIENABLE_CONTENT_BASED_AUTO_CROPPING = new Crestron.Logos.SplusObjects.DigitalInput( DIENABLE_CONTENT_BASED_AUTO_CROPPING__DigitalInput__, this );
    m_DigitalInputList.Add( DIENABLE_CONTENT_BASED_AUTO_CROPPING__DigitalInput__, DIENABLE_CONTENT_BASED_AUTO_CROPPING );
    
    DIDISABLE_CONTENT_BASED_AUTO_CROPPING = new Crestron.Logos.SplusObjects.DigitalInput( DIDISABLE_CONTENT_BASED_AUTO_CROPPING__DigitalInput__, this );
    m_DigitalInputList.Add( DIDISABLE_CONTENT_BASED_AUTO_CROPPING__DigitalInput__, DIDISABLE_CONTENT_BASED_AUTO_CROPPING );
    
    DIOPENSHUTTER = new Crestron.Logos.SplusObjects.DigitalInput( DIOPENSHUTTER__DigitalInput__, this );
    m_DigitalInputList.Add( DIOPENSHUTTER__DigitalInput__, DIOPENSHUTTER );
    
    DICLOSESHUTTER = new Crestron.Logos.SplusObjects.DigitalInput( DICLOSESHUTTER__DigitalInput__, this );
    m_DigitalInputList.Add( DICLOSESHUTTER__DigitalInput__, DICLOSESHUTTER );
    
    DIZOOMIN = new Crestron.Logos.SplusObjects.DigitalInput( DIZOOMIN__DigitalInput__, this );
    m_DigitalInputList.Add( DIZOOMIN__DigitalInput__, DIZOOMIN );
    
    DIZOOMOUT = new Crestron.Logos.SplusObjects.DigitalInput( DIZOOMOUT__DigitalInput__, this );
    m_DigitalInputList.Add( DIZOOMOUT__DigitalInput__, DIZOOMOUT );
    
    DIFOCUSIN = new Crestron.Logos.SplusObjects.DigitalInput( DIFOCUSIN__DigitalInput__, this );
    m_DigitalInputList.Add( DIFOCUSIN__DigitalInput__, DIFOCUSIN );
    
    DIFOCUSOUT = new Crestron.Logos.SplusObjects.DigitalInput( DIFOCUSOUT__DigitalInput__, this );
    m_DigitalInputList.Add( DIFOCUSOUT__DigitalInput__, DIFOCUSOUT );
    
    DIIRISIN = new Crestron.Logos.SplusObjects.DigitalInput( DIIRISIN__DigitalInput__, this );
    m_DigitalInputList.Add( DIIRISIN__DigitalInput__, DIIRISIN );
    
    DIIRISOUT = new Crestron.Logos.SplusObjects.DigitalInput( DIIRISOUT__DigitalInput__, this );
    m_DigitalInputList.Add( DIIRISOUT__DigitalInput__, DIIRISOUT );
    
    DILENSSHIFTUP = new Crestron.Logos.SplusObjects.DigitalInput( DILENSSHIFTUP__DigitalInput__, this );
    m_DigitalInputList.Add( DILENSSHIFTUP__DigitalInput__, DILENSSHIFTUP );
    
    DILENSSHIFTDOWN = new Crestron.Logos.SplusObjects.DigitalInput( DILENSSHIFTDOWN__DigitalInput__, this );
    m_DigitalInputList.Add( DILENSSHIFTDOWN__DigitalInput__, DILENSSHIFTDOWN );
    
    DILENSSHIFTLEFT = new Crestron.Logos.SplusObjects.DigitalInput( DILENSSHIFTLEFT__DigitalInput__, this );
    m_DigitalInputList.Add( DILENSSHIFTLEFT__DigitalInput__, DILENSSHIFTLEFT );
    
    DILENSSHIFTRIGHT = new Crestron.Logos.SplusObjects.DigitalInput( DILENSSHIFTRIGHT__DigitalInput__, this );
    m_DigitalInputList.Add( DILENSSHIFTRIGHT__DigitalInput__, DILENSSHIFTRIGHT );
    
    DIENABLE_WARP = new Crestron.Logos.SplusObjects.DigitalInput( DIENABLE_WARP__DigitalInput__, this );
    m_DigitalInputList.Add( DIENABLE_WARP__DigitalInput__, DIENABLE_WARP );
    
    DIDISABLE_WARP = new Crestron.Logos.SplusObjects.DigitalInput( DIDISABLE_WARP__DigitalInput__, this );
    m_DigitalInputList.Add( DIDISABLE_WARP__DigitalInput__, DIDISABLE_WARP );
    
    DIENABLE_4_CORNERS = new Crestron.Logos.SplusObjects.DigitalInput( DIENABLE_4_CORNERS__DigitalInput__, this );
    m_DigitalInputList.Add( DIENABLE_4_CORNERS__DigitalInput__, DIENABLE_4_CORNERS );
    
    DIDISABLE_4_CORNERS = new Crestron.Logos.SplusObjects.DigitalInput( DIDISABLE_4_CORNERS__DigitalInput__, this );
    m_DigitalInputList.Add( DIDISABLE_4_CORNERS__DigitalInput__, DIDISABLE_4_CORNERS );
    
    DISET_4_CORNERS_TOP_LEFT = new Crestron.Logos.SplusObjects.DigitalInput( DISET_4_CORNERS_TOP_LEFT__DigitalInput__, this );
    m_DigitalInputList.Add( DISET_4_CORNERS_TOP_LEFT__DigitalInput__, DISET_4_CORNERS_TOP_LEFT );
    
    DISET_4_CORNERS_BOTTOM_LEFT = new Crestron.Logos.SplusObjects.DigitalInput( DISET_4_CORNERS_BOTTOM_LEFT__DigitalInput__, this );
    m_DigitalInputList.Add( DISET_4_CORNERS_BOTTOM_LEFT__DigitalInput__, DISET_4_CORNERS_BOTTOM_LEFT );
    
    DISET_4_CORNERS_TOP_RIGHT = new Crestron.Logos.SplusObjects.DigitalInput( DISET_4_CORNERS_TOP_RIGHT__DigitalInput__, this );
    m_DigitalInputList.Add( DISET_4_CORNERS_TOP_RIGHT__DigitalInput__, DISET_4_CORNERS_TOP_RIGHT );
    
    DISET_4_CORNERS_BOTTOM_RIGHT = new Crestron.Logos.SplusObjects.DigitalInput( DISET_4_CORNERS_BOTTOM_RIGHT__DigitalInput__, this );
    m_DigitalInputList.Add( DISET_4_CORNERS_BOTTOM_RIGHT__DigitalInput__, DISET_4_CORNERS_BOTTOM_RIGHT );
    
    DIENABLE_BOW = new Crestron.Logos.SplusObjects.DigitalInput( DIENABLE_BOW__DigitalInput__, this );
    m_DigitalInputList.Add( DIENABLE_BOW__DigitalInput__, DIENABLE_BOW );
    
    DIDISABLE_BOW = new Crestron.Logos.SplusObjects.DigitalInput( DIDISABLE_BOW__DigitalInput__, this );
    m_DigitalInputList.Add( DIDISABLE_BOW__DigitalInput__, DIDISABLE_BOW );
    
    DIENABLE_SYMMETRIC = new Crestron.Logos.SplusObjects.DigitalInput( DIENABLE_SYMMETRIC__DigitalInput__, this );
    m_DigitalInputList.Add( DIENABLE_SYMMETRIC__DigitalInput__, DIENABLE_SYMMETRIC );
    
    DIDISABLE_SYMMETRIC = new Crestron.Logos.SplusObjects.DigitalInput( DIDISABLE_SYMMETRIC__DigitalInput__, this );
    m_DigitalInputList.Add( DIDISABLE_SYMMETRIC__DigitalInput__, DIDISABLE_SYMMETRIC );
    
    DISET_BOW_TOP = new Crestron.Logos.SplusObjects.DigitalInput( DISET_BOW_TOP__DigitalInput__, this );
    m_DigitalInputList.Add( DISET_BOW_TOP__DigitalInput__, DISET_BOW_TOP );
    
    DISET_BOW_BOTTOM = new Crestron.Logos.SplusObjects.DigitalInput( DISET_BOW_BOTTOM__DigitalInput__, this );
    m_DigitalInputList.Add( DISET_BOW_BOTTOM__DigitalInput__, DISET_BOW_BOTTOM );
    
    DIISCINEMASCOPE = new Crestron.Logos.SplusObjects.DigitalInput( DIISCINEMASCOPE__DigitalInput__, this );
    m_DigitalInputList.Add( DIISCINEMASCOPE__DigitalInput__, DIISCINEMASCOPE );
    
    DIRECALLPROFILE = new Crestron.Logos.SplusObjects.DigitalInput( DIRECALLPROFILE__DigitalInput__, this );
    m_DigitalInputList.Add( DIRECALLPROFILE__DigitalInput__, DIRECALLPROFILE );
    
    DIRECALLPRESET = new Crestron.Logos.SplusObjects.DigitalInput( DIRECALLPRESET__DigitalInput__, this );
    m_DigitalInputList.Add( DIRECALLPRESET__DigitalInput__, DIRECALLPRESET );
    
    DIENABLE_TRIGGER_1 = new Crestron.Logos.SplusObjects.DigitalInput( DIENABLE_TRIGGER_1__DigitalInput__, this );
    m_DigitalInputList.Add( DIENABLE_TRIGGER_1__DigitalInput__, DIENABLE_TRIGGER_1 );
    
    DIDISABLE_TRIGGER_1 = new Crestron.Logos.SplusObjects.DigitalInput( DIDISABLE_TRIGGER_1__DigitalInput__, this );
    m_DigitalInputList.Add( DIDISABLE_TRIGGER_1__DigitalInput__, DIDISABLE_TRIGGER_1 );
    
    DIENABLE_TRIGGER_2 = new Crestron.Logos.SplusObjects.DigitalInput( DIENABLE_TRIGGER_2__DigitalInput__, this );
    m_DigitalInputList.Add( DIENABLE_TRIGGER_2__DigitalInput__, DIENABLE_TRIGGER_2 );
    
    DIDISABLE_TRIGGER_2 = new Crestron.Logos.SplusObjects.DigitalInput( DIDISABLE_TRIGGER_2__DigitalInput__, this );
    m_DigitalInputList.Add( DIDISABLE_TRIGGER_2__DigitalInput__, DIDISABLE_TRIGGER_2 );
    
    DIENABLE_TRIGGER_3 = new Crestron.Logos.SplusObjects.DigitalInput( DIENABLE_TRIGGER_3__DigitalInput__, this );
    m_DigitalInputList.Add( DIENABLE_TRIGGER_3__DigitalInput__, DIENABLE_TRIGGER_3 );
    
    DIDISABLE_TRIGGER_3 = new Crestron.Logos.SplusObjects.DigitalInput( DIDISABLE_TRIGGER_3__DigitalInput__, this );
    m_DigitalInputList.Add( DIDISABLE_TRIGGER_3__DigitalInput__, DIDISABLE_TRIGGER_3 );
    
    DOCLIENTCONNECTED = new Crestron.Logos.SplusObjects.DigitalOutput( DOCLIENTCONNECTED__DigitalOutput__, this );
    m_DigitalOutputList.Add( DOCLIENTCONNECTED__DigitalOutput__, DOCLIENTCONNECTED );
    
    AICROP_VALUE_LEFT = new Crestron.Logos.SplusObjects.AnalogInput( AICROP_VALUE_LEFT__AnalogSerialInput__, this );
    m_AnalogInputList.Add( AICROP_VALUE_LEFT__AnalogSerialInput__, AICROP_VALUE_LEFT );
    
    AICROP_VALUE_RIGHT = new Crestron.Logos.SplusObjects.AnalogInput( AICROP_VALUE_RIGHT__AnalogSerialInput__, this );
    m_AnalogInputList.Add( AICROP_VALUE_RIGHT__AnalogSerialInput__, AICROP_VALUE_RIGHT );
    
    AICROP_VALUE_TOP = new Crestron.Logos.SplusObjects.AnalogInput( AICROP_VALUE_TOP__AnalogSerialInput__, this );
    m_AnalogInputList.Add( AICROP_VALUE_TOP__AnalogSerialInput__, AICROP_VALUE_TOP );
    
    AICROP_VALUE_BOTTOM = new Crestron.Logos.SplusObjects.AnalogInput( AICROP_VALUE_BOTTOM__AnalogSerialInput__, this );
    m_AnalogInputList.Add( AICROP_VALUE_BOTTOM__AnalogSerialInput__, AICROP_VALUE_BOTTOM );
    
    AILASERPOWER = new Crestron.Logos.SplusObjects.AnalogInput( AILASERPOWER__AnalogSerialInput__, this );
    m_AnalogInputList.Add( AILASERPOWER__AnalogSerialInput__, AILASERPOWER );
    
    AILEDPOWER = new Crestron.Logos.SplusObjects.AnalogInput( AILEDPOWER__AnalogSerialInput__, this );
    m_AnalogInputList.Add( AILEDPOWER__AnalogSerialInput__, AILEDPOWER );
    
    AIPRESETNUMBER = new Crestron.Logos.SplusObjects.AnalogInput( AIPRESETNUMBER__AnalogSerialInput__, this );
    m_AnalogInputList.Add( AIPRESETNUMBER__AnalogSerialInput__, AIPRESETNUMBER );
    
    AI4CORNERSX = new Crestron.Logos.SplusObjects.AnalogInput( AI4CORNERSX__AnalogSerialInput__, this );
    m_AnalogInputList.Add( AI4CORNERSX__AnalogSerialInput__, AI4CORNERSX );
    
    AI4CORNERSY = new Crestron.Logos.SplusObjects.AnalogInput( AI4CORNERSY__AnalogSerialInput__, this );
    m_AnalogInputList.Add( AI4CORNERSY__AnalogSerialInput__, AI4CORNERSY );
    
    AIBOWANGLE = new Crestron.Logos.SplusObjects.AnalogInput( AIBOWANGLE__AnalogSerialInput__, this );
    m_AnalogInputList.Add( AIBOWANGLE__AnalogSerialInput__, AIBOWANGLE );
    
    AIBOWLENGTH = new Crestron.Logos.SplusObjects.AnalogInput( AIBOWLENGTH__AnalogSerialInput__, this );
    m_AnalogInputList.Add( AIBOWLENGTH__AnalogSerialInput__, AIBOWLENGTH );
    
    AOCONNECTIONSTATUS = new Crestron.Logos.SplusObjects.AnalogOutput( AOCONNECTIONSTATUS__AnalogSerialOutput__, this );
    m_AnalogOutputList.Add( AOCONNECTIONSTATUS__AnalogSerialOutput__, AOCONNECTIONSTATUS );
    
    AOLASERPOWER = new Crestron.Logos.SplusObjects.AnalogOutput( AOLASERPOWER__AnalogSerialOutput__, this );
    m_AnalogOutputList.Add( AOLASERPOWER__AnalogSerialOutput__, AOLASERPOWER );
    
    AOLEDPOWER = new Crestron.Logos.SplusObjects.AnalogOutput( AOLEDPOWER__AnalogSerialOutput__, this );
    m_AnalogOutputList.Add( AOLEDPOWER__AnalogSerialOutput__, AOLEDPOWER );
    
    AOLASERONHOURS = new Crestron.Logos.SplusObjects.AnalogOutput( AOLASERONHOURS__AnalogSerialOutput__, this );
    m_AnalogOutputList.Add( AOLASERONHOURS__AnalogSerialOutput__, AOLASERONHOURS );
    
    AOCROPPINGMANUALLEFT = new Crestron.Logos.SplusObjects.AnalogOutput( AOCROPPINGMANUALLEFT__AnalogSerialOutput__, this );
    m_AnalogOutputList.Add( AOCROPPINGMANUALLEFT__AnalogSerialOutput__, AOCROPPINGMANUALLEFT );
    
    AOCROPPINGMANUALRIGHT = new Crestron.Logos.SplusObjects.AnalogOutput( AOCROPPINGMANUALRIGHT__AnalogSerialOutput__, this );
    m_AnalogOutputList.Add( AOCROPPINGMANUALRIGHT__AnalogSerialOutput__, AOCROPPINGMANUALRIGHT );
    
    AOCROPPINGMANUALTOP = new Crestron.Logos.SplusObjects.AnalogOutput( AOCROPPINGMANUALTOP__AnalogSerialOutput__, this );
    m_AnalogOutputList.Add( AOCROPPINGMANUALTOP__AnalogSerialOutput__, AOCROPPINGMANUALTOP );
    
    AOCROPPINGMANUALBOTTOM = new Crestron.Logos.SplusObjects.AnalogOutput( AOCROPPINGMANUALBOTTOM__AnalogSerialOutput__, this );
    m_AnalogOutputList.Add( AOCROPPINGMANUALBOTTOM__AnalogSerialOutput__, AOCROPPINGMANUALBOTTOM );
    
    SIFACTOR = new Crestron.Logos.SplusObjects.StringInput( SIFACTOR__AnalogSerialInput__, 5, this );
    m_StringInputList.Add( SIFACTOR__AnalogSerialInput__, SIFACTOR );
    
    SIHDRBOOSTVALUE = new Crestron.Logos.SplusObjects.StringInput( SIHDRBOOSTVALUE__AnalogSerialInput__, 3, this );
    m_StringInputList.Add( SIHDRBOOSTVALUE__AnalogSerialInput__, SIHDRBOOSTVALUE );
    
    SIPROFILENAME = new Crestron.Logos.SplusObjects.StringInput( SIPROFILENAME__AnalogSerialInput__, 30, this );
    m_StringInputList.Add( SIPROFILENAME__AnalogSerialInput__, SIPROFILENAME );
    
    SOFROMDEVICE = new Crestron.Logos.SplusObjects.StringOutput( SOFROMDEVICE__AnalogSerialOutput__, this );
    m_StringOutputList.Add( SOFROMDEVICE__AnalogSerialOutput__, SOFROMDEVICE );
    
    SOSYSTEMSTATE = new Crestron.Logos.SplusObjects.StringOutput( SOSYSTEMSTATE__AnalogSerialOutput__, this );
    m_StringOutputList.Add( SOSYSTEMSTATE__AnalogSerialOutput__, SOSYSTEMSTATE );
    
    SOINPUTSOURCE = new Crestron.Logos.SplusObjects.StringOutput( SOINPUTSOURCE__AnalogSerialOutput__, this );
    m_StringOutputList.Add( SOINPUTSOURCE__AnalogSerialOutput__, SOINPUTSOURCE );
    
    SODETECTEDSIGNAL = new Crestron.Logos.SplusObjects.StringOutput( SODETECTEDSIGNAL__AnalogSerialOutput__, this );
    m_StringOutputList.Add( SODETECTEDSIGNAL__AnalogSerialOutput__, SODETECTEDSIGNAL );
    
    SOCOLORSPACE = new Crestron.Logos.SplusObjects.StringOutput( SOCOLORSPACE__AnalogSerialOutput__, this );
    m_StringOutputList.Add( SOCOLORSPACE__AnalogSerialOutput__, SOCOLORSPACE );
    
    SOMASTERINGLUMINANCE = new Crestron.Logos.SplusObjects.StringOutput( SOMASTERINGLUMINANCE__AnalogSerialOutput__, this );
    m_StringOutputList.Add( SOMASTERINGLUMINANCE__AnalogSerialOutput__, SOMASTERINGLUMINANCE );
    
    SOCONTENTASPECTRATIO = new Crestron.Logos.SplusObjects.StringOutput( SOCONTENTASPECTRATIO__AnalogSerialOutput__, this );
    m_StringOutputList.Add( SOCONTENTASPECTRATIO__AnalogSerialOutput__, SOCONTENTASPECTRATIO );
    
    SOGAMMATYPE = new Crestron.Logos.SplusObjects.StringOutput( SOGAMMATYPE__AnalogSerialOutput__, this );
    m_StringOutputList.Add( SOGAMMATYPE__AnalogSerialOutput__, SOGAMMATYPE );
    
    SOVERTICALRESOLUTION = new Crestron.Logos.SplusObjects.StringOutput( SOVERTICALRESOLUTION__AnalogSerialOutput__, this );
    m_StringOutputList.Add( SOVERTICALRESOLUTION__AnalogSerialOutput__, SOVERTICALRESOLUTION );
    
    SOCENTER16BY9STATUS = new Crestron.Logos.SplusObjects.StringOutput( SOCENTER16BY9STATUS__AnalogSerialOutput__, this );
    m_StringOutputList.Add( SOCENTER16BY9STATUS__AnalogSerialOutput__, SOCENTER16BY9STATUS );
    
    SOCENTER16BY9FACTOR = new Crestron.Logos.SplusObjects.StringOutput( SOCENTER16BY9FACTOR__AnalogSerialOutput__, this );
    m_StringOutputList.Add( SOCENTER16BY9FACTOR__AnalogSerialOutput__, SOCENTER16BY9FACTOR );
    
    SOCROPPINGMODE = new Crestron.Logos.SplusObjects.StringOutput( SOCROPPINGMODE__AnalogSerialOutput__, this );
    m_StringOutputList.Add( SOCROPPINGMODE__AnalogSerialOutput__, SOCROPPINGMODE );
    
    SOCROPPINGASPECTRATIO = new Crestron.Logos.SplusObjects.StringOutput( SOCROPPINGASPECTRATIO__AnalogSerialOutput__, this );
    m_StringOutputList.Add( SOCROPPINGASPECTRATIO__AnalogSerialOutput__, SOCROPPINGASPECTRATIO );
    
    SOP7MODE = new Crestron.Logos.SplusObjects.StringOutput( SOP7MODE__AnalogSerialOutput__, this );
    m_StringOutputList.Add( SOP7MODE__AnalogSerialOutput__, SOP7MODE );
    
    SOHDRBOOSTVALUEFB = new Crestron.Logos.SplusObjects.StringOutput( SOHDRBOOSTVALUEFB__AnalogSerialOutput__, this );
    m_StringOutputList.Add( SOHDRBOOSTVALUEFB__AnalogSerialOutput__, SOHDRBOOSTVALUEFB );
    
    SOCONTENTBASEDAUTOCROPPINGSTATUS = new Crestron.Logos.SplusObjects.StringOutput( SOCONTENTBASEDAUTOCROPPINGSTATUS__AnalogSerialOutput__, this );
    m_StringOutputList.Add( SOCONTENTBASEDAUTOCROPPINGSTATUS__AnalogSerialOutput__, SOCONTENTBASEDAUTOCROPPINGSTATUS );
    
    SOSHUTTERPOSITION = new Crestron.Logos.SplusObjects.StringOutput( SOSHUTTERPOSITION__AnalogSerialOutput__, this );
    m_StringOutputList.Add( SOSHUTTERPOSITION__AnalogSerialOutput__, SOSHUTTERPOSITION );
    
    SOOUTLETTEMP = new Crestron.Logos.SplusObjects.StringOutput( SOOUTLETTEMP__AnalogSerialOutput__, this );
    m_StringOutputList.Add( SOOUTLETTEMP__AnalogSerialOutput__, SOOUTLETTEMP );
    
    SOINLETTEMP = new Crestron.Logos.SplusObjects.StringOutput( SOINLETTEMP__AnalogSerialOutput__, this );
    m_StringOutputList.Add( SOINLETTEMP__AnalogSerialOutput__, SOINLETTEMP );
    
    SOHUMIDITY = new Crestron.Logos.SplusObjects.StringOutput( SOHUMIDITY__AnalogSerialOutput__, this );
    m_StringOutputList.Add( SOHUMIDITY__AnalogSerialOutput__, SOHUMIDITY );
    
    SOPOWERACVOLTAGE = new Crestron.Logos.SplusObjects.StringOutput( SOPOWERACVOLTAGE__AnalogSerialOutput__, this );
    m_StringOutputList.Add( SOPOWERACVOLTAGE__AnalogSerialOutput__, SOPOWERACVOLTAGE );
    
    SOWARPSTATUS = new Crestron.Logos.SplusObjects.StringOutput( SOWARPSTATUS__AnalogSerialOutput__, this );
    m_StringOutputList.Add( SOWARPSTATUS__AnalogSerialOutput__, SOWARPSTATUS );
    
    SO4CORNERSSTATUS = new Crestron.Logos.SplusObjects.StringOutput( SO4CORNERSSTATUS__AnalogSerialOutput__, this );
    m_StringOutputList.Add( SO4CORNERSSTATUS__AnalogSerialOutput__, SO4CORNERSSTATUS );
    
    SOBOWSTATUS = new Crestron.Logos.SplusObjects.StringOutput( SOBOWSTATUS__AnalogSerialOutput__, this );
    m_StringOutputList.Add( SOBOWSTATUS__AnalogSerialOutput__, SOBOWSTATUS );
    
    SOSYMMETRICSTATUS = new Crestron.Logos.SplusObjects.StringOutput( SOSYMMETRICSTATUS__AnalogSerialOutput__, this );
    m_StringOutputList.Add( SOSYMMETRICSTATUS__AnalogSerialOutput__, SOSYMMETRICSTATUS );
    
    IPPORT = new UShortParameter( IPPORT__Parameter__, this );
    m_ParameterList.Add( IPPORT__Parameter__, IPPORT );
    
    SPIPADDRESS = new StringParameter( SPIPADDRESS__Parameter__, this );
    m_ParameterList.Add( SPIPADDRESS__Parameter__, SPIPADDRESS );
    
    
    DISTARTCLIENT.OnDigitalPush.Add( new InputChangeHandlerWrapper( DISTARTCLIENT_OnPush_0, false ) );
    DISTARTCLIENT.OnDigitalRelease.Add( new InputChangeHandlerWrapper( DISTARTCLIENT_OnRelease_1, false ) );
    DIENABLETRACELOGS.OnDigitalPush.Add( new InputChangeHandlerWrapper( DIENABLETRACELOGS_OnPush_2, false ) );
    DIENABLETRACELOGS.OnDigitalRelease.Add( new InputChangeHandlerWrapper( DIENABLETRACELOGS_OnRelease_3, false ) );
    DIPOWERON.OnDigitalPush.Add( new InputChangeHandlerWrapper( DIPOWERON_OnPush_4, false ) );
    DIPOWEROFF.OnDigitalPush.Add( new InputChangeHandlerWrapper( DIPOWEROFF_OnPush_5, false ) );
    DIREBOOTPROJECTORCOMMAND.OnDigitalPush.Add( new InputChangeHandlerWrapper( DIREBOOTPROJECTORCOMMAND_OnPush_6, false ) );
    DIGOTOSTANDBYECOMODE.OnDigitalPush.Add( new InputChangeHandlerWrapper( DIGOTOSTANDBYECOMODE_OnPush_7, false ) );
    DISOURCEDVI1.OnDigitalPush.Add( new InputChangeHandlerWrapper( DISOURCEDVI1_OnPush_8, false ) );
    DISOURCEDVI2.OnDigitalPush.Add( new InputChangeHandlerWrapper( DISOURCEDVI2_OnPush_9, false ) );
    DISOURCEDISPLAYPORT1.OnDigitalPush.Add( new InputChangeHandlerWrapper( DISOURCEDISPLAYPORT1_OnPush_10, false ) );
    DISOURCEDISPLAYPORT2.OnDigitalPush.Add( new InputChangeHandlerWrapper( DISOURCEDISPLAYPORT2_OnPush_11, false ) );
    DISOURCEDUALDVISEQUENTIAL.OnDigitalPush.Add( new InputChangeHandlerWrapper( DISOURCEDUALDVISEQUENTIAL_OnPush_12, false ) );
    DISOURCEDUALDISPLAYPORTSEQUENTIAL.OnDigitalPush.Add( new InputChangeHandlerWrapper( DISOURCEDUALDISPLAYPORTSEQUENTIAL_OnPush_13, false ) );
    DISOURCEDUALDVICOLUMNS.OnDigitalPush.Add( new InputChangeHandlerWrapper( DISOURCEDUALDVICOLUMNS_OnPush_14, false ) );
    DISOURCEDUALDISPLAYPORTCOLUMNS.OnDigitalPush.Add( new InputChangeHandlerWrapper( DISOURCEDUALDISPLAYPORTCOLUMNS_OnPush_15, false ) );
    DISOURCEHDMI.OnDigitalPush.Add( new InputChangeHandlerWrapper( DISOURCEHDMI_OnPush_16, false ) );
    DISOURCEHDBASET.OnDigitalPush.Add( new InputChangeHandlerWrapper( DISOURCEHDBASET_OnPush_17, false ) );
    DISOURCESDI.OnDigitalPush.Add( new InputChangeHandlerWrapper( DISOURCESDI_OnPush_18, false ) );
    DICENTER_16BY9_ENABLE.OnDigitalPush.Add( new InputChangeHandlerWrapper( DICENTER_16BY9_ENABLE_OnPush_19, false ) );
    DICENTER_16BY9_DISABLE.OnDigitalPush.Add( new InputChangeHandlerWrapper( DICENTER_16BY9_DISABLE_OnPush_20, false ) );
    DISET_FACTOR.OnDigitalPush.Add( new InputChangeHandlerWrapper( DISET_FACTOR_OnPush_21, false ) );
    DISET_CROPPING_TO_MANUAL.OnDigitalPush.Add( new InputChangeHandlerWrapper( DISET_CROPPING_TO_MANUAL_OnPush_22, false ) );
    DISET_CROPPING_TO_ASPECT.OnDigitalPush.Add( new InputChangeHandlerWrapper( DISET_CROPPING_TO_ASPECT_OnPush_23, false ) );
    DISET_CROPPING_TO_AUTO.OnDigitalPush.Add( new InputChangeHandlerWrapper( DISET_CROPPING_TO_AUTO_OnPush_24, false ) );
    DISET_CROPPING_ASPECT_RATIO_TO_16_9.OnDigitalPush.Add( new InputChangeHandlerWrapper( DISET_CROPPING_ASPECT_RATIO_TO_16_9_OnPush_25, false ) );
    DISET_CROPPING_ASPECT_RATIO_TO_1_85.OnDigitalPush.Add( new InputChangeHandlerWrapper( DISET_CROPPING_ASPECT_RATIO_TO_1_85_OnPush_26, false ) );
    DISET_CROPPING_ASPECT_RATIO_TO_2_2.OnDigitalPush.Add( new InputChangeHandlerWrapper( DISET_CROPPING_ASPECT_RATIO_TO_2_2_OnPush_27, false ) );
    DISET_CROPPING_ASPECT_RATIO_TO_2_35.OnDigitalPush.Add( new InputChangeHandlerWrapper( DISET_CROPPING_ASPECT_RATIO_TO_2_35_OnPush_28, false ) );
    DISET_CROPPING_ASPECT_RATIO_TO_2_37.OnDigitalPush.Add( new InputChangeHandlerWrapper( DISET_CROPPING_ASPECT_RATIO_TO_2_37_OnPush_29, false ) );
    DISET_CROPPING_ASPECT_RATIO_TO_2_39.OnDigitalPush.Add( new InputChangeHandlerWrapper( DISET_CROPPING_ASPECT_RATIO_TO_2_39_OnPush_30, false ) );
    DISET_MANUAL_CROPPING_ASPECT_RATIO.OnDigitalPush.Add( new InputChangeHandlerWrapper( DISET_MANUAL_CROPPING_ASPECT_RATIO_OnPush_31, false ) );
    DIENABLEGAMUTREMAPPING.OnDigitalPush.Add( new InputChangeHandlerWrapper( DIENABLEGAMUTREMAPPING_OnPush_32, false ) );
    DIENABLEGAMUTREMAPPING.OnDigitalRelease.Add( new InputChangeHandlerWrapper( DIENABLEGAMUTREMAPPING_OnRelease_33, false ) );
    DIENABLE_CONTENT_BASED_AUTO_CROPPING.OnDigitalPush.Add( new InputChangeHandlerWrapper( DIENABLE_CONTENT_BASED_AUTO_CROPPING_OnPush_34, false ) );
    DIDISABLE_CONTENT_BASED_AUTO_CROPPING.OnDigitalPush.Add( new InputChangeHandlerWrapper( DIDISABLE_CONTENT_BASED_AUTO_CROPPING_OnPush_35, false ) );
    SIHDRBOOSTVALUE.OnSerialChange.Add( new InputChangeHandlerWrapper( SIHDRBOOSTVALUE_OnChange_36, false ) );
    DIREFRESHEDID.OnDigitalPush.Add( new InputChangeHandlerWrapper( DIREFRESHEDID_OnPush_37, false ) );
    DIOPENSHUTTER.OnDigitalPush.Add( new InputChangeHandlerWrapper( DIOPENSHUTTER_OnPush_38, false ) );
    DICLOSESHUTTER.OnDigitalPush.Add( new InputChangeHandlerWrapper( DICLOSESHUTTER_OnPush_39, false ) );
    DIZOOMIN.OnDigitalPush.Add( new InputChangeHandlerWrapper( DIZOOMIN_OnPush_40, false ) );
    DIZOOMOUT.OnDigitalPush.Add( new InputChangeHandlerWrapper( DIZOOMOUT_OnPush_41, false ) );
    DIFOCUSIN.OnDigitalPush.Add( new InputChangeHandlerWrapper( DIFOCUSIN_OnPush_42, false ) );
    DIFOCUSOUT.OnDigitalPush.Add( new InputChangeHandlerWrapper( DIFOCUSOUT_OnPush_43, false ) );
    DIIRISIN.OnDigitalPush.Add( new InputChangeHandlerWrapper( DIIRISIN_OnPush_44, false ) );
    DIIRISOUT.OnDigitalPush.Add( new InputChangeHandlerWrapper( DIIRISOUT_OnPush_45, false ) );
    DILENSSHIFTUP.OnDigitalPush.Add( new InputChangeHandlerWrapper( DILENSSHIFTUP_OnPush_46, false ) );
    DILENSSHIFTDOWN.OnDigitalPush.Add( new InputChangeHandlerWrapper( DILENSSHIFTDOWN_OnPush_47, false ) );
    DILENSSHIFTLEFT.OnDigitalPush.Add( new InputChangeHandlerWrapper( DILENSSHIFTLEFT_OnPush_48, false ) );
    DILENSSHIFTRIGHT.OnDigitalPush.Add( new InputChangeHandlerWrapper( DILENSSHIFTRIGHT_OnPush_49, false ) );
    DIENABLE_WARP.OnDigitalPush.Add( new InputChangeHandlerWrapper( DIENABLE_WARP_OnPush_50, false ) );
    DIDISABLE_WARP.OnDigitalPush.Add( new InputChangeHandlerWrapper( DIDISABLE_WARP_OnPush_51, false ) );
    DIENABLE_4_CORNERS.OnDigitalPush.Add( new InputChangeHandlerWrapper( DIENABLE_4_CORNERS_OnPush_52, false ) );
    DIDISABLE_4_CORNERS.OnDigitalPush.Add( new InputChangeHandlerWrapper( DIDISABLE_4_CORNERS_OnPush_53, false ) );
    DISET_4_CORNERS_TOP_LEFT.OnDigitalPush.Add( new InputChangeHandlerWrapper( DISET_4_CORNERS_TOP_LEFT_OnPush_54, false ) );
    DISET_4_CORNERS_BOTTOM_LEFT.OnDigitalPush.Add( new InputChangeHandlerWrapper( DISET_4_CORNERS_BOTTOM_LEFT_OnPush_55, false ) );
    DISET_4_CORNERS_TOP_RIGHT.OnDigitalPush.Add( new InputChangeHandlerWrapper( DISET_4_CORNERS_TOP_RIGHT_OnPush_56, false ) );
    DISET_4_CORNERS_BOTTOM_RIGHT.OnDigitalPush.Add( new InputChangeHandlerWrapper( DISET_4_CORNERS_BOTTOM_RIGHT_OnPush_57, false ) );
    DIENABLE_BOW.OnDigitalPush.Add( new InputChangeHandlerWrapper( DIENABLE_BOW_OnPush_58, false ) );
    DIDISABLE_BOW.OnDigitalPush.Add( new InputChangeHandlerWrapper( DIDISABLE_BOW_OnPush_59, false ) );
    DIENABLE_SYMMETRIC.OnDigitalPush.Add( new InputChangeHandlerWrapper( DIENABLE_SYMMETRIC_OnPush_60, false ) );
    DIDISABLE_SYMMETRIC.OnDigitalPush.Add( new InputChangeHandlerWrapper( DIDISABLE_SYMMETRIC_OnPush_61, false ) );
    DISET_BOW_TOP.OnDigitalPush.Add( new InputChangeHandlerWrapper( DISET_BOW_TOP_OnPush_62, false ) );
    DISET_BOW_BOTTOM.OnDigitalPush.Add( new InputChangeHandlerWrapper( DISET_BOW_BOTTOM_OnPush_63, false ) );
    AILASERPOWER.OnAnalogChange.Add( new InputChangeHandlerWrapper( AILASERPOWER_OnChange_64, false ) );
    AILEDPOWER.OnAnalogChange.Add( new InputChangeHandlerWrapper( AILEDPOWER_OnChange_65, false ) );
    DIRECALLPROFILE.OnDigitalPush.Add( new InputChangeHandlerWrapper( DIRECALLPROFILE_OnPush_66, false ) );
    DIRECALLPRESET.OnDigitalPush.Add( new InputChangeHandlerWrapper( DIRECALLPRESET_OnPush_67, false ) );
    DIENABLE_TRIGGER_1.OnDigitalPush.Add( new InputChangeHandlerWrapper( DIENABLE_TRIGGER_1_OnPush_68, false ) );
    DIDISABLE_TRIGGER_1.OnDigitalPush.Add( new InputChangeHandlerWrapper( DIDISABLE_TRIGGER_1_OnPush_69, false ) );
    DIENABLE_TRIGGER_2.OnDigitalPush.Add( new InputChangeHandlerWrapper( DIENABLE_TRIGGER_2_OnPush_70, false ) );
    DIDISABLE_TRIGGER_2.OnDigitalPush.Add( new InputChangeHandlerWrapper( DIDISABLE_TRIGGER_2_OnPush_71, false ) );
    DIENABLE_TRIGGER_3.OnDigitalPush.Add( new InputChangeHandlerWrapper( DIENABLE_TRIGGER_3_OnPush_72, false ) );
    DIDISABLE_TRIGGER_3.OnDigitalPush.Add( new InputChangeHandlerWrapper( DIDISABLE_TRIGGER_3_OnPush_73, false ) );
    TCPSOCKET.OnSocketConnect.Add( new SocketHandlerWrapper( TCPSOCKET_OnSocketConnect_74, false ) );
    TCPSOCKET.OnSocketDisconnect.Add( new SocketHandlerWrapper( TCPSOCKET_OnSocketDisconnect_75, false ) );
    TCPSOCKET.OnSocketStatus.Add( new SocketHandlerWrapper( TCPSOCKET_OnSocketStatus_76, false ) );
    TCPSOCKET.OnSocketReceive.Add( new SocketHandlerWrapper( TCPSOCKET_OnSocketReceive_77, true ) );
    
    _SplusNVRAM.PopulateCustomAttributeList( true );
    
    NVRAM = _SplusNVRAM;
    
}

public override void LogosSimplSharpInitialize()
{
    
    
}

public UserModuleClass_BARCOPULSEPROJECTORCOMMANDS ( string InstanceName, string ReferenceID, Crestron.Logos.SplusObjects.CrestronStringEncoding nEncodingType ) : base( InstanceName, ReferenceID, nEncodingType ) {}




const uint DISTARTCLIENT__DigitalInput__ = 0;
const uint DIENABLETRACELOGS__DigitalInput__ = 1;
const uint DIPOWERON__DigitalInput__ = 2;
const uint DIPOWEROFF__DigitalInput__ = 3;
const uint DIREBOOTPROJECTORCOMMAND__DigitalInput__ = 4;
const uint DIGOTOSTANDBYECOMODE__DigitalInput__ = 5;
const uint DISOURCEDVI1__DigitalInput__ = 6;
const uint DISOURCEDVI2__DigitalInput__ = 7;
const uint DISOURCEDISPLAYPORT1__DigitalInput__ = 8;
const uint DISOURCEDISPLAYPORT2__DigitalInput__ = 9;
const uint DISOURCEDUALDVICOLUMNS__DigitalInput__ = 10;
const uint DISOURCEDUALDISPLAYPORTCOLUMNS__DigitalInput__ = 11;
const uint DISOURCEDUALDVISEQUENTIAL__DigitalInput__ = 12;
const uint DISOURCEDUALDISPLAYPORTSEQUENTIAL__DigitalInput__ = 13;
const uint DISOURCEHDMI__DigitalInput__ = 14;
const uint DISOURCEHDBASET__DigitalInput__ = 15;
const uint DISOURCESDI__DigitalInput__ = 16;
const uint DIREFRESHEDID__DigitalInput__ = 17;
const uint DICENTER_16BY9_ENABLE__DigitalInput__ = 18;
const uint DICENTER_16BY9_DISABLE__DigitalInput__ = 19;
const uint DISET_FACTOR__DigitalInput__ = 20;
const uint DISET_CROPPING_TO_MANUAL__DigitalInput__ = 21;
const uint DISET_CROPPING_TO_ASPECT__DigitalInput__ = 22;
const uint DISET_CROPPING_TO_AUTO__DigitalInput__ = 23;
const uint DISET_CROPPING_ASPECT_RATIO_TO_16_9__DigitalInput__ = 24;
const uint DISET_CROPPING_ASPECT_RATIO_TO_1_85__DigitalInput__ = 25;
const uint DISET_CROPPING_ASPECT_RATIO_TO_2_2__DigitalInput__ = 26;
const uint DISET_CROPPING_ASPECT_RATIO_TO_2_35__DigitalInput__ = 27;
const uint DISET_CROPPING_ASPECT_RATIO_TO_2_37__DigitalInput__ = 28;
const uint DISET_CROPPING_ASPECT_RATIO_TO_2_39__DigitalInput__ = 29;
const uint DISET_MANUAL_CROPPING_ASPECT_RATIO__DigitalInput__ = 30;
const uint DIENABLEGAMUTREMAPPING__DigitalInput__ = 31;
const uint DIENABLE_CONTENT_BASED_AUTO_CROPPING__DigitalInput__ = 32;
const uint DIDISABLE_CONTENT_BASED_AUTO_CROPPING__DigitalInput__ = 33;
const uint DIOPENSHUTTER__DigitalInput__ = 34;
const uint DICLOSESHUTTER__DigitalInput__ = 35;
const uint DIZOOMIN__DigitalInput__ = 36;
const uint DIZOOMOUT__DigitalInput__ = 37;
const uint DIFOCUSIN__DigitalInput__ = 38;
const uint DIFOCUSOUT__DigitalInput__ = 39;
const uint DIIRISIN__DigitalInput__ = 40;
const uint DIIRISOUT__DigitalInput__ = 41;
const uint DILENSSHIFTUP__DigitalInput__ = 42;
const uint DILENSSHIFTDOWN__DigitalInput__ = 43;
const uint DILENSSHIFTLEFT__DigitalInput__ = 44;
const uint DILENSSHIFTRIGHT__DigitalInput__ = 45;
const uint DIENABLE_WARP__DigitalInput__ = 46;
const uint DIDISABLE_WARP__DigitalInput__ = 47;
const uint DIENABLE_4_CORNERS__DigitalInput__ = 48;
const uint DIDISABLE_4_CORNERS__DigitalInput__ = 49;
const uint DISET_4_CORNERS_TOP_LEFT__DigitalInput__ = 50;
const uint DISET_4_CORNERS_BOTTOM_LEFT__DigitalInput__ = 51;
const uint DISET_4_CORNERS_TOP_RIGHT__DigitalInput__ = 52;
const uint DISET_4_CORNERS_BOTTOM_RIGHT__DigitalInput__ = 53;
const uint DIENABLE_BOW__DigitalInput__ = 54;
const uint DIDISABLE_BOW__DigitalInput__ = 55;
const uint DIENABLE_SYMMETRIC__DigitalInput__ = 56;
const uint DIDISABLE_SYMMETRIC__DigitalInput__ = 57;
const uint DISET_BOW_TOP__DigitalInput__ = 58;
const uint DISET_BOW_BOTTOM__DigitalInput__ = 59;
const uint DIISCINEMASCOPE__DigitalInput__ = 60;
const uint DIRECALLPROFILE__DigitalInput__ = 61;
const uint DIRECALLPRESET__DigitalInput__ = 62;
const uint DIENABLE_TRIGGER_1__DigitalInput__ = 63;
const uint DIDISABLE_TRIGGER_1__DigitalInput__ = 64;
const uint DIENABLE_TRIGGER_2__DigitalInput__ = 65;
const uint DIDISABLE_TRIGGER_2__DigitalInput__ = 66;
const uint DIENABLE_TRIGGER_3__DigitalInput__ = 67;
const uint DIDISABLE_TRIGGER_3__DigitalInput__ = 68;
const uint AICROP_VALUE_LEFT__AnalogSerialInput__ = 0;
const uint AICROP_VALUE_RIGHT__AnalogSerialInput__ = 1;
const uint AICROP_VALUE_TOP__AnalogSerialInput__ = 2;
const uint AICROP_VALUE_BOTTOM__AnalogSerialInput__ = 3;
const uint AILASERPOWER__AnalogSerialInput__ = 4;
const uint AILEDPOWER__AnalogSerialInput__ = 5;
const uint AIPRESETNUMBER__AnalogSerialInput__ = 6;
const uint AI4CORNERSX__AnalogSerialInput__ = 7;
const uint AI4CORNERSY__AnalogSerialInput__ = 8;
const uint AIBOWANGLE__AnalogSerialInput__ = 9;
const uint AIBOWLENGTH__AnalogSerialInput__ = 10;
const uint SIFACTOR__AnalogSerialInput__ = 11;
const uint SIHDRBOOSTVALUE__AnalogSerialInput__ = 12;
const uint SIPROFILENAME__AnalogSerialInput__ = 13;
const uint DOCLIENTCONNECTED__DigitalOutput__ = 0;
const uint AOCONNECTIONSTATUS__AnalogSerialOutput__ = 0;
const uint AOLASERPOWER__AnalogSerialOutput__ = 1;
const uint AOLEDPOWER__AnalogSerialOutput__ = 2;
const uint AOLASERONHOURS__AnalogSerialOutput__ = 3;
const uint AOCROPPINGMANUALLEFT__AnalogSerialOutput__ = 4;
const uint AOCROPPINGMANUALRIGHT__AnalogSerialOutput__ = 5;
const uint AOCROPPINGMANUALTOP__AnalogSerialOutput__ = 6;
const uint AOCROPPINGMANUALBOTTOM__AnalogSerialOutput__ = 7;
const uint SOFROMDEVICE__AnalogSerialOutput__ = 8;
const uint SOSYSTEMSTATE__AnalogSerialOutput__ = 9;
const uint SOINPUTSOURCE__AnalogSerialOutput__ = 10;
const uint SODETECTEDSIGNAL__AnalogSerialOutput__ = 11;
const uint SOCOLORSPACE__AnalogSerialOutput__ = 12;
const uint SOMASTERINGLUMINANCE__AnalogSerialOutput__ = 13;
const uint SOCONTENTASPECTRATIO__AnalogSerialOutput__ = 14;
const uint SOGAMMATYPE__AnalogSerialOutput__ = 15;
const uint SOVERTICALRESOLUTION__AnalogSerialOutput__ = 16;
const uint SOCENTER16BY9STATUS__AnalogSerialOutput__ = 17;
const uint SOCENTER16BY9FACTOR__AnalogSerialOutput__ = 18;
const uint SOCROPPINGMODE__AnalogSerialOutput__ = 19;
const uint SOCROPPINGASPECTRATIO__AnalogSerialOutput__ = 20;
const uint SOP7MODE__AnalogSerialOutput__ = 21;
const uint SOHDRBOOSTVALUEFB__AnalogSerialOutput__ = 22;
const uint SOCONTENTBASEDAUTOCROPPINGSTATUS__AnalogSerialOutput__ = 23;
const uint SOSHUTTERPOSITION__AnalogSerialOutput__ = 24;
const uint SOOUTLETTEMP__AnalogSerialOutput__ = 25;
const uint SOINLETTEMP__AnalogSerialOutput__ = 26;
const uint SOHUMIDITY__AnalogSerialOutput__ = 27;
const uint SOPOWERACVOLTAGE__AnalogSerialOutput__ = 28;
const uint SOWARPSTATUS__AnalogSerialOutput__ = 29;
const uint SO4CORNERSSTATUS__AnalogSerialOutput__ = 30;
const uint SOBOWSTATUS__AnalogSerialOutput__ = 31;
const uint SOSYMMETRICSTATUS__AnalogSerialOutput__ = 32;
const uint SPIPADDRESS__Parameter__ = 10;
const uint IPPORT__Parameter__ = 11;

[SplusStructAttribute(-1, true, false)]
public class SplusNVRAM : SplusStructureBase
{

    public SplusNVRAM( SplusObject __caller__ ) : base( __caller__ ) {}
    
    
}

SplusNVRAM _SplusNVRAM = null;

public class __CEvent__ : CEvent
{
    public __CEvent__() {}
    public void Close() { base.Close(); }
    public int Reset() { return base.Reset() ? 1 : 0; }
    public int Set() { return base.Set() ? 1 : 0; }
    public int Wait( int timeOutInMs ) { return base.Wait( timeOutInMs ) ? 1 : 0; }
}
public class __CMutex__ : CMutex
{
    public __CMutex__() {}
    public void Close() { base.Close(); }
    public void ReleaseMutex() { base.ReleaseMutex(); }
    public int WaitForMutex() { return base.WaitForMutex() ? 1 : 0; }
}
 public int IsNull( object obj ){ return (obj == null) ? 1 : 0; }
}


}

using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using System.Linq;
using Crestron;
using Crestron.Logos.SplusLibrary;
using Crestron.Logos.SplusObjects;
using Crestron.SimplSharp;

namespace UserModule_SEND_WOL_PACKET
{
    public class UserModuleClass_SEND_WOL_PACKET : SplusObject
    {
        static CCriticalSection g_criticalSection = new CCriticalSection();
        
        
        
        
        
        Crestron.Logos.SplusObjects.DigitalInput DIENABLEUDP;
        Crestron.Logos.SplusObjects.DigitalInput DISENDWOLPACKET;
        SplusUdpSocket MYUDP;
        StringParameter SPBROADCASTADDRESS;
        UShortParameter IPPORT;
        StringParameter SPMACADDRESSPART1;
        StringParameter SPMACADDRESSPART2;
        StringParameter SPMACADDRESSPART3;
        StringParameter SPMACADDRESSPART4;
        StringParameter SPMACADDRESSPART5;
        StringParameter SPMACADDRESSPART6;
        ushort IUDPENABLED = 0;
        CrestronString GSMAGICPACKET;
        object DIENABLEUDP_OnPush_0 ( Object __EventInfo__ )
        
            { 
            Crestron.Logos.SplusObjects.SignalEventArgs __SignalEventArg__ = (Crestron.Logos.SplusObjects.SignalEventArgs)__EventInfo__;
            try
            {
                SplusExecutionContext __context__ = SplusThreadStartCode(__SignalEventArg__);
                short SISTATUS = 0;
                
                
                __context__.SourceCodeLine = 102;
                IUDPENABLED = (ushort) ( 1 ) ; 
                __context__.SourceCodeLine = 104;
                SISTATUS = (short) ( Functions.SocketUDP_Enable( MYUDP , SPBROADCASTADDRESS  , (ushort)( IPPORT  .Value ) ) ) ; 
                __context__.SourceCodeLine = 106;
                if ( Functions.TestForTrue  ( ( Functions.BoolToInt ( SISTATUS < 0 ))  ) ) 
                    {
                    __context__.SourceCodeLine = 107;
                    Trace( "Error listening to {0} on port  {1:d} - [{2:d}]", SPBROADCASTADDRESS , (short)IPPORT  .Value, (short)SISTATUS) ; 
                    }
                
                
                
            }
            catch(Exception e) { ObjectCatchHandler(e); }
            finally { ObjectFinallyHandler( __SignalEventArg__ ); }
            return this;
            
        }
        
    object DIENABLEUDP_OnRelease_1 ( Object __EventInfo__ )
    
        { 
        Crestron.Logos.SplusObjects.SignalEventArgs __SignalEventArg__ = (Crestron.Logos.SplusObjects.SignalEventArgs)__EventInfo__;
        try
        {
            SplusExecutionContext __context__ = SplusThreadStartCode(__SignalEventArg__);
            short SISTATUS = 0;
            
            
            __context__.SourceCodeLine = 114;
            IUDPENABLED = (ushort) ( 0 ) ; 
            __context__.SourceCodeLine = 116;
            SISTATUS = (short) ( Functions.SocketUDP_Disable( MYUDP ) ) ; 
            __context__.SourceCodeLine = 118;
            if ( Functions.TestForTrue  ( ( Functions.BoolToInt ( SISTATUS < 0 ))  ) ) 
                {
                __context__.SourceCodeLine = 119;
                Trace( "Error disabling the UDP Socket [{0:d}]", (short)SISTATUS) ; 
                }
            
            
            
        }
        catch(Exception e) { ObjectCatchHandler(e); }
        finally { ObjectFinallyHandler( __SignalEventArg__ ); }
        return this;
        
    }
    
object MYUDP_OnSocketReceive_2 ( Object __Info__ )

    { 
    SocketEventInfo __SocketInfo__ = (SocketEventInfo)__Info__;
    try
    {
        SplusExecutionContext __context__ = SplusThreadStartCode(__SocketInfo__);
        short ISBROADCAST = 0;
        
        short ISMULTICAST = 0;
        
        short LOCALSTATUS = 0;
        
        CrestronString SENDERIPADDRESS;
        SENDERIPADDRESS  = new CrestronString( Crestron.Logos.SplusObjects.CrestronStringEncoding.eEncodingASCII, 32, this );
        
        
        __context__.SourceCodeLine = 130;
        ISBROADCAST = (short) ( Functions.SocketIsBroadcast( MYUDP ) ) ; 
        __context__.SourceCodeLine = 132;
        if ( Functions.TestForTrue  ( ( Functions.BoolToInt ( ISBROADCAST < 0 ))  ) ) 
            {
            __context__.SourceCodeLine = 133;
            Trace( "IsBroadcast returned error: {0:d}\r\n", (short)ISBROADCAST) ; 
            }
        
        __context__.SourceCodeLine = 135;
        ISMULTICAST = (short) ( Functions.SocketIsMulticast( MYUDP ) ) ; 
        __context__.SourceCodeLine = 137;
        if ( Functions.TestForTrue  ( ( Functions.BoolToInt ( ISMULTICAST < 0 ))  ) ) 
            {
            __context__.SourceCodeLine = 138;
            Trace( "IsMulticast returned error: {0:d}\r\n", (short)ISMULTICAST) ; 
            }
        
        __context__.SourceCodeLine = 140;
        LOCALSTATUS = (short) ( Functions.SocketGetSenderIPAddress( MYUDP , ref SENDERIPADDRESS ) ) ; 
        __context__.SourceCodeLine = 142;
        if ( Functions.TestForTrue  ( ( Functions.BoolToInt ( LOCALSTATUS < 0 ))  ) ) 
            {
            __context__.SourceCodeLine = 143;
            Trace( "SocketGetSenderIPAddress returned error: {0:d}\r\n", (short)LOCALSTATUS) ; 
            }
        
        __context__.SourceCodeLine = 145;
        if ( Functions.TestForTrue  ( ( ISBROADCAST)  ) ) 
            {
            __context__.SourceCodeLine = 146;
            Trace( "Broadcast data received from {0}\r\n", SENDERIPADDRESS ) ; 
            }
        
        else 
            {
            __context__.SourceCodeLine = 148;
            if ( Functions.TestForTrue  ( ( ISMULTICAST)  ) ) 
                {
                __context__.SourceCodeLine = 149;
                Trace( "Multicast data received from {0}\r\n", SENDERIPADDRESS ) ; 
                }
            
            else 
                {
                __context__.SourceCodeLine = 152;
                Trace( "Unicast data received from {0}\r\n", SENDERIPADDRESS ) ; 
                }
            
            }
        
        __context__.SourceCodeLine = 154;
        if ( Functions.TestForTrue  ( ( Functions.BoolToInt ( Functions.Length( MYUDP.SocketRxBuf ) < 256 ))  ) ) 
            {
            __context__.SourceCodeLine = 155;
            Trace( "RX: {0}", MYUDP .  SocketRxBuf ) ; 
            }
        
        __context__.SourceCodeLine = 160;
        Functions.ClearBuffer ( MYUDP .  SocketRxBuf ) ; 
        
        
    }
    catch(Exception e) { ObjectCatchHandler(e); }
    finally { ObjectFinallyHandler( __SocketInfo__ ); }
    return this;
    
}

object DISENDWOLPACKET_OnPush_3 ( Object __EventInfo__ )

    { 
    Crestron.Logos.SplusObjects.SignalEventArgs __SignalEventArg__ = (Crestron.Logos.SplusObjects.SignalEventArgs)__EventInfo__;
    try
    {
        SplusExecutionContext __context__ = SplusThreadStartCode(__SignalEventArg__);
        ushort I = 0;
        
        short SISTATUS = 0;
        
        CrestronString SHEADER;
        SHEADER  = new CrestronString( Crestron.Logos.SplusObjects.CrestronStringEncoding.eEncodingASCII, 6, this );
        
        CrestronString SMACADDRESS;
        SMACADDRESS  = new CrestronString( Crestron.Logos.SplusObjects.CrestronStringEncoding.eEncodingASCII, 6, this );
        
        CrestronString SSUMMEDMACADDRESS;
        SSUMMEDMACADDRESS  = new CrestronString( Crestron.Logos.SplusObjects.CrestronStringEncoding.eEncodingASCII, 100, this );
        
        
        __context__.SourceCodeLine = 171;
        if ( Functions.TestForTrue  ( ( Functions.BoolToInt (DIENABLEUDP  .Value == 1))  ) ) 
            { 
            __context__.SourceCodeLine = 174;
            GSMAGICPACKET  .UpdateValue ( ""  ) ; 
            __context__.SourceCodeLine = 175;
            SSUMMEDMACADDRESS  .UpdateValue ( ""  ) ; 
            __context__.SourceCodeLine = 176;
            SHEADER  .UpdateValue ( Functions.Chr (  (int) ( 255 ) ) + Functions.Chr (  (int) ( 255 ) ) + Functions.Chr (  (int) ( 255 ) ) + Functions.Chr (  (int) ( 255 ) ) + Functions.Chr (  (int) ( 255 ) ) + Functions.Chr (  (int) ( 255 ) )  ) ; 
            __context__.SourceCodeLine = 179;
            SMACADDRESS  .UpdateValue ( SPMACADDRESSPART1 + SPMACADDRESSPART2 + SPMACADDRESSPART3 + SPMACADDRESSPART4 + SPMACADDRESSPART5 + SPMACADDRESSPART6  ) ; 
            __context__.SourceCodeLine = 182;
            ushort __FN_FORSTART_VAL__1 = (ushort) ( 1 ) ;
            ushort __FN_FOREND_VAL__1 = (ushort)16; 
            int __FN_FORSTEP_VAL__1 = (int)1; 
            for ( I  = __FN_FORSTART_VAL__1; (__FN_FORSTEP_VAL__1 > 0)  ? ( (I  >= __FN_FORSTART_VAL__1) && (I  <= __FN_FOREND_VAL__1) ) : ( (I  <= __FN_FORSTART_VAL__1) && (I  >= __FN_FOREND_VAL__1) ) ; I  += (ushort)__FN_FORSTEP_VAL__1) 
                { 
                __context__.SourceCodeLine = 184;
                SSUMMEDMACADDRESS  .UpdateValue ( SSUMMEDMACADDRESS + SMACADDRESS  ) ; 
                __context__.SourceCodeLine = 182;
                } 
            
            __context__.SourceCodeLine = 188;
            GSMAGICPACKET  .UpdateValue ( SHEADER + SSUMMEDMACADDRESS  ) ; 
            __context__.SourceCodeLine = 189;
            Trace( "Magic Packet: {0}\r\n", GSMAGICPACKET ) ; 
            __context__.SourceCodeLine = 192;
            SISTATUS = (short) ( Functions.SocketSend( MYUDP , GSMAGICPACKET ) ) ; 
            __context__.SourceCodeLine = 194;
            if ( Functions.TestForTrue  ( ( Functions.BoolToInt ( SISTATUS < 0 ))  ) ) 
                {
                __context__.SourceCodeLine = 195;
                Trace( "Error Sending Magic Packet: {0:d}\r\n", (short)SISTATUS) ; 
                }
            
            } 
        
        
        
    }
    catch(Exception e) { ObjectCatchHandler(e); }
    finally { ObjectFinallyHandler( __SignalEventArg__ ); }
    return this;
    
}

public override object FunctionMain (  object __obj__ ) 
    { 
    try
    {
        SplusExecutionContext __context__ = SplusFunctionMainStartCode();
        
        __context__.SourceCodeLine = 202;
        WaitForInitializationComplete ( ) ; 
        
        
    }
    catch(Exception e) { ObjectCatchHandler(e); }
    finally { ObjectFinallyHandler(); }
    return __obj__;
    }
    

public override void LogosSplusInitialize()
{
    SocketInfo __socketinfo__ = new SocketInfo( 1, this );
    InitialParametersClass.ResolveHostName = __socketinfo__.ResolveHostName;
    _SplusNVRAM = new SplusNVRAM( this );
    GSMAGICPACKET  = new CrestronString( Crestron.Logos.SplusObjects.CrestronStringEncoding.eEncodingASCII, 110, this );
    MYUDP  = new SplusUdpSocket ( 1024, this );
    
    DIENABLEUDP = new Crestron.Logos.SplusObjects.DigitalInput( DIENABLEUDP__DigitalInput__, this );
    m_DigitalInputList.Add( DIENABLEUDP__DigitalInput__, DIENABLEUDP );
    
    DISENDWOLPACKET = new Crestron.Logos.SplusObjects.DigitalInput( DISENDWOLPACKET__DigitalInput__, this );
    m_DigitalInputList.Add( DISENDWOLPACKET__DigitalInput__, DISENDWOLPACKET );
    
    IPPORT = new UShortParameter( IPPORT__Parameter__, this );
    m_ParameterList.Add( IPPORT__Parameter__, IPPORT );
    
    SPBROADCASTADDRESS = new StringParameter( SPBROADCASTADDRESS__Parameter__, this );
    m_ParameterList.Add( SPBROADCASTADDRESS__Parameter__, SPBROADCASTADDRESS );
    
    SPMACADDRESSPART1 = new StringParameter( SPMACADDRESSPART1__Parameter__, this );
    m_ParameterList.Add( SPMACADDRESSPART1__Parameter__, SPMACADDRESSPART1 );
    
    SPMACADDRESSPART2 = new StringParameter( SPMACADDRESSPART2__Parameter__, this );
    m_ParameterList.Add( SPMACADDRESSPART2__Parameter__, SPMACADDRESSPART2 );
    
    SPMACADDRESSPART3 = new StringParameter( SPMACADDRESSPART3__Parameter__, this );
    m_ParameterList.Add( SPMACADDRESSPART3__Parameter__, SPMACADDRESSPART3 );
    
    SPMACADDRESSPART4 = new StringParameter( SPMACADDRESSPART4__Parameter__, this );
    m_ParameterList.Add( SPMACADDRESSPART4__Parameter__, SPMACADDRESSPART4 );
    
    SPMACADDRESSPART5 = new StringParameter( SPMACADDRESSPART5__Parameter__, this );
    m_ParameterList.Add( SPMACADDRESSPART5__Parameter__, SPMACADDRESSPART5 );
    
    SPMACADDRESSPART6 = new StringParameter( SPMACADDRESSPART6__Parameter__, this );
    m_ParameterList.Add( SPMACADDRESSPART6__Parameter__, SPMACADDRESSPART6 );
    
    
    DIENABLEUDP.OnDigitalPush.Add( new InputChangeHandlerWrapper( DIENABLEUDP_OnPush_0, false ) );
    DIENABLEUDP.OnDigitalRelease.Add( new InputChangeHandlerWrapper( DIENABLEUDP_OnRelease_1, false ) );
    MYUDP.OnSocketReceive.Add( new SocketHandlerWrapper( MYUDP_OnSocketReceive_2, false ) );
    DISENDWOLPACKET.OnDigitalPush.Add( new InputChangeHandlerWrapper( DISENDWOLPACKET_OnPush_3, false ) );
    
    _SplusNVRAM.PopulateCustomAttributeList( true );
    
    NVRAM = _SplusNVRAM;
    
}

public override void LogosSimplSharpInitialize()
{
    
    
}

public UserModuleClass_SEND_WOL_PACKET ( string InstanceName, string ReferenceID, Crestron.Logos.SplusObjects.CrestronStringEncoding nEncodingType ) : base( InstanceName, ReferenceID, nEncodingType ) {}




const uint DIENABLEUDP__DigitalInput__ = 0;
const uint DISENDWOLPACKET__DigitalInput__ = 1;
const uint SPBROADCASTADDRESS__Parameter__ = 10;
const uint IPPORT__Parameter__ = 11;
const uint SPMACADDRESSPART1__Parameter__ = 12;
const uint SPMACADDRESSPART2__Parameter__ = 13;
const uint SPMACADDRESSPART3__Parameter__ = 14;
const uint SPMACADDRESSPART4__Parameter__ = 15;
const uint SPMACADDRESSPART5__Parameter__ = 16;
const uint SPMACADDRESSPART6__Parameter__ = 17;

[SplusStructAttribute(-1, true, false)]
public class SplusNVRAM : SplusStructureBase
{

    public SplusNVRAM( SplusObject __caller__ ) : base( __caller__ ) {}
    
    
}

SplusNVRAM _SplusNVRAM = null;

public class __CEvent__ : CEvent
{
    public __CEvent__() {}
    public void Close() { base.Close(); }
    public int Reset() { return base.Reset() ? 1 : 0; }
    public int Set() { return base.Set() ? 1 : 0; }
    public int Wait( int timeOutInMs ) { return base.Wait( timeOutInMs ) ? 1 : 0; }
}
public class __CMutex__ : CMutex
{
    public __CMutex__() {}
    public void Close() { base.Close(); }
    public void ReleaseMutex() { base.ReleaseMutex(); }
    public int WaitForMutex() { return base.WaitForMutex() ? 1 : 0; }
}
 public int IsNull( object obj ){ return (obj == null) ? 1 : 0; }
}


}

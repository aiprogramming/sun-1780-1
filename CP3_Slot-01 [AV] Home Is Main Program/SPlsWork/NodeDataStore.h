namespace NodeDataStore;
        // class declarations
         class SetupConsole;
         class NodeEvents;
         class fileHelper;
         class NodeList;
         class CoreDataStore;
         class NodeDataEntry;
         class Zone;
         class ActionQueue;
         class NodeListEntry;
         class NodeData;
         class NodeDataFile;
         class XSig;
         class NodeDataJoin;
         class sqlHelper;
         class NodeListFile;
         class NodeListDB;
         class NodeDataDB;
         class Node;
         class DataChangeEventArgs;
     class SetupConsole 
    {
        // class delegates

        // class events

        // class functions
        STRING_FUNCTION processSetupCommand ( STRING cmd );
        SIGNED_LONG_INTEGER_FUNCTION GetHashCode ();
        STRING_FUNCTION ToString ();

        // class variables
        static STRING version[];

        // class properties
    };

     class NodeEvents 
    {
        // class delegates

        // class events
        EventHandler OnCore_CMD ( NodeEvents sender, DataChangeEventArgs e );
        EventHandler OnCore_Values_TX ( NodeEvents sender, DataChangeEventArgs e );
        EventHandler OnCore_Labels_TX ( NodeEvents sender, DataChangeEventArgs e );
        EventHandler OnCore_Defaults_TX ( NodeEvents sender, DataChangeEventArgs e );
        EventHandler OnWaiting ( NodeEvents sender, DataChangeEventArgs e );
        EventHandler OnConnected ( NodeEvents sender, DataChangeEventArgs e );
        EventHandler OnInit ( NodeEvents sender, DataChangeEventArgs e );
        EventHandler OnReady ( NodeEvents sender, DataChangeEventArgs e );

        // class functions
        SIGNED_LONG_INTEGER_FUNCTION GetHashCode ();
        STRING_FUNCTION ToString ();

        // class variables
        static STRING version[];

        // class properties
        SIGNED_LONG_INTEGER ID;
        STRING Name[];
        INTEGER NodeType;
        STRING ValuesRX[];
        STRING LabelsRX[];
        STRING DefaultsRX[];
    };

     class fileHelper 
    {
        // class delegates

        // class events

        // class functions
        SIGNED_LONG_INTEGER_FUNCTION GetHashCode ();
        STRING_FUNCTION ToString ();

        // class variables
        static STRING version[];

        // class properties
    };

     class NodeList 
    {
        // class delegates

        // class events

        // class functions
        FUNCTION read ( STRING file );
        FUNCTION write ( STRING file );
        FUNCTION addNodeListEntry ( SIGNED_LONG_INTEGER id , SIGNED_LONG_INTEGER type , STRING name );
        STRING_FUNCTION getNodeList ();
        FUNCTION initNodes ();
        STRING_FUNCTION FixNodesWithMissingData ();
        SIGNED_LONG_INTEGER_FUNCTION listSize ();
        SIGNED_LONG_INTEGER_FUNCTION ConvertNodeID ( SIGNED_LONG_INTEGER id );
        SIGNED_LONG_INTEGER_FUNCTION GetHashCode ();
        STRING_FUNCTION ToString ();

        // class variables
        static STRING version[];

        // class properties
    };

    static class CoreDataStore 
    {
        // class delegates
        delegate INTEGER_FUNCTION CoreInitialized ( INTEGER state );

        // class events

        // class functions
        static FUNCTION startCore ();
        static FUNCTION setupNodeList ();
        static FUNCTION Send_NodeNotify ( INTEGER nodeID , INTEGER data );
        static FUNCTION Send_NodeType ( INTEGER nodeID , INTEGER data );
        static FUNCTION Send_NodeName ( INTEGER nodeID , STRING data );
        static FUNCTION Send_NodeValuesRX ( INTEGER nodeID , STRING data );
        static FUNCTION Send_NodeLabelsRX ( INTEGER nodeID , STRING data );
        static FUNCTION Send_NodeDefaultsRX ( INTEGER nodeID , STRING data );
        static FUNCTION setCoreHoldingPenNextNode ( SIGNED_LONG_INTEGER value );
        static FUNCTION setCMD ( SIMPLSHARPSTRING value );
        SIGNED_LONG_INTEGER_FUNCTION GetHashCode ();
        STRING_FUNCTION ToString ();

        // class variables
        static NodeList myNodeList;
        static ActionQueue myQueue;
        static SetupConsole myConsole;
        static XSig myXSig;
        static Zone myZones;
        static STRING nodeList[];
        static SIGNED_LONG_INTEGER nodeListMode;
        static SIGNED_LONG_INTEGER programDataPoints;
        static STRING version[];
        static STRING rmDir[];
        static STRING dbFile[];
        static STRING connectionString[];

        // class properties
        DelegateProperty CoreInitialized Initialized;
    };

     class NodeDataEntry 
    {
        // class delegates

        // class events

        // class functions
        SIGNED_LONG_INTEGER_FUNCTION GetHashCode ();
        STRING_FUNCTION ToString ();

        // class variables
        static STRING version[];

        // class properties
    };

     class Zone 
    {
        // class delegates

        // class events

        // class functions
        STRING_FUNCTION listZones ();
        STRING_FUNCTION getInfo ( SIGNED_LONG_INTEGER i );
        STRING_FUNCTION getName ( SIGNED_LONG_INTEGER i );
        STRING_FUNCTION setName ( SIGNED_LONG_INTEGER i , STRING name );
        STRING_FUNCTION getSource ( SIGNED_LONG_INTEGER i );
        STRING_FUNCTION setSource ( SIGNED_LONG_INTEGER i , SIGNED_LONG_INTEGER source );
        STRING_FUNCTION getSourceList ( SIGNED_LONG_INTEGER i );
        SIGNED_LONG_INTEGER_FUNCTION GetHashCode ();
        STRING_FUNCTION ToString ();

        // class variables
        static STRING version[];

        // class properties
    };

     class ActionQueue 
    {
        // class delegates

        // class events

        // class functions
        FUNCTION Init ();
        FUNCTION createCoreAction ( STRING action , SIGNED_LONG_INTEGER nodeID , SIGNED_LONG_INTEGER join , STRING value );
        SIGNED_LONG_INTEGER_FUNCTION GetHashCode ();
        STRING_FUNCTION ToString ();

        // class variables
        static STRING version[];

        // class properties
    };

     class NodeData 
    {
        // class delegates

        // class events

        // class functions
        FUNCTION init ();
        FUNCTION read ();
        FUNCTION capture ();
        FUNCTION update ();
        FUNCTION write ();
        FUNCTION sendValues ();
        FUNCTION setNodeID ( SIGNED_LONG_INTEGER nodeID );
        STRING_FUNCTION getNodeInfo ();
        STRING_FUNCTION getNodeName ();
        STRING_FUNCTION setNodeName ( STRING nodeName );
        STRING_FUNCTION getNodeType ();
        STRING_FUNCTION setNodeType ( SIGNED_LONG_INTEGER type );
        STRING_FUNCTION getNodeValue ( SIGNED_LONG_INTEGER valueID );
        STRING_FUNCTION setNodeValue ( SIGNED_LONG_INTEGER valueID , STRING value );
        STRING_FUNCTION nodeRefresh ();
        STRING_FUNCTION nodeDefaults ();
        STRING_FUNCTION nodeUpdate ();
        SIGNED_LONG_INTEGER_FUNCTION getValueType ( SIGNED_LONG_INTEGER join );
        SIGNED_LONG_INTEGER_FUNCTION GetHashCode ();
        STRING_FUNCTION ToString ();

        // class variables
        static STRING version[];

        // class properties
        SIGNED_LONG_INTEGER id;
    };

     class NodeDataFile 
    {
        // class delegates

        // class events

        // class functions
        FUNCTION init ();
        FUNCTION read ();
        FUNCTION capture ();
        FUNCTION update ();
        FUNCTION write ();
        FUNCTION sendValues ();
        FUNCTION setNodeID ( SIGNED_LONG_INTEGER nodeID );
        STRING_FUNCTION getNodeInfo ();
        STRING_FUNCTION getNodeName ();
        STRING_FUNCTION setNodeName ( STRING nodeName );
        STRING_FUNCTION getNodeType ();
        STRING_FUNCTION setNodeType ( SIGNED_LONG_INTEGER type );
        STRING_FUNCTION getNodeValue ( SIGNED_LONG_INTEGER valueID );
        STRING_FUNCTION setNodeValue ( SIGNED_LONG_INTEGER valueID , STRING value );
        STRING_FUNCTION nodeRefresh ();
        STRING_FUNCTION nodeDefaults ();
        STRING_FUNCTION nodeUpdate ();
        SIGNED_LONG_INTEGER_FUNCTION getValueType ( SIGNED_LONG_INTEGER join );
        STRING_FUNCTION exportNodeAsSQL ();
        SIGNED_LONG_INTEGER_FUNCTION GetHashCode ();
        STRING_FUNCTION ToString ();

        // class variables
        SIGNED_LONG_INTEGER nodeType;
        STRING name[];
        static STRING version[];

        // class properties
        SIGNED_LONG_INTEGER id;
    };

     class XSig 
    {
        // class delegates

        // class events

        // class functions
        STRING_FUNCTION sendSerial ( INTEGER join , STRING value );
        STRING_FUNCTION sendAnalog ( INTEGER join , INTEGER value );
        SIGNED_LONG_INTEGER_FUNCTION GetHashCode ();
        STRING_FUNCTION ToString ();

        // class variables
        static STRING version[];

        // class properties
    };

     class sqlHelper 
    {
        // class delegates

        // class events

        // class functions
        static STRING_FUNCTION createDB ();
        static FUNCTION createNodeList ();
        static FUNCTION createNode ( SIGNED_LONG_INTEGER id );
        static FUNCTION executeSqlCommand ( STRING sqlStatement );
        SIGNED_LONG_INTEGER_FUNCTION GetHashCode ();
        STRING_FUNCTION ToString ();

        // class variables
        static STRING version[];

        // class properties
    };

     class NodeListFile 
    {
        // class delegates

        // class events

        // class functions
        FUNCTION read ( STRING file );
        FUNCTION write ( STRING file );
        FUNCTION addNodeListEntry ( SIGNED_LONG_INTEGER id , SIGNED_LONG_INTEGER type , STRING name );
        STRING_FUNCTION getNodeList ();
        FUNCTION initNodes ();
        STRING_FUNCTION FixNodesWithMissingData ();
        SIGNED_LONG_INTEGER_FUNCTION listSize ();
        STRING_FUNCTION exportListAsSQL ();
        SIGNED_LONG_INTEGER_FUNCTION ConvertNodeID ( SIGNED_LONG_INTEGER id );
        SIGNED_LONG_INTEGER_FUNCTION GetHashCode ();
        STRING_FUNCTION ToString ();

        // class variables
        static STRING version[];

        // class properties
    };

     class NodeListDB 
    {
        // class delegates

        // class events

        // class functions
        FUNCTION read ( STRING file );
        FUNCTION write ( STRING file );
        FUNCTION addNodeListEntry ( SIGNED_LONG_INTEGER id , SIGNED_LONG_INTEGER type , STRING name );
        STRING_FUNCTION getNodeList ();
        FUNCTION initNodes ();
        STRING_FUNCTION FixNodesWithMissingData ();
        SIGNED_LONG_INTEGER_FUNCTION listSize ();
        SIGNED_LONG_INTEGER_FUNCTION ConvertNodeID ( SIGNED_LONG_INTEGER id );
        SIGNED_LONG_INTEGER_FUNCTION GetHashCode ();
        STRING_FUNCTION ToString ();

        // class variables
        static STRING version[];

        // class properties
    };

     class NodeDataDB 
    {
        // class delegates

        // class events

        // class functions
        FUNCTION init ();
        FUNCTION read ();
        FUNCTION capture ();
        FUNCTION update ();
        FUNCTION write ();
        FUNCTION sendValues ();
        FUNCTION setNodeID ( SIGNED_LONG_INTEGER nodeID );
        STRING_FUNCTION getNodeInfo ();
        STRING_FUNCTION getNodeName ();
        STRING_FUNCTION setNodeName ( STRING nodeName );
        STRING_FUNCTION getNodeType ();
        STRING_FUNCTION setNodeType ( SIGNED_LONG_INTEGER type );
        STRING_FUNCTION getNodeValue ( SIGNED_LONG_INTEGER valueID );
        STRING_FUNCTION setNodeValue ( SIGNED_LONG_INTEGER valueID , STRING value );
        STRING_FUNCTION nodeRefresh ();
        STRING_FUNCTION nodeDefaults ();
        STRING_FUNCTION nodeUpdate ();
        SIGNED_LONG_INTEGER_FUNCTION getValueType ( SIGNED_LONG_INTEGER join );
        SIGNED_LONG_INTEGER_FUNCTION GetHashCode ();
        STRING_FUNCTION ToString ();

        // class variables
        SIGNED_LONG_INTEGER nodeType;
        STRING name[];
        static STRING version[];

        // class properties
        SIGNED_LONG_INTEGER id;
    };

     class Node 
    {
        // class delegates

        // class events
        EventHandler OnCore_CMD ( Node sender, DataChangeEventArgs e );
        EventHandler OnCore_Values_TX ( Node sender, DataChangeEventArgs e );
        EventHandler OnCore_Labels_TX ( Node sender, DataChangeEventArgs e );
        EventHandler OnCore_Defaults_TX ( Node sender, DataChangeEventArgs e );
        EventHandler OnWaiting ( Node sender, DataChangeEventArgs e );
        EventHandler OnConnected ( Node sender, DataChangeEventArgs e );
        EventHandler OnInit ( Node sender, DataChangeEventArgs e );
        EventHandler OnReady ( Node sender, DataChangeEventArgs e );

        // class functions
        SIGNED_LONG_INTEGER_FUNCTION GetHashCode ();
        STRING_FUNCTION ToString ();

        // class variables
        static STRING version[];

        // class properties
        SIGNED_LONG_INTEGER ID;
    };

     class DataChangeEventArgs 
    {
        // class delegates

        // class events

        // class functions
        SIGNED_LONG_INTEGER_FUNCTION GetHashCode ();
        STRING_FUNCTION ToString ();

        // class variables
        static STRING version[];

        // class properties
        STRING serial[];
        INTEGER analog;
    };

